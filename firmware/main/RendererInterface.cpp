#include <RendererInterface.h>
#include <priorities.h>
#include <esp_log.h>
#include <freertos/FreeRTOS.h>
#include <freertos/semphr.h>
#include <freertos/task.h>
#include <fpm/literals.hpp>

static const char *TAG = "RendererInterface";

LedColor LedColor::fromHSV(float h, float s, float v)
{
    h          = std::clamp(h, 0.0f, 359.0f);
    s          = std::clamp(s, 0.0f, 1.0f);
    v          = std::clamp(v, 0.0f, 1.0f);
    int     hi = static_cast<int>(h / 60);
    float   f  = h / 60 - hi;
    uint8_t p  = v * (1 - s) * 255;
    uint8_t q  = v * (1 - f * s) * 255;
    uint8_t t  = v * (1 - (1 - f) * s) * 255;
    uint8_t vu = v * 255;

    switch(hi)
    {
        // clang-format off
    default:
    case 0: return LedColor(vu, t, p);
    case 1: return LedColor(q, vu, p);
    case 2: return LedColor(p, vu, t);
    case 3: return LedColor(p, q, vu);
    case 4: return LedColor(t, p, vu);
    case 5: return LedColor(vu, p, q);
        // clang-format on
    }
}

LedColor LedColor::fromHSV(fpm::fixed_16_16 h, fpm::fixed_16_16 s, fpm::fixed_16_16 v)
{
    using namespace fpm::literals;

    h          = std::clamp(h, 0.0_fp16, 359.99999_fp16);
    s          = std::clamp(s, 0.0_fp16, 1.0_fp16);
    v          = std::clamp(v, 0.0_fp16, 1.0_fp16);
    int     hi = static_cast<int>(h / 60);
    auto    f  = h / 60 - hi;
    uint8_t p  = static_cast<uint8_t>(v * (1 - s) * 255);
    uint8_t q  = static_cast<uint8_t>(v * (1 - f * s) * 255);
    uint8_t t  = static_cast<uint8_t>(v * (1 - (1 - f) * s) * 255);
    uint8_t vu = static_cast<uint8_t>(v * 255);

    switch(hi)
    {
        // clang-format off
    default:
    case 0: return LedColor(vu, t, p);
    case 1: return LedColor(q, vu, p);
    case 2: return LedColor(p, vu, t);
    case 3: return LedColor(p, q, vu);
    case 4: return LedColor(t, p, vu);
    case 5: return LedColor(vu, p, q);
        // clang-format on
    }
}


RendererInterface::RendererInterface()
    : _frameBuffer(frameSize, heap_caps_malloc(frameSize, MALLOC_CAP_DMA), frameSize)
{
    _stopSemaphore = xSemaphoreCreateBinary();
    assert(_stopSemaphore);
    _planeSemaphore = xSemaphoreCreateBinary();
}

RendererInterface::~RendererInterface()
{
    if(_running)
    {
        _running = false;
        xSemaphoreTake(_stopSemaphore, portMAX_DELAY);
    }
    vSemaphoreDelete(_stopSemaphore);
    vSemaphoreDelete(_planeSemaphore);
    _frameBuffer.clear();
    heap_caps_free(_frameBuffer.data());
}

bool RendererInterface::init()
{
    if(!_cube.reconfigure())
    {
        ESP_LOGE(TAG, "Error configuring the FPGA");
        return false;
    }

    _running = true;
    xTaskCreate(reinterpret_cast<TaskFunction_t>(RendererInterface::sRenderingTask),
                "RendererInterface", 2048, this, priorities::task::renderer, nullptr);
    return true;
}

void RendererInterface::start()
{
    if(!_running)
        return;
    if(_active)
    {
        ESP_LOGW(TAG, "Starting the renderer while already started");
        return;
    }
    _active = true;
    interface()->enable(true);
}

void RendererInterface::stop()
{
    if(!_running)
        return;
    if(!_active)
    {
        ESP_LOGW(TAG, "Stopping the renderer while already stopped");
        return;
    }
    _active = false;
    interface()->disable();
}

void RendererInterface::sRenderingTask(RendererInterface *self)
{
    self->renderingTask();
}

void RendererInterface::renderingTask()
{
    using planeCallback_t = drivers::LedCubeInterface::planeCallback_t;
    interface()->setPlaneCallback(
        planeCallback_t::create<RendererInterface, &RendererInterface::iPlaneCallback>(*this));
    uint32_t frameCounter = 0;
    int      lastPlane    = 0;
    while(_running)
    {
        if(!_active)
        {
            vTaskDelay(pdMS_TO_TICKS(10));
            continue;
        }
        if(!xSemaphoreTake(_planeSemaphore, pdMS_TO_TICKS(100)))
            continue;

        int activePlane = _activePlane;

        // Send the last rendered plane
        int  lastPlane = activePlane == 0 ? dims[2] - 1 : activePlane - 1;
        auto data      = plane(lastPlane);
        drivers::LedCubeInterface::address_type planeAddr = strides[2] * lastPlane;

        interface()->writeMemory(planeAddr, reinterpret_cast<std::byte *>(data.data()),
                                 data.size());

        if(activePlane < lastPlane)
        {
            frameCounter++;
        }
        lastPlane = activePlane;

        // Render the new plane
        auto cb = _renderCallback;
        if(cb)
        {
            cb(this, frameCounter, activePlane);
        }
    }
    xSemaphoreGive(_stopSemaphore);
}

void RendererInterface::iPlaneCallback(int p)
{
    BaseType_t xHigherPrioTaskWoken = pdFALSE;

    // Notify the render task to start rendering the next plane
    _activePlane = p;
    xSemaphoreGiveFromISR(_planeSemaphore, &xHigherPrioTaskWoken);

    if(xHigherPrioTaskWoken)
        portYIELD_FROM_ISR();
}