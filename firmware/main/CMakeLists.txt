idf_component_register(SRCS 
        main.cpp
        Buttons_touch.cpp
        Drivers.cpp
        FPGA.cpp
        LedCube.cpp
        LedCubeInterface.cpp
        Microphone.cpp
        USB.cpp
        RendererInterface.cpp
        RendererManager.cpp
        renderers/PcRenderer.cpp
        renderers/PlasmaRenderer.cpp
        renderers/RainRenderer.cpp
        renderers/TestRenderer.cpp
        renderers/FireRenderer.cpp
        communication/networkserver.cpp
        communication/networkconnection.cpp
        communication/connectionManager.cpp
        communication/server.cpp
    INCLUDE_DIRS "."
    REQUIRES
        driver
        esp_wifi
        esp_rom
        statusled
        freertoscpp
        esp_event
        vfs
        spiffs
        esp_timer
        etl
        esp_tinyusb
        mdns
        allocators
        fpm
        networkmanager
        configuration
        sysinfo
    )

target_link_libraries(${COMPONENT_LIB} PUBLIC proto_ledcube.nanopb)
# target_include_directories(${COMPONENT_LIB} PRIVATE ${dir_pw_third_party_nanopb})
target_link_libraries(${COMPONENT_LIB} PRIVATE pw_sys_io pw_assert)

component_compile_options($<$<COMPILE_LANGUAGE:CXX>:-std=gnu++17>)

add_subdirectory(proto)
