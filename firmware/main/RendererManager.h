#pragma once

#include <RendererInterface.h>
#include <led-cube.pb.h>
#include <configuration.h>
#include <memory>
#include <renderers/irenderer.h>

class Server;

class RendererManager
{
public:
    RendererManager(std::shared_ptr<Configuration> config, Server &server);
    ~RendererManager();


private:
    RendererInterface              _itf;
    std::shared_ptr<Configuration> _config;
    Server &                       _server;
    ledcube_RuntimeConfig_Mode     _currentMode;
    int                            _globalIntensity;
    std::unique_ptr<IRenderer>     _activeRenderer;

    void loadRenderer();

    // callbacks
    void onRuntimeConfigChanged(ledcube_RuntimeConfig &config);
    void onRenderPlane(RendererInterface *renderer, uint32_t frameNr, int planeNr);
};
