
#include <USB.h>
#include <tusb.h>
#include <priorities.h>
#include <array>
#include <etl/string.h>

#include <soc/periph_defs.h>
#include <soc/usb_periph.h>
#include <tinyusb.h>
#include <tinyusb_net.h>
#include <userboard.h>
#include <esp_log.h>
#include <esp_mac.h>
#include <esp_netif.h>
#include <lwip/esp_netif_net_stack.h>
#include <soc/rtc_cntl_reg.h>

#include <usb/descriptors/descriptors.h>
#include <usb/descriptors/standard_classes.h>
#include <usb/types.h>

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>


static const char *TAG = "USB";

using namespace drivers;


//------------- Interfaces enumeration -------------//
enum
{
    ITF_NUM_CDC = 0,
    ITF_NUM_CDC_DATA,
    ITF_NUM_NET,
    ITF_NUM_NET_DATA,
    ITF_NUM_DFU_RT,
    ITF_NUM_TOTAL
};


static constexpr size_t TUSB_DESC_TOTAL_LEN = TUD_CONFIG_DESC_LEN + CFG_TUD_CDC * TUD_CDC_DESC_LEN +
                                              CFG_TUD_NCM * TUD_CDC_NCM_DESC_LEN +
                                              CFG_TUD_DFU_RUNTIME * TUD_DFU_RT_DESC_LEN;

//------------- USB Endpoint numbers -------------//
enum
{
    // Available USB Endpoints: 5 IN/OUT EPs and 1 IN EP
    EP_EMPTY = 0,
    EPNUM_0_CDC_NOTIF,
    EPNUM_0_CDC,
    EPNUM_NET_NOTIF,
    EPNUM_NET_DATA,
};

//------------- STRID -------------//
enum
{
    STRID_LANGID = 0,
    STRID_MANUFACTURER,
    STRID_PRODUCT,
    STRID_SERIAL,
    STRID_CDC_INTERFACE,
    STRID_NET_INTERFACE,
    STRID_MAC,
    STRID_DFU_RT,
    NUM_STRIDS
};

//------------- Array of String Descriptors -------------//
static const char *const g_string_descriptors[] = {
    // array of pointer to string descriptors
    (char[]){0x09, 0x04},                    // 0: is supported language is English (0x0409)
    CONFIG_TINYUSB_DESC_MANUFACTURER_STRING, // 1: Manufacturer
    CONFIG_TINYUSB_DESC_PRODUCT_STRING,      // 2: Product
    CONFIG_TINYUSB_DESC_SERIAL_STRING,       // 3: Serials, should use chip ID
    "LED Cube CDC",                          // 4: CDC Interface
    "USB net",                               // 5. NET Interface
    "",                                      // 6. MAC
    "DFU Runtime",                           // 7. DFU Runtime
    NULL                                     // NULL: Must be last. Indicates end of array
};

static uint8_t const g_config_descriptor[] = {
    // Configuration number, interface count, string index, total length, attribute, power in mA
    TUD_CONFIG_DESCRIPTOR(1,
                          ITF_NUM_TOTAL,
                          0,
                          TUSB_DESC_TOTAL_LEN,
                          TUSB_DESC_CONFIG_ATT_REMOTE_WAKEUP,
                          100),
    // Interface number, string index, EP notification address and size, EP data address (out, in)
    // and size.
    TUD_CDC_DESCRIPTOR(ITF_NUM_CDC,
                       STRID_CDC_INTERFACE,
                       0x80 | EPNUM_0_CDC_NOTIF,
                       8,
                       EPNUM_0_CDC,
                       0x80 | EPNUM_0_CDC,
                       64),
    // Interface number, description string index, MAC address string index, EP notification address
    // and size, EP data address (out, in), and size, max segment size.
    TUD_CDC_NCM_DESCRIPTOR(ITF_NUM_NET,
                           STRID_NET_INTERFACE,
                           STRID_MAC,
                           (0x80 | EPNUM_NET_NOTIF),
                           64,
                           EPNUM_NET_DATA,
                           (0x80 | EPNUM_NET_DATA),
                           64,
                           CFG_TUD_NET_MTU),
    // Interface number, string index, attributes, detach timeout, transfer size
    TUD_DFU_RT_DESCRIPTOR(ITF_NUM_DFU_RT, STRID_DFU_RT, 0x0d, 1000, 4096),
};

USB::USB()
{
    const tinyusb_config_t config{
        .device_descriptor        = nullptr,
        .string_descriptor        = const_cast<const char **>(g_string_descriptors),
        .string_descriptor_count  = NUM_STRIDS,
        .external_phy             = false,
        .configuration_descriptor = g_config_descriptor,
        .self_powered             = true,
        .vbus_monitor_io          = board::usb::usb_vbus,
    };

    ESP_ERROR_CHECK(tinyusb_driver_install(&config));

    // Configure the network interface for the USB device.
    tinyusb_net_config_t net_config{
        .mac_addr         = {0},
        .on_recv_callback = reinterpret_cast<tusb_net_rx_cb_t>(&USB::onNetReceive),
        .free_tx_buffer   = nullptr,
        .on_init_callback = reinterpret_cast<tusb_net_init_cb_t>(&USB::onNetInit),
        .user_context     = this,
    };
    // Retrieve the MAC address from the board.
    // We will use the Ethernet MAC address for the USB device.
    ESP_ERROR_CHECK(esp_read_mac(net_config.mac_addr, ESP_MAC_ETH));
    ESP_ERROR_CHECK(tinyusb_net_init(TINYUSB_USBDEV_0, &net_config));

    createNetIf();
    esp_netif_action_start(_netif, 0, 0, nullptr);

    ESP_LOGI(TAG, "USB driver installed");
}

USB::~USB()
{
    ESP_ERROR_CHECK(tinyusb_driver_uninstall());
}

void USB::createNetIf()
{
    esp_netif_ip_info_t netif_ip_info{};
    netif_ip_info.ip.addr      = esp_ip4addr_aton("192.168.54.1");
    netif_ip_info.gw.addr      = esp_ip4addr_aton("192.168.54.1");
    netif_ip_info.netmask.addr = esp_ip4addr_aton("255.255.255.0");

    // Create the base configuration
    esp_netif_inherent_config_t base_cfg{
        .flags = static_cast<esp_netif_flags>(ESP_NETIF_DHCP_SERVER | ESP_NETIF_FLAG_AUTOUP),
        .mac{},
        .ip_info       = &netif_ip_info,
        .get_ip_event  = 0,
        .lost_ip_event = 0,
        .if_key        = "usb0",
        .if_desc       = "USB Network Interface",
        .route_prio    = 30,
        .bridge_info   = nullptr,
    };
    ESP_ERROR_CHECK(esp_read_mac(base_cfg.mac, ESP_MAC_ETH));
    esp_netif_driver_ifconfig_t driver_cfg{
        .handle   = this,
        .transmit = reinterpret_cast<esp_err_t (*)(void *, void *, size_t)>(&USB::onNetTransmit),
        .transmit_wrap = nullptr,
        .driver_free_rx_buffer =
            reinterpret_cast<void (*)(void *, void *)>(&USB::onNetFreeRxBuffer),
    };
    esp_netif_netstack_config stack_cfg{
        .lwip{
            .init_fn  = ethernetif_init,
            .input_fn = ethernetif_input,
        },
    };
    esp_netif_config_t netif_cfg{
        .base   = &base_cfg,
        .driver = &driver_cfg,
        .stack  = &stack_cfg,
    };

    _netif = esp_netif_new(&netif_cfg);
}

esp_err_t USB::onNetReceive(void *buffer, uint16_t len, USB *self)
{
    if(!self->_netif)
    {
        // Silently drop the packet
        return ESP_OK;
    }

    void *buf_copy = malloc(len);
    if(!buf_copy)
    {
        return ESP_ERR_NO_MEM;
    }
    memcpy(buf_copy, buffer, len);
    return esp_netif_receive(self->_netif, buf_copy, len, buf_copy);
}

void USB::onNetInit(USB *self)
{
    ESP_LOGI(TAG, "Network interface initialized");
}

esp_err_t USB::onNetTransmit(USB *self, void *buffer, size_t len)
{
    if(tinyusb_net_send_sync(buffer, len, buffer, pdMS_TO_TICKS(100)) != ESP_OK)
    {
        ESP_LOGE(TAG, "Failed to transmit %d bytes", len);
    }
    return ESP_OK;
}

void USB::onNetFreeRxBuffer(USB *self, void *buffer)
{
    free(buffer);
}

extern "C" void tud_dfu_runtime_reboot_to_dfu_cb(void)
{
    ESP_LOGI(TAG, "Rebooting to DFU mode");
    REG_WRITE(RTC_CNTL_OPTION1_REG, RTC_CNTL_FORCE_DOWNLOAD_BOOT);
    esp_restart();
}