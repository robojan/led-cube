#include <LedCube.h>
#include <userboard.h>
#include <esp_assert.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <esp_log.h>

static const char *TAG = "LedCube";

using namespace drivers;

LedCube::LedCube()
{
    initialize();
}

void LedCube::initialize()
{
    ESP_ERROR_CHECK(gpio_set_direction(board::FPGA::done_pin, GPIO_MODE_INPUT));
    ESP_ERROR_CHECK(gpio_set_direction(board::FPGA::rst_pin, GPIO_MODE_OUTPUT));
    ESP_ERROR_CHECK(gpio_set_level(board::FPGA::rst_pin, 0));

    _fpga = std::make_unique<FPGA>();
    _mode = Mode::Unconfigured;
}

bool LedCube::reconfigure()
{
    if(_mode != Mode::Unconfigured)
    {
        unconfigure();
    }
    ESP_LOGI(TAG, "Configuring the FPGA");
    assert(_fpga);
    auto status = _fpga->configure(BitstreamFile);
    if(status != ESP_OK)
    {
        ESP_LOGE(TAG, "Error configuring the LED cube: %d", status);
        return false;
    }
    _fpga.reset();
    reset(false);
    vTaskDelay(1);
    _if   = std::make_unique<LedCubeInterface>();
    _mode = Mode::LedCube;
    return true;
}

void LedCube::unconfigure()
{
    assert(_mode != Mode::Unconfigured);
    reset(true);
    _if.reset();
    _fpga = std::make_unique<FPGA>();
    _mode = Mode::Unconfigured;
}

void LedCube::reset(bool enable) const
{
    ESP_ERROR_CHECK(gpio_set_level(board::FPGA::rst_pin, enable ? 0 : 1));
}