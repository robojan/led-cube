#pragma once

#include <driver/spi_master.h>
#include <cstdint>
#include <cstddef>
#include <freertos/mutex.hpp>
#include <volatile.h>
#include <etl/delegate.h>

namespace drivers
{
class LedCubeInterface
{
public:
    using address_type = uint32_t;
    enum class RegisterAddr : uint8_t;
    using planeCallback_t = etl::delegate<void(int)>;

    LedCubeInterface();
    ~LedCubeInterface();

    void test();
    void test2();
    void clearMem();
    void dumpMemory();
    void testMem();

    void    writeMemory(address_type addr, const std::byte *data, size_t size);
    void    readMemory(address_type addr, std::byte *data, size_t size);
    void    setGlobalIntensity(uint8_t intensity);
    uint8_t getGlobalIntensity();
    void    enable(bool enable);
    bool    isEnabled();
    bool    isRunning();
    void    disable();
    void    waitUntilDisabled();


    bool isSynced() const { return _hasSynced.load(); }
    int  currentPlane() const { return _currentPlane.load(); }

    void setPlaneCallback(planeCallback_t cb);

private:
    spi_device_handle_t _spiHandle = nullptr;
    freertos::Mutex     _mutex;
    planeCallback_t     _planeCallback;
    struct
    {
        uint8_t ctrl;
    } _regCache = {0};

    // Interrupt variables
    volatile_variable<int>  _currentPlane = 0;
    volatile_variable<bool> _hasSynced    = false;

    void      hostInitialize();
    std::byte readRegister(RegisterAddr reg);
    void      writeRegister(RegisterAddr reg, std::byte val);

    static void sIntrPlaneBlank(LedCubeInterface *self);
    void        intrPlaneBlank();
};

} // namespace drivers