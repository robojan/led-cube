#include <Microphone.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <userboard.h>
#include <priorities.h>
#include <esp_err.h>
#include <esp_log.h>
#include <driver/i2s_std.h>

using namespace drivers;

static const char *TAG = "Microphone";

Microphone::Microphone(uint32_t samplerate, uint32_t numSamples, uint32_t bufferCount)
{
    i2s_chan_config_t channelConfig{
        .id                   = I2S_NUM_AUTO,
        .role                 = I2S_ROLE_MASTER,
        .dma_desc_num         = bufferCount,
        .dma_frame_num        = numSamples,
        .auto_clear_after_cb  = false,
        .auto_clear_before_cb = false,
        .allow_pd             = false,
        .intr_priority        = priorities::isr::microphone,
    };
    ESP_ERROR_CHECK(i2s_new_channel(&channelConfig, nullptr, &_rxHandle));

    i2s_std_config_t config{
        .clk_cfg{
            .sample_rate_hz = samplerate,
            .clk_src        = I2S_CLK_SRC_APLL,
            .mclk_multiple  = I2S_MCLK_MULTIPLE_384,
        },
        .slot_cfg{
            .data_bit_width = I2S_DATA_BIT_WIDTH_24BIT,
            .slot_bit_width = I2S_SLOT_BIT_WIDTH_32BIT,
            .slot_mode      = I2S_SLOT_MODE_MONO,
            .slot_mask      = I2S_STD_SLOT_LEFT,
            .ws_width       = 32,
            .ws_pol         = true,
            .bit_shift      = true,
            .msb_right      = false,
        },
        .gpio_cfg{
            .mclk = I2S_GPIO_UNUSED,
            .bclk = board::microphone::clk_pin,
            .ws   = board::microphone::wordselect_pin,
            .dout = I2S_GPIO_UNUSED,
            .din  = board::microphone::data_pin,
            .invert_flags =
                {
                    .mclk_inv = 0,
                    .bclk_inv = 0,
                    .ws_inv   = 0,
                },
        },
    };
    ESP_ERROR_CHECK(i2s_channel_init_std_mode(_rxHandle, &config));


    _taskExitSemaphore = xSemaphoreCreateBinary();
    assert(_taskExitSemaphore != nullptr);

    _bufferSize = numSamples;
}

Microphone::~Microphone()
{
    if(isRunning())
    {
        stop();
    }
    ESP_ERROR_CHECK(i2s_del_channel(_rxHandle));
}

void Microphone::start(DataReadyFunction cb)
{
    assert(!isRunning());
    _stopping = false;
    _callback = std::move(cb);

    ESP_ERROR_CHECK(i2s_channel_enable(_rxHandle));

    auto status = xTaskCreate(&Microphone::readStaticTask, "mic", 700, this,
                              priorities::task::microphone, &_task);
    assert(status == pdTRUE);
}

void Microphone::stop()
{
    _stopping = true;

    ESP_ERROR_CHECK(i2s_channel_disable(_rxHandle));

    xSemaphoreTake(_taskExitSemaphore, portMAX_DELAY);
}

void Microphone::readStaticTask(void *self)
{
    reinterpret_cast<Microphone *>(self)->readTask();
}

void Microphone::readTask()
{
    std::vector<AudioSample> dataBuffer;
    dataBuffer.resize(_bufferSize);

    while(!_stopping)
    {
        size_t bytesRead;
        auto   status =
            i2s_channel_read(_rxHandle, dataBuffer.data(), dataBuffer.size() * sizeof(AudioSample),
                             &bytesRead, portMAX_DELAY);
        if(status != ESP_OK)
        {
            ESP_LOGE(TAG, "Error reading from microphone: %s", esp_err_to_name(status));
            break;
        }

        assert(bytesRead % sizeof(AudioSample) == 0);
        int numSamplesRead = bytesRead / sizeof(AudioSample);

        _callback(dataBuffer.data(), numSamplesRead);
    }

    xSemaphoreGive(_taskExitSemaphore);
}