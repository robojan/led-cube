#pragma once

#include <esp_event.h>

namespace drivers
{
ESP_EVENT_DECLARE_BASE(DRIVER_EVENTS);

namespace EventId
{
enum EventId
{
    ButtonPressed,
    ButtonReleased
};
}
} // namespace drivers
