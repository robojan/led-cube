#pragma once

#include <cstdint>
#include <functional>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/semphr.h>
#include <driver/i2s_std.h>

namespace drivers
{
struct AudioSample
{
    int32_t val : 32;
    inline AudioSample(int v = 0) : val(v) {}

    inline             operator int() const { return val; }
    inline AudioSample operator+=(AudioSample other)
    {
        val += other.val;
        return *this;
    }
    inline AudioSample operator-=(AudioSample other)
    {
        val -= other.val;
        return *this;
    }
    inline AudioSample operator*=(AudioSample other)
    {
        val *= other.val;
        return *this;
    }
    inline AudioSample operator/=(AudioSample other)
    {
        val /= other.val;
        return *this;
    }
    inline AudioSample operator%=(AudioSample other)
    {
        val %= other.val;
        return *this;
    }
} __attribute__((packed));
static_assert(sizeof(AudioSample) == 4);

inline AudioSample operator+(AudioSample a, AudioSample b)
{
    return a.val + b.val;
}
inline AudioSample operator-(AudioSample a, AudioSample b)
{
    return a.val - b.val;
}
inline AudioSample operator*(AudioSample a, AudioSample b)
{
    return a.val * b.val;
}
inline AudioSample operator/(AudioSample a, AudioSample b)
{
    return a.val / b.val;
}
inline AudioSample operator%(AudioSample a, AudioSample b)
{
    return a.val % b.val;
}

class Microphone
{
public:
    using DataReadyFunction = std::function<void(const AudioSample *buf, int len)>;

private:
    TaskHandle_t      _task     = nullptr;
    bool              _stopping = false;
    DataReadyFunction _callback;
    SemaphoreHandle_t _taskExitSemaphore = nullptr;
    int               _bufferSize;
    i2s_chan_handle_t _rxHandle{nullptr};

public:
    Microphone(uint32_t samplerate, uint32_t numSamples, uint32_t bufferCount = 2);
    ~Microphone();

    bool isRunning() const { return _task != nullptr; };
    void start(DataReadyFunction cb);
    void stop();

private:
    static void readStaticTask(void *self);
    void        readTask();
};
} // namespace drivers
