#pragma once

#include "../types.h"

namespace usb::descriptors
{
struct Pipe
{
    constexpr Pipe(uint16_t outMaxPacketSize, uint16_t inMaxPacketSize)
        : _outMaxPacketSize(outMaxPacketSize),
          _inMaxPacketSize(inMaxPacketSize),
          _hsOutMaxPacketSize(outMaxPacketSize),
          _hsInMaxPacketSize(inMaxPacketSize)
    {
    }

    constexpr Pipe(uint16_t outMaxPacketSize,
                   uint16_t inMaxPacketSize,
                   uint16_t hsOutMaxPacketSize,
                   uint16_t hsInMaxPacketSize)
        : _outMaxPacketSize(outMaxPacketSize),
          _inMaxPacketSize(inMaxPacketSize),
          _hsOutMaxPacketSize(hsOutMaxPacketSize),
          _hsInMaxPacketSize(hsInMaxPacketSize)
    {
    }

    static constexpr int DescriptorsSize() { return descriptorSize::endpoint * 2; }

    template <typename T>
    constexpr void fillDescriptor(T &array, int &idx, int &ifIdx, int &endpointAddress) const
    {
        array[idx++] = static_cast<uint8_t>(descriptorSize::endpoint);
        array[idx++] = static_cast<uint8_t>(DescriptorType::Endpoint);
        array[idx++] = endpointAddress | static_cast<uint8_t>(Direction::Out);
        array[idx++] = static_cast<uint8_t>(TransferType::Bulk);
        array[idx++] = static_cast<uint8_t>(_outMaxPacketSize & 0xFF);
        array[idx++] = static_cast<uint8_t>((_outMaxPacketSize >> 8) & 0xFF);
        array[idx++] = 0;
        array[idx++] = static_cast<uint8_t>(descriptorSize::endpoint);
        array[idx++] = static_cast<uint8_t>(DescriptorType::Endpoint);
        array[idx++] = endpointAddress | static_cast<uint8_t>(Direction::In);
        array[idx++] = static_cast<uint8_t>(TransferType::Bulk);
        array[idx++] = static_cast<uint8_t>(_inMaxPacketSize & 0xFF);
        array[idx++] = static_cast<uint8_t>((_inMaxPacketSize >> 8) & 0xFF);
        array[idx++] = 0;
        endpointAddress++;
    }

    template <typename T>
    constexpr void updateDescriptor(Speed speed, T &array, int &idx) const
    {
        auto inMaxPacketSize  = speed == HighSpeed ? _hsInMaxPacketSize : _inMaxPacketSize;
        auto outMaxPacketSize = speed == HighSpeed ? _hsOutMaxPacketSize : _outMaxPacketSize;
        array[idx + 4]        = static_cast<uint8_t>(outMaxPacketSize & 0xFF);
        array[idx + 5]        = static_cast<uint8_t>((outMaxPacketSize >> 8) & 0xFF);
        array[idx + 4 + descriptorSize::endpoint] = static_cast<uint8_t>(inMaxPacketSize & 0xFF);
        array[idx + 5 + descriptorSize::endpoint] =
            static_cast<uint8_t>((inMaxPacketSize >> 8) & 0xFF);
        idx += descriptorSize::endpoint * 2;
    }

    static constexpr int isEndpoint() { return 2; }

private:
    uint16_t _outMaxPacketSize;
    uint16_t _inMaxPacketSize;
    uint16_t _hsOutMaxPacketSize;
    uint16_t _hsInMaxPacketSize;
};
} // namespace usb::descriptors