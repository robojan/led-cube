#pragma once

#include "../types.h"
#include <tuple>

namespace usb::descriptors
{
template <typename... Args>
struct Configuration
{
    constexpr Configuration(bool                selfPowered,
                            bool                remoteWakeup,
                            int                 bMaxPower,
                            uint8_t             iConfiguration,
                            std::tuple<Args...> interfaces)
        : _iConfiguration(iConfiguration),
          _bmAttributes((selfPowered ? 0x40 : 0) | (remoteWakeup ? 0x20 : 0) | 0x80),
          _bMaxPower(bMaxPower / 2),
          _interfaces(std::move(interfaces))
    {
    }

    constexpr Configuration(bool    selfPowered,
                            bool    remoteWakeup,
                            int     bMaxPower,
                            uint8_t iConfiguration,
                            Args... interfaces)
        : _iConfiguration(iConfiguration),
          _bmAttributes((selfPowered ? 0x40 : 0) | (remoteWakeup ? 0x20 : 0) | 0x80),
          _bMaxPower(bMaxPower / 2),
          _interfaces(std::make_tuple(interfaces...))
    {
    }

    static constexpr int DescriptorsSize()
    {
        return descriptorSize::configuration + (Args::DescriptorsSize() + ...);
    }

    static constexpr int NumInterfaces() { return (Args::NumInterfaces() + ...); }

    static constexpr auto getNumStrings() { return 1 + (Args::getNumStrings() + ...); }

    template <typename T>
    constexpr void fillDescriptor(T &array, int &idx, int configIdx) const
    {
        array[idx++]        = static_cast<uint8_t>(descriptorSize::configuration);
        array[idx++]        = static_cast<uint8_t>(DescriptorType::Configuration);
        array[idx++]        = static_cast<uint8_t>(DescriptorsSize() & 0xFF);
        array[idx++]        = static_cast<uint8_t>((DescriptorsSize() >> 8) & 0xFF);
        array[idx++]        = static_cast<uint8_t>(NumInterfaces());
        array[idx++]        = static_cast<uint8_t>(configIdx);
        array[idx++]        = _iConfiguration;
        array[idx++]        = _bmAttributes;
        array[idx++]        = _bMaxPower;
        int ifIdx           = 0;
        int endpointAddress = 1;
        std::apply(
            [&array, &idx, &ifIdx, &endpointAddress](auto &&... args) {
                ((args.fillDescriptor(array, idx, ifIdx, endpointAddress)), ...);
            },
            _interfaces);
    }

    template <typename T>
    constexpr void updateDescriptor(Speed speed, T &array, int &idx) const
    {
        idx += descriptorSize::configuration;
        std::apply([&array, &idx,
                    speed](auto &&... args) { ((args.updateDescriptor(speed, array, idx)), ...); },
                   _interfaces);
    }

private:
    uint8_t             _iConfiguration;
    uint8_t             _bmAttributes;
    uint8_t             _bMaxPower;
    std::tuple<Args...> _interfaces;
};

} // namespace usb::descriptors