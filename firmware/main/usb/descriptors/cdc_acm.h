#pragma once

#include "../types.h"

namespace usb::descriptors
{
struct ClassSpecific_CDC_ACM
{
    constexpr ClassSpecific_CDC_ACM(uint8_t bmCapabilities) : _bmCapabilities(bmCapabilities) {}

    static constexpr int DescriptorsSize() { return 4; }

    template <typename T>
    constexpr void fillDescriptor(T &array, int &idx, int &ifIdx, int endpointAddress) const
    {
        array[idx++] = static_cast<uint8_t>(DescriptorsSize());
        array[idx++] = static_cast<uint8_t>(DescriptorType::CS_Interface);
        array[idx++] = 0x02;
        array[idx++] = _bmCapabilities;
    }

    template <typename T>
    constexpr void updateDescriptor(Speed speed, T &array, int &idx) const
    {
        idx += DescriptorsSize();
    }
    static constexpr int isEndpoint() { return 0; }

private:
    uint8_t _bmCapabilities;
};
} // namespace usb::descriptors
