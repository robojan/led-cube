#pragma once

#include "descriptors.h"

namespace usb::descriptors
{
constexpr auto MSC(uint8_t name)
{
    return Interface(Class::MassStorage, 0x06, 0x50, name, Pipe(64, 64, 512, 512));
}

constexpr auto CDC(uint8_t name)
{
    return InterfaceAssociation(
        Class::CDC, 0x02, 0x01, 0,
        Interface(Class::CDC, 0x02, 0x00, name, ClassSpecific_CDC_Header(make_BCD(1, 2, 0)),
                  ClassSpecific_CDC_CallManagement(0, 0), ClassSpecific_CDC_ACM(2),
                  ClassSpecific_CDC_Union<2>({-1, 0}),
                  Endpoint::interruptEndpoint(Direction::In, 8, 16)),
        Interface(Class::CDC_Data, 0x00, 0x00, 0, Pipe(64, 64, 512, 512)));
}
} // namespace usb::descriptors
