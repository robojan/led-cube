#pragma once

#include "../types.h"

namespace usb::descriptors
{
struct ClassSpecific_CDC_Header
{
    constexpr ClassSpecific_CDC_Header(BCD bcdCDC) : _bcdCDC(bcdCDC) {}

    static constexpr int DescriptorsSize() { return 5; }

    template <typename T>
    constexpr void fillDescriptor(T &array, int &idx, int &ifIdx, int endpointAddress) const
    {
        array[idx++] = static_cast<uint8_t>(DescriptorsSize());
        array[idx++] = static_cast<uint8_t>(DescriptorType::CS_Interface);
        array[idx++] = 0x00;
        array[idx++] = static_cast<uint8_t>(_bcdCDC & 0xFF);
        array[idx++] = static_cast<uint8_t>((_bcdCDC >> 8) & 0xFF);
    }

    template <typename T>
    constexpr void updateDescriptor(Speed speed, T &array, int &idx) const
    {
        idx += DescriptorsSize();
    }
    static constexpr int isEndpoint() { return 0; }

private:
    BCD _bcdCDC;
};

} // namespace usb::descriptors