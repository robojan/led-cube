#pragma once

#include "interface.h"
#include <tuple>

namespace usb::descriptors
{
template <typename... Args>
struct InterfaceAssociation
{
    constexpr InterfaceAssociation(Class               bInterfaceClass,
                                   uint8_t             bInterfaceSubClass,
                                   uint8_t             bInterfaceProtocol,
                                   uint8_t             iFunction,
                                   std::tuple<Args...> interfaces)
        : _iFunction(iFunction),
          _bInterfaceClass(static_cast<uint8_t>(bInterfaceClass)),
          _bInterfaceSubClass(bInterfaceSubClass),
          _bInterfaceProtocol(bInterfaceProtocol),
          _interfaces(std::move(interfaces))
    {
    }

    constexpr InterfaceAssociation(Class   bInterfaceClass,
                                   uint8_t bInterfaceSubClass,
                                   uint8_t bInterfaceProtocol,
                                   uint8_t iFunction,
                                   Args... interfaces)
        : _iFunction(iFunction),
          _bInterfaceClass(static_cast<uint8_t>(bInterfaceClass)),
          _bInterfaceSubClass(bInterfaceSubClass),
          _bInterfaceProtocol(bInterfaceProtocol),
          _interfaces(std::make_tuple(interfaces...))
    {
    }

    static constexpr int DescriptorsSize()
    {
        return descriptorSize::interfaceAssociation + (Args::DescriptorsSize() + ...);
    }

    static constexpr int NumInterfaces() { return (Args::isInterface() + ...); }

    static constexpr auto getNumStrings() { return 1; }

    template <typename T>
    constexpr void fillDescriptor(T &array, int &idx, int &ifIdx, int &endpointAddress) const
    {
        array[idx++] = static_cast<uint8_t>(descriptorSize::interfaceAssociation);
        array[idx++] = static_cast<uint8_t>(DescriptorType::InterfaceAssociation);
        array[idx++] = static_cast<uint8_t>(ifIdx);
        array[idx++] = static_cast<uint8_t>(NumInterfaces());
        array[idx++] = _bInterfaceClass;
        array[idx++] = _bInterfaceSubClass;
        array[idx++] = _bInterfaceProtocol;
        array[idx++] = _iFunction;
        std::apply(
            [&array, &idx, &ifIdx, &endpointAddress](auto &&... args) {
                ((args.fillDescriptor(array, idx, ifIdx, endpointAddress)), ...);
            },
            _interfaces);
    }

    template <typename T>
    constexpr void updateDescriptor(Speed speed, T &array, int &idx) const
    {
        idx += descriptorSize::interfaceAssociation;
        std::apply([&array, &idx,
                    speed](auto &&... args) { ((args.updateDescriptor(speed, array, idx)), ...); },
                   _interfaces);
    }

    static constexpr int isInterface() { return 0; }

private:
    uint8_t             _iFunction;
    uint8_t             _bInterfaceClass;
    uint8_t             _bInterfaceSubClass;
    uint8_t             _bInterfaceProtocol;
    std::tuple<Args...> _interfaces;
};

} // namespace usb::descriptors