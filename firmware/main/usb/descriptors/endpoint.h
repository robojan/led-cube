#pragma once

#include "../types.h"

namespace usb::descriptors
{
struct Endpoint
{
    constexpr Endpoint(Direction          direction,
                       TransferType       transferType,
                       SynchonisationType synchronisationType,
                       UsageType          usageType,
                       uint16_t           wMaxPacketSize,
                       uint8_t            bInterval)
        : _bEndpointAddress(static_cast<uint8_t>(direction)),
          _bmAttributes(static_cast<uint8_t>(transferType) |
                        static_cast<uint8_t>(synchronisationType) |
                        static_cast<uint8_t>(usageType)),
          _wMaxPacketSize(wMaxPacketSize),
          _bInterval(bInterval)
    {
    }

    static constexpr Endpoint bulkEndpoint(Direction direction, uint16_t wMaxPacketSize)
    {
        return Endpoint(direction, TransferType::Bulk, SynchonisationType::NoSynchronisation,
                        UsageType::DataEndpoint, wMaxPacketSize, 0);
    }

    static constexpr Endpoint interruptEndpoint(Direction direction,
                                                uint16_t  wMaxPacketSize,
                                                uint8_t   bInterval)
    {
        return Endpoint(direction, TransferType::Interrupt, SynchonisationType::NoSynchronisation,
                        UsageType::DataEndpoint, wMaxPacketSize, bInterval);
    }

    static constexpr int DescriptorsSize() { return descriptorSize::endpoint; }

    template <typename T>
    constexpr void fillDescriptor(T &array, int &idx, int &ifIdx, int &endpointAddress) const
    {
        array[idx++] = static_cast<uint8_t>(descriptorSize::endpoint);
        array[idx++] = static_cast<uint8_t>(DescriptorType::Endpoint);
        array[idx++] = _bEndpointAddress | endpointAddress;
        array[idx++] = _bmAttributes;
        array[idx++] = static_cast<uint8_t>(_wMaxPacketSize & 0xFF);
        array[idx++] = static_cast<uint8_t>((_wMaxPacketSize >> 8) & 0xFF);
        array[idx++] = _bInterval;
        endpointAddress++;
    }

    template <typename T>
    constexpr void updateDescriptor(Speed speed, T &array, int &idx) const
    {
        auto wMaxPacketSize =
            speed == HighSpeed ? _wMaxPacketSize : _wMaxPacketSize; // TODO reimplement
        array[idx + 4] = static_cast<uint8_t>(wMaxPacketSize & 0xFF);
        array[idx + 5] = static_cast<uint8_t>((wMaxPacketSize >> 8) & 0xFF);
        idx += descriptorSize::endpoint;
    }

    static constexpr int isEndpoint() { return 1; }

private:
    uint8_t  _bEndpointAddress;
    uint8_t  _bmAttributes;
    uint16_t _wMaxPacketSize;
    uint8_t  _bInterval;
};
} // namespace usb::descriptors