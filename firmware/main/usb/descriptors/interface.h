#pragma once

#include "../types.h"
#include <tuple>

namespace usb::descriptors
{
template <typename... Args>
struct Interface
{
    constexpr Interface(Class               bInterfaceClass,
                        uint8_t             bInterfaceSubClass,
                        uint8_t             bInterfaceProtocol,
                        uint8_t             iInterface,
                        std::tuple<Args...> endpoints)
        : _iInterface(iInterface),
          _bInterfaceClass(static_cast<uint8_t>(bInterfaceClass)),
          _bInterfaceSubClass(bInterfaceSubClass),
          _bInterfaceProtocol(bInterfaceProtocol),
          _endpoints(std::move(endpoints))
    {
    }

    constexpr Interface(Class   bInterfaceClass,
                        uint8_t bInterfaceSubClass,
                        uint8_t bInterfaceProtocol,
                        uint8_t iInterface,
                        Args... endpoints)
        : _iInterface(iInterface),
          _bInterfaceClass(static_cast<uint8_t>(bInterfaceClass)),
          _bInterfaceSubClass(bInterfaceSubClass),
          _bInterfaceProtocol(bInterfaceProtocol),
          _endpoints(std::make_tuple(endpoints...))
    {
    }

    static constexpr int DescriptorsSize()
    {
        return descriptorSize::interface + (Args::DescriptorsSize() + ...);
    }

    static constexpr int NumInterfaces() { return 1; }

    static constexpr int NumEndpoints() { return (Args::isEndpoint() + ...); }

    static constexpr auto getNumStrings() { return 1; }

    template <typename T>
    constexpr void fillDescriptor(T &array, int &idx, int &ifIdx, int &endpointAddress) const
    {
        array[idx++] = static_cast<uint8_t>(descriptorSize::interface);
        array[idx++] = static_cast<uint8_t>(DescriptorType::Interface);
        array[idx++] = static_cast<uint8_t>(ifIdx);
        array[idx++] = static_cast<uint8_t>(0); // TODO
        array[idx++] = static_cast<uint8_t>(NumEndpoints());
        array[idx++] = _bInterfaceClass;
        array[idx++] = _bInterfaceSubClass;
        array[idx++] = _bInterfaceProtocol;
        array[idx++] = _iInterface;
        ifIdx++;
        std::apply(
            [&array, &idx, &ifIdx, &endpointAddress](auto &&... args) {
                ((args.fillDescriptor(array, idx, ifIdx, endpointAddress)), ...);
            },
            _endpoints);
    }

    template <typename T>
    constexpr void fillOSCompatibleIDDescriptor(T &array, int &idx, int &ifIdx) const
    {
        ifIdx++;
    }

    template <typename T>
    constexpr void updateDescriptor(Speed speed, T &array, int &idx) const
    {
        idx += descriptorSize::interface;
        std::apply([&array, &idx,
                    speed](auto &&... args) { ((args.updateDescriptor(speed, array, idx)), ...); },
                   _endpoints);
    }

    static constexpr int isInterface() { return 1; }

private:
    uint8_t             _iInterface;
    uint8_t             _bInterfaceClass;
    uint8_t             _bInterfaceSubClass;
    uint8_t             _bInterfaceProtocol;
    std::tuple<Args...> _endpoints;
};
} // namespace usb::descriptors