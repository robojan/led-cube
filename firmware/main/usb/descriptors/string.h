#pragma once

#include <vector>
#include <string_view>
#include <cassert>
#include "../types.h"

namespace usb::descriptors
{
std::vector<uint8_t> to_string_descriptor(std::u16string_view str)
{
    std::vector<uint8_t> result(2 * (str.size() + 1) + 2);
    assert((str.size() + 1) * 2 < 254);
    auto i      = 0;
    result[i++] = result.size();
    result[i++] = static_cast<uint8_t>(DescriptorType::String);
    for(auto c : str)
    {
        result[i++] = (c >> 0) & 0xFF;
        result[i++] = (c >> 8) & 0xFF;
    }
    result[i++] = 0;
    result[i++] = 0;
    assert(i == str.size());
    return result;
}

std::vector<uint8_t> to_string_descriptor(std::string_view str)
{
    std::vector<uint8_t> result(2 * (str.size() + 1) + 2);
    assert((str.size() + 1) * 2 < 254);
    auto i      = 0;
    result[i++] = result.size();
    result[i++] = static_cast<uint8_t>(DescriptorType::String);
    for(auto c : str)
    {
        result[i++] = c & 0xFF;
        result[i++] = 0;
    }
    result[i++] = 0;
    result[i++] = 0;
    assert(i == str.size());
    return result;
}

std::vector<uint8_t> to_string_descriptor(std::initializer_list<LangID> supportedLanguages)
{
    std::vector<uint8_t> result(2 * (supportedLanguages.size() + 1) + 2);
    assert((supportedLanguages.size() + 1) * 2 < 254);
    auto i      = 0;
    result[i++] = result.size();
    result[i++] = static_cast<uint8_t>(DescriptorType::String);
    for(auto c : supportedLanguages)
    {
        result[i++] = (c >> 0) & 0xFF;
        result[i++] = (c >> 8) & 0xFF;
    }
    result[i++] = 0;
    result[i++] = 0;
    assert(i == supportedLanguages.size());
    return result;
}

} // namespace usb::descriptors