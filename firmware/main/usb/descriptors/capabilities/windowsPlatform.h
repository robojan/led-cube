#pragma once

#include "../../types.h"
#include <array>

namespace usb::descriptors
{
// For now this descriptor is hard coded to support only one windows version
struct WindowsPlatform
{
    constexpr WindowsPlatform(uint8_t  vendorCode,
                              uint16_t msosDescriptorSize,
                              uint8_t  altEnumCode             = 0,
                              uint32_t supportedWindowsVersion = WindowsVersions::Nt6_3)
        : _vendorCode(vendorCode),
          _supportedWindows(supportedWindowsVersion),
          _msOsDescriptorSetTotalLength(msosDescriptorSize),
          _bAltEnumCode(altEnumCode)
    {
    }

    constexpr int descriptorSize() const
    {
        return descriptorSize::platform + 1 * descriptorSize::windowsPlatformSetInformation;
    }

    template <typename T>
    constexpr void fillDescriptor(T &array, int &idx) const
    {
        array[idx++] = descriptorSize();
        array[idx++] = DescriptorType::DeviceCapability;
        array[idx++] = CapabilityType::Platform;
        array[idx++] = 0;
        for(auto x : _platformUUID)
            array[idx++] = x;

        // Descriptor set information
        array[idx++] = static_cast<uint8_t>((_supportedWindows >> 0) & 0xFF);
        array[idx++] = static_cast<uint8_t>((_supportedWindows >> 8) & 0xFF);
        array[idx++] = static_cast<uint8_t>((_supportedWindows >> 16) & 0xFF);
        array[idx++] = static_cast<uint8_t>((_supportedWindows >> 24) & 0xFF);
        array[idx++] = static_cast<uint8_t>((_msOsDescriptorSetTotalLength >> 0) & 0xFF);
        array[idx++] = static_cast<uint8_t>((_msOsDescriptorSetTotalLength >> 8) & 0xFF);
        array[idx++] = _vendorCode;
        array[idx++] = _bAltEnumCode;
    }

private:
    uint8_t                                  _vendorCode;
    uint32_t                                 _supportedWindows;
    uint16_t                                 _msOsDescriptorSetTotalLength;
    uint8_t                                  _bAltEnumCode;
    static constexpr std::array<uint8_t, 16> _platformUUID{0xDF, 0x60, 0xDD, 0xD8, 0x89, 0x45,
                                                           0xC7, 0x4C, 0x9C, 0xD2, 0x65, 0x9D,
                                                           0x9E, 0x64, 0x8A, 0x9F};
};

} // namespace usb::descriptors
