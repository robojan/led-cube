#pragma once

#include "../../types.h"

namespace usb::descriptors
{
struct Usb20Extension
{
    constexpr Usb20Extension(bool lpm)
    {
        _bmAttributes = 0;
        if(lpm)
            _bmAttributes |= (1 << 1);
    }

    constexpr int descriptorSize() const { return descriptorSize::usb20extension; }

    template <typename T>
    constexpr void fillDescriptor(T &array, int &idx) const
    {
        array[idx++] = descriptorSize;
        array[idx++] = DescriptorType::DeviceCapability;
        array[idx++] = CapabilityType::USB20Extension;
        array[idx++] = static_cast<uint8_t>((_bmAttributes >> 0) & 0xFF);
        array[idx++] = static_cast<uint8_t>((_bmAttributes >> 8) & 0xFF);
        array[idx++] = static_cast<uint8_t>((_bmAttributes >> 16) & 0xFF);
        array[idx++] = static_cast<uint8_t>((_bmAttributes >> 24) & 0xFF);
    }

private:
    uint32_t _bmAttributes = 0;
};

} // namespace usb::descriptors
