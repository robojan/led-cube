#pragma once

#include "../../types.h"

namespace usb::descriptors
{
struct PrecisionTimeMeasurement
{
    constexpr PrecisionTimeMeasurement() {}

    constexpr int descriptorSize() const { return descriptorSize::precisionTimeMeasurement; }

    template <typename T>
    constexpr void fillDescriptor(T &array, int &idx) const
    {
        array[idx++] = descriptorSize;
        array[idx++] = DescriptorType::DeviceCapability;
        array[idx++] = CapabilityType::PrecisionTimeMeasurement;
    }
};

} // namespace usb::descriptors
