#pragma once

#include "../../types.h"
#include <string_view>

namespace usb::descriptors
{
struct ContainerId
{
    constexpr ContainerId(UUID uuid) : _uuid(std::move(uuid)) {}

    constexpr int descriptorSize() const { return descriptorSize::containerId; }

    template <typename T>
    constexpr void fillDescriptor(T &array, int &idx) const
    {
        array[idx++] = descriptorSize;
        array[idx++] = DescriptorType::DeviceCapability;
        array[idx++] = CapabilityType::ContainerID;
        array[idx++] = 0;
        for(int i = 0; i < 16; i++)
        {
            array[idx++] = _uuid.uuid[i];
        }
    }

private:
    UUID _uuid;
};

} // namespace usb::descriptors
