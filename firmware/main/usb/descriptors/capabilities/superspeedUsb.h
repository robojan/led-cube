#pragma once

#include "../../types.h"
#include <cassert>

namespace usb::descriptors
{
struct SuperSpeedUsb
{
    constexpr SuperSpeedUsb(bool        lpm,
                            SpeedBitMap supportedSpeeds,
                            SpeedBitMap fullyFunctioningSpeed,
                            int         u1DeviceExitLatency,
                            int         u2DeviceExitLatency)
    {
        _bmAttributes    = lpm ? 0x01 : 0x00;
        _supportedSpeeds = supportedSpeeds;
        assert((fullyFunctioningSpeed & (fullyFunctioningSpeed - 1)) == 0);
        _bFunctionalitySupport = logb(fullyFunctioningSpeed);
        assert(u1DeviceExitLatency <= 10);
        _bU1DevExitLat = u1DeviceExitLatency;
        assert(u2DeviceExitLatency <= 0x7ff);
        _bU2DevExitLat = u2DeviceExitLatency;
    }

    constexpr int descriptorSize() const { return descriptorSize::superSpeedUsb; }

    template <typename T>
    constexpr void fillDescriptor(T &array, int &idx) const
    {
        array[idx++] = descriptorSize;
        array[idx++] = DescriptorType::DeviceCapability;
        array[idx++] = CapabilityType::SuperSpeedUSB;
        array[idx++] = _bmAttributes;
        array[idx++] = static_cast<uint8_t>((_supportedSpeeds >> 0) & 0xFF);
        array[idx++] = static_cast<uint8_t>((_supportedSpeeds >> 8) & 0xFF);
        array[idx++] = _bFunctionalitySupport;
        array[idx++] = _bU1DevExitLat;
        array[idx++] = static_cast<uint8_t>((_bU2DevExitLat >> 0) & 0xFF);
        array[idx++] = static_cast<uint8_t>((_bU2DevExitLat >> 8) & 0xFF);
    }

private:
    uint8_t  _bmAttributes          = 0;
    uint16_t _supportedSpeeds       = 0;
    uint8_t  _bFunctionalitySupport = 0;
    uint8_t  _bU1DevExitLat         = 0;
    uint16_t _bU2DevExitLat         = 0;

    static constexpr int logb(int index)
    {
        int targetlevel = 0;
        while(index >>= 1)
            ++targetlevel;
        return targetlevel;
    }
};

} // namespace usb::descriptors
