#pragma once

#include "../types.h"

namespace usb::descriptors
{
template <int N>
struct ClassSpecific_CDC_Union
{
    constexpr ClassSpecific_CDC_Union(std::array<int, N> interfaceOffsets)
        : _interfaceOffsets(interfaceOffsets)
    {
    }

    static constexpr int DescriptorsSize() { return 3 + N; }

    template <typename T>
    constexpr void fillDescriptor(T &array, int &idx, int &ifIdx, int endpointAddress) const
    {
        array[idx++] = static_cast<uint8_t>(DescriptorsSize());
        array[idx++] = static_cast<uint8_t>(DescriptorType::CS_Interface);
        array[idx++] = 0x06;
        for(int i = 0; i < N; i++)
        {
            array[idx++] = ifIdx + _interfaceOffsets[i];
        }
    }

    template <typename T>
    constexpr void updateDescriptor(Speed speed, T &array, int &idx) const
    {
        idx += DescriptorsSize();
    }
    static constexpr int isEndpoint() { return 0; }

private:
    std::array<int, N> _interfaceOffsets;
};
} // namespace usb::descriptors
