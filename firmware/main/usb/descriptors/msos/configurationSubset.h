#pragma once

#include "../../types.h"
#include <tuple>

namespace usb::descriptors::msos
{
template <typename... Args>
struct ConfigurationSubset
{
    constexpr ConfigurationSubset(uint8_t bConfigurationValue, std ::tuple<Args...> subsets)
        : _bConfigurationValue(bConfigurationValue), _subsets(std::move(subsets))
    {
    }

    constexpr ConfigurationSubset(uint8_t bConfigurationValue, Args... subsets)
        : _bConfigurationValue(bConfigurationValue), _subsets(std::make_tuple(subsets...))
    {
    }

    static constexpr int numSubsets() { return sizeof...(Args); }

    constexpr int descriptorSize() const
    {
        return descriptorSize::msos20ConfigurationSubsetHeader +
               std::apply([](auto &&... p) { return (p.descriptorSize() + ...); }, _subsets);
    }

    template <typename T>
    constexpr void fillDescriptor(T &array, int &idx) const
    {
        uint16_t wLength         = descriptorSize::msos20ConfigurationSubsetHeader;
        uint16_t wDescriptorType = MsOs20DescriptorType::SubsetHeaderConfiguration;
        uint16_t wTotalLength    = descriptorSize();
        array[idx++]             = static_cast<uint8_t>((wLength >> 0) & 0xFF);
        array[idx++]             = static_cast<uint8_t>((wLength >> 8) & 0xFF);
        array[idx++]             = static_cast<uint8_t>((wDescriptorType >> 0) & 0xFF);
        array[idx++]             = static_cast<uint8_t>((wDescriptorType >> 8) & 0xFF);
        array[idx++]             = _bConfigurationValue;
        array[idx++]             = 0;
        array[idx++]             = static_cast<uint8_t>((wTotalLength >> 0) & 0xFF);
        array[idx++]             = static_cast<uint8_t>((wTotalLength >> 8) & 0xFF);
        std::apply([&array, &idx](auto &&... args) { ((args.fillDescriptor(array, idx)), ...); },
                   _subsets);
    }

    constexpr int numCompatibleIdFunctions() const
    {
        return std::apply([](auto &&... p) { return (p.numCompatibleIdFunctions() + ...); },
                          _subsets);
    }

    template <typename T>
    constexpr void fillOs1CompatibleIdDescriptor(T &array, int &idx, int interface) const
    {
        std::apply(
            [&array, &idx, &interface](auto &&... args) {
                ((args.fillOs1CompatibleIdDescriptor(array, idx, interface)), ...);
            },
            _subsets);
    }

    constexpr int numProperties() const
    {
        return std::apply([](auto &&... p) { return (p.numProperties() + ...); }, _subsets);
    }

    constexpr int os1ExtendedPropertiesDescriptorSize() const
    {
        return std::apply(
            [](auto &&... p) { return (p.os1ExtendedPropertiesDescriptorSize() + ...); }, _subsets);
    }

    template <typename T>
    constexpr void fillOs1ExtendedPropertiesDescriptor(T &array, int &idx) const
    {
        std::apply(
            [&array, &idx](auto &&... args) {
                ((args.fillOs1ExtendedPropertiesDescriptor(array, idx)), ...);
            },
            _subsets);
    }


private:
    uint8_t             _bConfigurationValue;
    std::tuple<Args...> _subsets;
};

} // namespace usb::descriptors::msos
