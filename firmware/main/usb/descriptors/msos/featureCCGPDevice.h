#pragma once

#include "../../types.h"
#include <string_view>
#include <cassert>

namespace usb::descriptors::msos
{
struct FeatureCCGPDevice
{
    constexpr FeatureCCGPDevice() {}

    constexpr static int descriptorSize() { return descriptorSize::msos20FeatureCCGPDevice; }

    template <typename T>
    constexpr void fillDescriptor(T &array, int &idx) const
    {
        uint16_t wLength         = descriptorSize::msos20FeatureCCGPDevice;
        uint16_t wDescriptorType = MsOs20DescriptorType::FeatureCCGPDevice;
        array[idx++]             = static_cast<uint8_t>((wLength >> 0) & 0xFF);
        array[idx++]             = static_cast<uint8_t>((wLength >> 8) & 0xFF);
        array[idx++]             = static_cast<uint8_t>((wDescriptorType >> 0) & 0xFF);
        array[idx++]             = static_cast<uint8_t>((wDescriptorType >> 8) & 0xFF);
    }

    static constexpr int numCompatibleIdFunctions() { return 0; }

    template <typename T>
    constexpr void fillOs1CompatibleIdDescriptor(T &array, int &idx, int interface) const
    {
    }

    static constexpr int numProperties() { return 0; }

    static constexpr int os1ExtendedPropertiesDescriptorSize() { return 0; }

    template <typename T>
    static constexpr void fillOs1ExtendedPropertiesDescriptor(T &array, int &idx)
    {
    }
};

} // namespace usb::descriptors::msos
