#pragma once

#include "../../types.h"
#include <tuple>


namespace usb::descriptors::msos
{
template <typename... Args>
struct MsOs
{
    constexpr MsOs(uint32_t windowsVersion, std ::tuple<Args...> subsets)
        : _windowsVersion(windowsVersion), _subsets(std::move(subsets))
    {
    }

    constexpr MsOs(uint32_t windowsVersion, Args... subsets)
        : _windowsVersion(windowsVersion), _subsets(std::make_tuple(subsets...))
    {
    }

    static constexpr int numSubsets() { return sizeof...(Args); }

    constexpr int os2DescriptorSize() const
    {
        return descriptorSize::msos20SetHeader +
               std::apply([](auto &&... p) { return (p.descriptorSize() + ...); }, _subsets);
    }

    template <const MsOs &d>
    static constexpr auto getOs2Descriptor()
    {
        std::array<uint8_t, d.os2DescriptorSize()> descriptor{};
        d.fillOs2Descriptor(descriptor);
        return descriptor;
    }

    constexpr int numCompatibleIdFunctions() const
    {
        return std::apply([](auto &&... p) { return (p.numCompatibleIdFunctions() + ...); },
                          _subsets);
    }

    constexpr int os1CompatibleIdDescriptorSize() const
    {
        return descriptorSize::extendedCompatID +
               numCompatibleIdFunctions() * descriptorSize::extendedCompatIDFunction;
    }

    template <const MsOs &d>
    static constexpr auto getOs1CompatibleIdDescriptor()
    {
        std::array<uint8_t, d.os1CompatibleIdDescriptorSize()> descriptor{};
        d.fillOs1CompatibleIdDescriptor(descriptor);
        return descriptor;
    }

    constexpr int numProperties() const
    {
        return std::apply([](auto &&... p) { return (p.numProperties() + ...); }, _subsets);
    }

    constexpr int os1ExtendedPropertiesDescriptorSize() const
    {
        return descriptorSize::extendedProperties +
               std::apply(
                   [](auto &&... p) { return (p.os1ExtendedPropertiesDescriptorSize() + ...); },
                   _subsets);
    }

    template <const MsOs &d>
    static constexpr auto getOs1ExtendedPropertiesDescriptor()
    {
        std::array<uint8_t, d.os1ExtendedPropertiesDescriptorSize()> descriptor{};
        d.fillOs1ExtendedPropertiesDescriptor(descriptor);
        return descriptor;
    }

private:
    uint32_t            _windowsVersion;
    std::tuple<Args...> _subsets;

    template <typename T>
    constexpr void fillOs2Descriptor(T &array) const
    {
        int      idx             = 0;
        uint16_t wLength         = descriptorSize::msos20SetHeader;
        uint16_t wDescriptorType = MsOs20DescriptorType::SetHeader;
        uint16_t wTotalLength    = os2DescriptorSize();
        array[idx++]             = static_cast<uint8_t>((wLength >> 0) & 0xFF);
        array[idx++]             = static_cast<uint8_t>((wLength >> 8) & 0xFF);
        array[idx++]             = static_cast<uint8_t>((wDescriptorType >> 0) & 0xFF);
        array[idx++]             = static_cast<uint8_t>((wDescriptorType >> 8) & 0xFF);
        array[idx++]             = static_cast<uint8_t>((_windowsVersion >> 0) & 0xFF);
        array[idx++]             = static_cast<uint8_t>((_windowsVersion >> 8) & 0xFF);
        array[idx++]             = static_cast<uint8_t>((_windowsVersion >> 16) & 0xFF);
        array[idx++]             = static_cast<uint8_t>((_windowsVersion >> 24) & 0xFF);
        array[idx++]             = static_cast<uint8_t>((wTotalLength >> 0) & 0xFF);
        array[idx++]             = static_cast<uint8_t>((wTotalLength >> 8) & 0xFF);
        std::apply([&array, &idx](auto &&... args) { ((args.fillDescriptor(array, idx)), ...); },
                   _subsets);
    }


    template <typename T>
    constexpr void fillOs1CompatibleIdDescriptor(T &array) const
    {
        int      idx        = 0;
        uint32_t dwSize     = os1CompatibleIdDescriptorSize();
        uint16_t bcdVersion = make_BCD(1, 0, 0);
        uint16_t wIndex     = static_cast<uint16_t>(OSFeatureIdx::ExtendedCompatID);
        array[idx++]        = static_cast<uint8_t>((dwSize >> 0) & 0xFF);
        array[idx++]        = static_cast<uint8_t>((dwSize >> 8) & 0xFF);
        array[idx++]        = static_cast<uint8_t>((dwSize >> 16) & 0xFF);
        array[idx++]        = static_cast<uint8_t>((dwSize >> 24) & 0xFF);
        array[idx++]        = static_cast<uint8_t>((bcdVersion >> 0) & 0xFF);
        array[idx++]        = static_cast<uint8_t>((bcdVersion >> 8) & 0xFF);
        array[idx++]        = static_cast<uint8_t>((wIndex >> 0) & 0xFF);
        array[idx++]        = static_cast<uint8_t>((wIndex >> 8) & 0xFF);
        array[idx++]        = static_cast<uint8_t>(numCompatibleIdFunctions());
        array[idx++]        = 0x00;
        array[idx++]        = 0x00;
        array[idx++]        = 0x00;
        array[idx++]        = 0x00;
        array[idx++]        = 0x00;
        array[idx++]        = 0x00;
        array[idx++]        = 0x00;
        std::apply(
            [&array, &idx](auto &&... args) {
                ((args.fillOs1CompatibleIdDescriptor(array, idx, 0)), ...);
            },
            _subsets);
    }


    template <typename T>
    constexpr void fillOs1ExtendedPropertiesDescriptor(T &array) const
    {
        int      idx        = 0;
        uint32_t dwSize     = os1ExtendedPropertiesDescriptorSize();
        uint16_t bcdVersion = make_BCD(1, 0, 0);
        uint16_t wIndex     = static_cast<uint16_t>(OSFeatureIdx::ExtendedProperties);
        uint16_t wCount     = numProperties();
        array[idx++]        = static_cast<uint8_t>((dwSize >> 0) & 0xFF);
        array[idx++]        = static_cast<uint8_t>((dwSize >> 8) & 0xFF);
        array[idx++]        = static_cast<uint8_t>((dwSize >> 16) & 0xFF);
        array[idx++]        = static_cast<uint8_t>((dwSize >> 24) & 0xFF);
        array[idx++]        = static_cast<uint8_t>((bcdVersion >> 0) & 0xFF);
        array[idx++]        = static_cast<uint8_t>((bcdVersion >> 8) & 0xFF);
        array[idx++]        = static_cast<uint8_t>((wIndex >> 0) & 0xFF);
        array[idx++]        = static_cast<uint8_t>((wIndex >> 8) & 0xFF);
        array[idx++]        = static_cast<uint8_t>((wCount >> 0) & 0xFF);
        array[idx++]        = static_cast<uint8_t>((wCount >> 8) & 0xFF);
        std::apply(
            [&array, &idx](auto &&... args) {
                ((args.fillOs1ExtendedPropertiesDescriptor(array, idx)), ...);
            },
            _subsets);
    }
};

} // namespace usb::descriptors::msos
