#pragma once

#include "../../types.h"
#include <tuple>

namespace usb::descriptors::msos
{
template <typename... Args>
struct FunctionSubset
{
    constexpr FunctionSubset(uint8_t bFirstInterface, std ::tuple<Args...> features)
        : _bFirstInterface(bFirstInterface), _features(std::move(features))
    {
    }

    constexpr FunctionSubset(uint8_t bFirstInterface, Args... features)
        : _bFirstInterface(bFirstInterface), _features(std::make_tuple(features...))
    {
    }

    static constexpr int numSubsets() { return sizeof...(Args); }

    constexpr int descriptorSize() const
    {
        return descriptorSize::msos20FunctionSubsetHeader +
               std::apply([](auto &&... p) { return (p.descriptorSize() + ...); }, _features);
    }

    template <typename T>
    constexpr void fillDescriptor(T &array, int &idx) const
    {
        uint16_t wLength         = descriptorSize::msos20FunctionSubsetHeader;
        uint16_t wDescriptorType = MsOs20DescriptorType::SubsetHeaderFunction;
        uint16_t wTotalLength    = descriptorSize();
        array[idx++]             = static_cast<uint8_t>((wLength >> 0) & 0xFF);
        array[idx++]             = static_cast<uint8_t>((wLength >> 8) & 0xFF);
        array[idx++]             = static_cast<uint8_t>((wDescriptorType >> 0) & 0xFF);
        array[idx++]             = static_cast<uint8_t>((wDescriptorType >> 8) & 0xFF);
        array[idx++]             = _bFirstInterface;
        array[idx++]             = 0;
        array[idx++]             = static_cast<uint8_t>((wTotalLength >> 0) & 0xFF);
        array[idx++]             = static_cast<uint8_t>((wTotalLength >> 8) & 0xFF);
        std::apply([&array, &idx](auto &&... args) { ((args.fillDescriptor(array, idx)), ...); },
                   _features);
    }

    constexpr int numCompatibleIdFunctions() const
    {
        return std::apply([](auto &&... p) { return (p.numCompatibleIdFunctions() + ...); },
                          _features);
    }

    template <typename T>
    constexpr void fillOs1CompatibleIdDescriptor(T &array, int &idx, int interface) const
    {
        interface = _bFirstInterface;
        std::apply(
            [&array, &idx, &interface](auto &&... args) {
                ((args.fillOs1CompatibleIdDescriptor(array, idx, interface)), ...);
            },
            _features);
    }

    constexpr int numProperties() const
    {
        return std::apply([](auto &&... p) { return (p.numProperties() + ...); }, _features);
    }

    constexpr int os1ExtendedPropertiesDescriptorSize() const
    {
        return std::apply(
            [](auto &&... p) { return (p.os1ExtendedPropertiesDescriptorSize() + ...); },
            _features);
    }

    template <typename T>
    constexpr void fillOs1ExtendedPropertiesDescriptor(T &array, int &idx) const
    {
        std::apply(
            [&array, &idx](auto &&... args) {
                ((args.fillOs1ExtendedPropertiesDescriptor(array, idx)), ...);
            },
            _features);
    }


private:
    uint8_t             _bFirstInterface;
    std::tuple<Args...> _features;
};

} // namespace usb::descriptors::msos
