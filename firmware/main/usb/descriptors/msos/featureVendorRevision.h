#pragma once

#include "../../types.h"
#include <string_view>
#include <cassert>

namespace usb::descriptors::msos
{
struct FeatureVendorRevision
{
    constexpr FeatureVendorRevision(uint16_t revision) : _revision(revision) {}

    constexpr static int descriptorSize() { return descriptorSize::msos20FeatureVendorRevision; }

    template <typename T>
    constexpr void fillDescriptor(T &array, int &idx) const
    {
        uint16_t wLength         = descriptorSize::msos20FeatureVendorRevision;
        uint16_t wDescriptorType = MsOs20DescriptorType::FeatureVendorRevision;
        array[idx++]             = static_cast<uint8_t>((wLength >> 0) & 0xFF);
        array[idx++]             = static_cast<uint8_t>((wLength >> 8) & 0xFF);
        array[idx++]             = static_cast<uint8_t>((wDescriptorType >> 0) & 0xFF);
        array[idx++]             = static_cast<uint8_t>((wDescriptorType >> 8) & 0xFF);
        array[idx++]             = static_cast<uint8_t>((_revision >> 0) & 0xFF);
        array[idx++]             = static_cast<uint8_t>((_revision >> 8) & 0xFF);
    }

    static constexpr int numCompatibleIdFunctions() { return 0; }

    template <typename T>
    constexpr void fillOs1CompatibleIdDescriptor(T &array, int &idx, int interface) const
    {
    }

    static constexpr int numProperties() { return 0; }

    static constexpr int os1ExtendedPropertiesDescriptorSize() { return 0; }

    template <typename T>
    static constexpr void fillOs1ExtendedPropertiesDescriptor(T &array, int &idx)
    {
    }

private:
    uint16_t _revision;
};

} // namespace usb::descriptors::msos
