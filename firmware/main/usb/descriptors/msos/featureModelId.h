#pragma once

#include "../../types.h"
#include <string_view>
#include <cassert>

namespace usb::descriptors::msos
{
struct FeatureModelId
{
    constexpr FeatureModelId(UUID modelId) : _modelId(modelId) {}

    constexpr static int descriptorSize() { return descriptorSize::msos20FeatureModelID; }

    template <typename T>
    constexpr void fillDescriptor(T &array, int &idx) const
    {
        uint16_t wLength         = descriptorSize::msos20FeatureModelID;
        uint16_t wDescriptorType = MsOs20DescriptorType::FeatureModelId;
        array[idx++]             = static_cast<uint8_t>((wLength >> 0) & 0xFF);
        array[idx++]             = static_cast<uint8_t>((wLength >> 8) & 0xFF);
        array[idx++]             = static_cast<uint8_t>((wDescriptorType >> 0) & 0xFF);
        array[idx++]             = static_cast<uint8_t>((wDescriptorType >> 8) & 0xFF);
        for(auto v : _modelId.uuid)
            array[idx++] = v;
    }

    static constexpr int numCompatibleIdFunctions() { return 0; }

    template <typename T>
    constexpr void fillOs1CompatibleIdDescriptor(T &array, int &idx, int interface) const
    {
    }

    static constexpr int numProperties() { return 0; }

    static constexpr int os1ExtendedPropertiesDescriptorSize() { return 0; }

    template <typename T>
    static constexpr void fillOs1ExtendedPropertiesDescriptor(T &array, int &idx)
    {
    }

private:
    UUID _modelId;
};

} // namespace usb::descriptors::msos
