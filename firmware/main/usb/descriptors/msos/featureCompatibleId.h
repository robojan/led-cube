#pragma once

#include "../../types.h"
#include <string_view>
#include <cassert>

namespace usb::descriptors::msos
{
struct FeatureCompatibleId
{
    constexpr FeatureCompatibleId(std::string_view compatibleID,
                                  std::string_view subCompatibleID = "")
        : _compatibleID(), _subCompatibleID()
    {
        assert(compatibleID.size() < 8);
        assert(subCompatibleID.size() < 8);

        char *outIt = _compatibleID;
        for(auto inIt = compatibleID.begin(); inIt != compatibleID.end(); inIt++, outIt++)
        {
            *outIt = *inIt;
        }
        outIt = _subCompatibleID;
        for(auto inIt = subCompatibleID.begin(); inIt != subCompatibleID.end(); inIt++, outIt++)
        {
            *outIt = *inIt;
        }
    }

    constexpr static int descriptorSize() { return descriptorSize::msos20FeatureCompatibleId; }

    template <typename T>
    constexpr void fillDescriptor(T &array, int &idx) const
    {
        uint16_t wLength         = descriptorSize::msos20FeatureCompatibleId;
        uint16_t wDescriptorType = MsOs20DescriptorType::FeatureCompatibleId;
        array[idx++]             = static_cast<uint8_t>((wLength >> 0) & 0xFF);
        array[idx++]             = static_cast<uint8_t>((wLength >> 8) & 0xFF);
        array[idx++]             = static_cast<uint8_t>((wDescriptorType >> 0) & 0xFF);
        array[idx++]             = static_cast<uint8_t>((wDescriptorType >> 8) & 0xFF);
        for(int i = 0; i < 8; i++)
        {
            array[idx++] = _compatibleID[i];
        }
        for(int i = 0; i < 8; i++)
        {
            array[idx++] = _subCompatibleID[i];
        }
    }

    static constexpr int numCompatibleIdFunctions() { return 1; }

    template <typename T>
    constexpr void fillOs1CompatibleIdDescriptor(T &array, int &idx, int interface) const
    {
        array[idx++] = interface;
        array[idx++] = 0x01;
        for(int i = 0; i < 8; i++)
        {
            array[idx++] = _compatibleID[i];
        }
        for(int i = 0; i < 8; i++)
        {
            array[idx++] = _subCompatibleID[i];
        }
        for(int i = 0; i < 6; i++)
        {
            array[idx++] = 0;
        }
    }

    static constexpr int numProperties() { return 0; }

    static constexpr int os1ExtendedPropertiesDescriptorSize() { return 0; }

    template <typename T>
    static constexpr void fillOs1ExtendedPropertiesDescriptor(T &array, int &idx)
    {
    }

private:
    char _compatibleID[8];
    char _subCompatibleID[8];
};

} // namespace usb::descriptors::msos
