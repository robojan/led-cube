#pragma once

#include "../../types.h"
#include <string_view>
#include <cassert>

namespace usb::descriptors::msos
{
struct BaseProperty
{
    constexpr BaseProperty(std::u16string_view property, ExtendedPropertyType type)
        : _name(property), _type(type)
    {
    }

    constexpr int baseDescriptorSize() const { return 2 + 2 + 2 + 2 + 2 * (_name.size() + 1) + 2; }

    template <typename T>
    constexpr void fillBaseDescriptor(T &array, int &idx, uint16_t wPropertyDataLength) const
    {
        uint16_t wLength             = baseDescriptorSize() + wPropertyDataLength;
        uint16_t wDescriptorType     = MsOs20DescriptorType::FeatureRegProperty;
        uint16_t wPropertyDataType   = static_cast<uint16_t>(_type);
        uint16_t wPropertyNameLength = static_cast<uint16_t>(2 * (_name.size() + 1));

        array[idx++] = static_cast<uint8_t>((wLength >> 0) & 0xFF);
        array[idx++] = static_cast<uint8_t>((wLength >> 8) & 0xFF);
        array[idx++] = static_cast<uint8_t>((wDescriptorType >> 0) & 0xFF);
        array[idx++] = static_cast<uint8_t>((wDescriptorType >> 8) & 0xFF);
        array[idx++] = static_cast<uint8_t>((wPropertyDataType >> 0) & 0xFF);
        array[idx++] = static_cast<uint8_t>((wPropertyDataType >> 8) & 0xFF);
        array[idx++] = static_cast<uint8_t>((wPropertyNameLength >> 0) & 0xFF);
        array[idx++] = static_cast<uint8_t>((wPropertyNameLength >> 8) & 0xFF);
        for(char16_t c : _name)
        {
            array[idx++] = static_cast<uint8_t>((c >> 0) & 0xFF);
            array[idx++] = static_cast<uint8_t>((c >> 8) & 0xFF);
        }
        array[idx++] = 0;
        array[idx++] = 0;
        array[idx++] = static_cast<uint8_t>((wPropertyDataLength >> 0) & 0xFF);
        array[idx++] = static_cast<uint8_t>((wPropertyDataLength >> 8) & 0xFF);
    }

    static constexpr int numCompatibleIdFunctions() { return 0; }

    template <typename T>
    constexpr void fillOs1CompatibleIdDescriptor(T &array, int &idx, int interface) const
    {
    }

    constexpr int os1BaseDescriptorSize() const { return 4 + 4 + 2 + 2 * (_name.size() + 1) + 4; }

    template <typename T>
    constexpr void fillOs1BaseDescriptor(T &array, int &idx, uint32_t dwPropertyDataLength) const
    {
        uint32_t dwSize              = os1BaseDescriptorSize() + dwPropertyDataLength;
        uint32_t dwPropertyDataType  = static_cast<uint32_t>(_type);
        uint16_t wPropertyNameLength = static_cast<uint16_t>(2 * (_name.size() + 1));
        array[idx++]                 = static_cast<uint8_t>((dwSize >> 0) & 0xFF);
        array[idx++]                 = static_cast<uint8_t>((dwSize >> 8) & 0xFF);
        array[idx++]                 = static_cast<uint8_t>((dwSize >> 16) & 0xFF);
        array[idx++]                 = static_cast<uint8_t>((dwSize >> 24) & 0xFF);
        array[idx++]                 = static_cast<uint8_t>((dwPropertyDataType >> 0) & 0xFF);
        array[idx++]                 = static_cast<uint8_t>((dwPropertyDataType >> 8) & 0xFF);
        array[idx++]                 = static_cast<uint8_t>((dwPropertyDataType >> 16) & 0xFF);
        array[idx++]                 = static_cast<uint8_t>((dwPropertyDataType >> 24) & 0xFF);
        array[idx++]                 = static_cast<uint8_t>((wPropertyNameLength >> 0) & 0xFF);
        array[idx++]                 = static_cast<uint8_t>((wPropertyNameLength >> 8) & 0xFF);
        for(char16_t c : _name)
        {
            array[idx++] = static_cast<uint8_t>((c >> 0) & 0xFF);
            array[idx++] = static_cast<uint8_t>((c >> 8) & 0xFF);
        }
        array[idx++] = 0;
        array[idx++] = 0;
        array[idx++] = static_cast<uint8_t>((dwPropertyDataLength >> 0) & 0xFF);
        array[idx++] = static_cast<uint8_t>((dwPropertyDataLength >> 8) & 0xFF);
        array[idx++] = static_cast<uint8_t>((dwPropertyDataLength >> 16) & 0xFF);
        array[idx++] = static_cast<uint8_t>((dwPropertyDataLength >> 24) & 0xFF);
    }

    static constexpr int numProperties() { return 1; }

private:
    std::u16string_view  _name;
    ExtendedPropertyType _type;
};

struct FeaturePropertyREG_SZ : BaseProperty
{
    constexpr FeaturePropertyREG_SZ(std::u16string_view property, std::u16string_view data)
        : BaseProperty(property, ExtendedPropertyType::REG_SZ), _data(data)
    {
    }

    constexpr int dataSize() const { return 2 * (_data.size() + 1); }
    constexpr int descriptorSize() const { return baseDescriptorSize() + dataSize(); }

    template <typename T>
    constexpr void fillDescriptor(T &array, int &idx) const
    {
        fillBaseDescriptor(array, idx, dataSize());
        for(char16_t c : _data)
        {
            array[idx++] = static_cast<uint8_t>((c >> 0) & 0xFF);
            array[idx++] = static_cast<uint8_t>((c >> 8) & 0xFF);
        }
        array[idx++] = 0;
        array[idx++] = 0;
    }

    constexpr int os1ExtendedPropertiesDescriptorSize() const
    {
        return os1BaseDescriptorSize() + dataSize();
    }

    template <typename T>
    constexpr void fillOs1ExtendedPropertiesDescriptor(T &array, int &idx) const
    {
        fillOs1BaseDescriptor(array, idx, dataSize());
        for(char16_t c : _data)
        {
            array[idx++] = static_cast<uint8_t>((c >> 0) & 0xFF);
            array[idx++] = static_cast<uint8_t>((c >> 8) & 0xFF);
        }
        array[idx++] = 0;
        array[idx++] = 0;
    }

private:
    std::u16string_view _data;
};

struct FeaturePropertyREG_EXPAND_SZ : BaseProperty
{
    constexpr FeaturePropertyREG_EXPAND_SZ(std::u16string_view property, std::u16string_view data)
        : BaseProperty(property, ExtendedPropertyType::REG_EXPAND_SZ), _data(data)
    {
    }

    constexpr int dataSize() const { return 2 * (_data.size() + 1); }
    constexpr int descriptorSize() const { return baseDescriptorSize() + dataSize(); }

    template <typename T>
    constexpr void fillDescriptor(T &array, int &idx) const
    {
        fillBaseDescriptor(array, idx, dataSize());
        for(char16_t c : _data)
        {
            array[idx++] = static_cast<uint8_t>((c >> 0) & 0xFF);
            array[idx++] = static_cast<uint8_t>((c >> 8) & 0xFF);
        }
        array[idx++] = 0;
        array[idx++] = 0;
    }

    constexpr int os1ExtendedPropertiesDescriptorSize() const
    {
        return os1BaseDescriptorSize() + dataSize();
    }

    template <typename T>
    constexpr void fillOs1ExtendedPropertiesDescriptor(T &array, int &idx) const
    {
        fillOs1BaseDescriptor(array, idx, dataSize());
        for(char16_t c : _data)
        {
            array[idx++] = static_cast<uint8_t>((c >> 0) & 0xFF);
            array[idx++] = static_cast<uint8_t>((c >> 8) & 0xFF);
        }
        array[idx++] = 0;
        array[idx++] = 0;
    }

private:
    std::u16string_view _data;
};

struct FeaturePropertyREG_BINARY : BaseProperty
{
    constexpr FeaturePropertyREG_BINARY(std::u16string_view            property,
                                        std::initializer_list<uint8_t> data)
        : BaseProperty(property, ExtendedPropertyType::REG_BINARY), _data(data)
    {
    }

    constexpr int dataSize() const { return _data.size(); }
    constexpr int descriptorSize() const { return baseDescriptorSize() + dataSize(); }

    template <typename T>
    constexpr void fillDescriptor(T &array, int &idx) const
    {
        fillBaseDescriptor(array, idx, dataSize());
        for(uint8_t v : _data)
        {
            array[idx++] = v;
        }
    }

    constexpr int os1ExtendedPropertiesDescriptorSize() const
    {
        return os1BaseDescriptorSize() + dataSize();
    }

    template <typename T>
    constexpr void fillOs1ExtendedPropertiesDescriptor(T &array, int &idx) const
    {
        fillOs1BaseDescriptor(array, idx, dataSize());
        for(uint8_t v : _data)
        {
            array[idx++] = v;
        }
    }

private:
    std::initializer_list<uint8_t> _data;
};

struct FeaturePropertyREG_DWORD_LITTLE_ENDIAN : BaseProperty
{
    constexpr FeaturePropertyREG_DWORD_LITTLE_ENDIAN(std::u16string_view property, uint32_t data)
        : BaseProperty(property, ExtendedPropertyType::REG_DWORD_LITTLE_ENDIAN), _data(data)
    {
    }

    constexpr int dataSize() const { return 4; }
    constexpr int descriptorSize() const { return baseDescriptorSize() + dataSize(); }

    template <typename T>
    constexpr void fillDescriptor(T &array, int &idx) const
    {
        fillBaseDescriptor(array, idx, dataSize());
        array[idx++] = static_cast<uint8_t>((_data >> 0) & 0xFF);
        array[idx++] = static_cast<uint8_t>((_data >> 8) & 0xFF);
        array[idx++] = static_cast<uint8_t>((_data >> 16) & 0xFF);
        array[idx++] = static_cast<uint8_t>((_data >> 24) & 0xFF);
    }

    constexpr int os1ExtendedPropertiesDescriptorSize() const
    {
        return os1BaseDescriptorSize() + dataSize();
    }

    template <typename T>
    constexpr void fillOs1ExtendedPropertiesDescriptor(T &array, int &idx) const
    {
        fillOs1BaseDescriptor(array, idx, dataSize());
        array[idx++] = static_cast<uint8_t>((_data >> 0) & 0xFF);
        array[idx++] = static_cast<uint8_t>((_data >> 8) & 0xFF);
        array[idx++] = static_cast<uint8_t>((_data >> 16) & 0xFF);
        array[idx++] = static_cast<uint8_t>((_data >> 24) & 0xFF);
    }

private:
    uint32_t _data;
};

struct FeaturePropertyREG_DWORD_BIG_ENDIAN : BaseProperty
{
    constexpr FeaturePropertyREG_DWORD_BIG_ENDIAN(std::u16string_view property, uint32_t data)
        : BaseProperty(property, ExtendedPropertyType::REG_DWORD_BIG_ENDIAN), _data(data)
    {
    }

    constexpr int dataSize() const { return 4; }
    constexpr int descriptorSize() const { return baseDescriptorSize() + dataSize(); }

    template <typename T>
    constexpr void fillDescriptor(T &array, int &idx) const
    {
        fillBaseDescriptor(array, idx, dataSize());
        array[idx++] = static_cast<uint8_t>((_data >> 24) & 0xFF);
        array[idx++] = static_cast<uint8_t>((_data >> 16) & 0xFF);
        array[idx++] = static_cast<uint8_t>((_data >> 8) & 0xFF);
        array[idx++] = static_cast<uint8_t>((_data >> 0) & 0xFF);
    }

    constexpr int os1ExtendedPropertiesDescriptorSize() const
    {
        return os1BaseDescriptorSize() + dataSize();
    }

    template <typename T>
    constexpr void fillOs1ExtendedPropertiesDescriptor(T &array, int &idx) const
    {
        fillOs1BaseDescriptor(array, idx, dataSize());
        array[idx++] = static_cast<uint8_t>((_data >> 24) & 0xFF);
        array[idx++] = static_cast<uint8_t>((_data >> 16) & 0xFF);
        array[idx++] = static_cast<uint8_t>((_data >> 8) & 0xFF);
        array[idx++] = static_cast<uint8_t>((_data >> 0) & 0xFF);
    }


private:
    uint32_t _data;
};

struct FeaturePropertyREG_LINK : BaseProperty
{
    constexpr FeaturePropertyREG_LINK(std::u16string_view property, std::u16string_view data)
        : BaseProperty(property, ExtendedPropertyType::REG_LINK), _data(data)
    {
    }

    constexpr int dataSize() const { return 2 * (_data.size() + 1); }
    constexpr int descriptorSize() const { return baseDescriptorSize() + dataSize(); }

    template <typename T>
    constexpr void fillDescriptor(T &array, int &idx) const
    {
        fillBaseDescriptor(array, idx, dataSize());
        for(char16_t c : _data)
        {
            array[idx++] = static_cast<uint8_t>((c >> 0) & 0xFF);
            array[idx++] = static_cast<uint8_t>((c >> 8) & 0xFF);
        }
        array[idx++] = 0;
        array[idx++] = 0;
    }

    constexpr int os1ExtendedPropertiesDescriptorSize() const
    {
        return os1BaseDescriptorSize() + dataSize();
    }

    template <typename T>
    constexpr void fillOs1ExtendedPropertiesDescriptor(T &array, int &idx) const
    {
        fillOs1BaseDescriptor(array, idx, dataSize());
        for(char16_t c : _data)
        {
            array[idx++] = static_cast<uint8_t>((c >> 0) & 0xFF);
            array[idx++] = static_cast<uint8_t>((c >> 8) & 0xFF);
        }
        array[idx++] = 0;
        array[idx++] = 0;
    }

private:
    std::u16string_view _data;
};

struct FeaturePropertyREG_MULTI_SZ : BaseProperty
{
    constexpr FeaturePropertyREG_MULTI_SZ(std::u16string_view                        property,
                                          std::initializer_list<std::u16string_view> data)
        : BaseProperty(property, ExtendedPropertyType::REG_MULTI_SZ), _data(data)
    {
    }

    constexpr int dataSize() const
    {
        int size = 2;
        for(auto &str : _data)
        {
            size += 2 * (str.size() + 1);
        }
        return size;
    }
    constexpr int descriptorSize() const { return baseDescriptorSize() + dataSize(); }

    template <typename T>
    constexpr void fillDescriptor(T &array, int &idx) const
    {
        fillBaseDescriptor(array, idx, dataSize());
        for(auto &str : _data)
        {
            for(char16_t c : str)
            {
                array[idx++] = static_cast<uint8_t>((c >> 0) & 0xFF);
                array[idx++] = static_cast<uint8_t>((c >> 8) & 0xFF);
            }
            array[idx++] = 0;
            array[idx++] = 0;
        }
        array[idx++] = 0;
        array[idx++] = 0;
    }

    constexpr int os1ExtendedPropertiesDescriptorSize() const
    {
        return os1BaseDescriptorSize() + dataSize();
    }

    template <typename T>
    constexpr void fillOs1ExtendedPropertiesDescriptor(T &array, int &idx) const
    {
        fillOs1BaseDescriptor(array, idx, dataSize());
        for(auto &str : _data)
        {
            for(char16_t c : str)
            {
                array[idx++] = static_cast<uint8_t>((c >> 0) & 0xFF);
                array[idx++] = static_cast<uint8_t>((c >> 8) & 0xFF);
            }
            array[idx++] = 0;
            array[idx++] = 0;
        }
        array[idx++] = 0;
        array[idx++] = 0;
    }

private:
    std::initializer_list<std::u16string_view> _data;
};

} // namespace usb::descriptors::msos
