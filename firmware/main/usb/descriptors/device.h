#pragma once

#include "../types.h"
#include <tuple>

namespace usb::descriptors
{
template <typename... Args>
struct Device
{
    constexpr Device(Version             bcdUSB,
                     Class               bDeviceClass,
                     uint8_t             bDeviceSubClass,
                     uint8_t             bDeviceProtocol,
                     MaxPacketSize       bMaxPacketSize0,
                     uint16_t            idVendor,
                     uint16_t            idProduct,
                     BCD                 bcdDevice,
                     uint8_t             iManufacturer,
                     uint8_t             iProduct,
                     uint8_t             iSerialNumber,
                     std::tuple<Args...> t)
        : _elems(std::move(t)),
          _bcdUSB(bcdUSB),
          _bDeviceClass(bDeviceClass),
          _bDeviceSubClass(bDeviceSubClass),
          _bDeviceProtocol(bDeviceProtocol),
          _bMaxPacketSize0(bMaxPacketSize0),
          _idVendor(idVendor),
          _idProduct(idProduct),
          _bcdDevice(bcdDevice),
          _iManufacturer(iManufacturer),
          _iProduct(iProduct),
          _iSerialNumber(iSerialNumber)
    {
    }
    constexpr Device(Version       bcdUSB,
                     Class         bDeviceClass,
                     uint8_t       bDeviceSubClass,
                     uint8_t       bDeviceProtocol,
                     MaxPacketSize bMaxPacketSize0,
                     uint16_t      idVendor,
                     uint16_t      idProduct,
                     BCD           bcdDevice,
                     uint8_t       iManufacturer,
                     uint8_t       iProduct,
                     uint8_t       iSerialNumber,
                     Args... t)
        : _elems(std::make_tuple(t...)),
          _bcdUSB(bcdUSB),
          _bDeviceClass(bDeviceClass),
          _bDeviceSubClass(bDeviceSubClass),
          _bDeviceProtocol(bDeviceProtocol),
          _bMaxPacketSize0(bMaxPacketSize0),
          _idVendor(idVendor),
          _idProduct(idProduct),
          _bcdDevice(bcdDevice),
          _iManufacturer(iManufacturer),
          _iProduct(iProduct),
          _iSerialNumber(iSerialNumber)
    {
    }

    static constexpr int configurationDescriptorsSize() { return (Args::DescriptorsSize() + ...); }

    template <typename T>
    auto operator+(T elem) const
    {
        auto elems{std::tuple_cat(_elems, std::make_tuple(elem))};
        return Device<Args..., T>(_bcdUSB, _bDeviceClass, _bDeviceSubClass, _bDeviceProtocol,
                                  _bMaxPacketSize0, _idVendor, _idProduct, _bcdDevice,
                                  _iManufacturer, _iProduct, _iSerialNumber, elems);
    }

    template <int Configuration>
    constexpr auto getClassConfig() const
    {
        return std::get<Configuration>(_elems).getServices();
    }

    template <int Configuration>
    constexpr auto getConfiguration() const
    {
        return std::get<Configuration>(_elems);
    }

    static constexpr auto getNumConfigurations() { return sizeof...(Args); }

    constexpr auto getConfigurationDescriptors() const
    {
        std::array<uint8_t, configurationDescriptorsSize()> descriptor{};
        fillConfigurationDescriptor(descriptor);
        return descriptor;
    }

    auto getConfigurationDescriptorOffset(int idx) const
    {
        return getConfigurationDescriptorOffsetFromTuple<0>(idx, 0);
    }

    auto updateConfigurationDescriptors(
        Speed                                                speed,
        std::array<uint8_t, configurationDescriptorsSize()> &descriptors) const
    {
        int idx = 0;
        std::apply(
            [&descriptors, &idx, speed](auto &&... args) {
                ((args.updateDescriptor(speed, descriptors, idx)), ...);
            },
            _elems);
    }

    constexpr auto getDeviceDescriptor() const
    {
        auto descriptor = std::array<uint8_t, descriptorSize::device>{
            static_cast<uint8_t>(descriptorSize::device),
            static_cast<uint8_t>(DescriptorType::Device),
            static_cast<uint8_t>(static_cast<uint16_t>(_bcdUSB) & 0xFF),
            static_cast<uint8_t>((static_cast<uint16_t>(_bcdUSB) >> 8) & 0xFF),
            static_cast<uint8_t>(_bDeviceClass),
            static_cast<uint8_t>(_bDeviceSubClass),
            static_cast<uint8_t>(_bDeviceProtocol),
            static_cast<uint8_t>(_bMaxPacketSize0),
            static_cast<uint8_t>(_idVendor & 0xFF),
            static_cast<uint8_t>((_idVendor >> 8) & 0xFF),
            static_cast<uint8_t>(_idProduct),
            static_cast<uint8_t>((_idProduct >> 8) & 0xFF),
            static_cast<uint8_t>(_bcdDevice & 0xFF),
            static_cast<uint8_t>((_bcdDevice >> 8) & 0xFF),
            static_cast<uint8_t>(_iManufacturer),
            static_cast<uint8_t>(_iProduct),
            static_cast<uint8_t>(_iSerialNumber),
            static_cast<uint8_t>(getNumConfigurations())};
        return descriptor;
    }

private:
    std::tuple<Args...> _elems;
    Version             _bcdUSB;
    Class               _bDeviceClass;
    uint8_t             _bDeviceSubClass;
    uint8_t             _bDeviceProtocol;
    MaxPacketSize       _bMaxPacketSize0;
    uint16_t            _idVendor;
    uint16_t            _idProduct;
    BCD                 _bcdDevice;
    uint8_t             _iManufacturer;
    uint8_t             _iProduct;
    uint8_t             _iSerialNumber;

    template <typename T>
    constexpr void fillConfigurationDescriptor(T &array) const
    {
        int idx       = 0;
        int configIdx = 1;
        std::apply([&array, &idx, &configIdx](
                       auto &&... args) { ((args.fillDescriptor(array, idx, configIdx++)), ...); },
                   _elems);
    }

    template <int I, int maxI>
    constexpr auto getConfigurationFromTuple(int idx)
    {
        if constexpr(I == maxI)
        {
            throw std::range_error("Index out of range");
        }
        else
        {
            if(idx == I)
            {
                return std::get<I>(_elems);
            }
            else
            {
                return getConfigurationFromTuple<I + 1, maxI>(idx);
            }
        }
    }

    template <int I>
    static constexpr int getConfigurationDescriptorOffsetFromTuple(int idx, int offset)
    {
        if constexpr(I == getNumConfigurations())
        {
            return 0;
        }
        else
        {
            if(idx == I)
            {
                return offset;
            }
            else
            {
                auto configDescriptorSize =
                    std::tuple_element<I, std::tuple<Args...>>::type::DescriptorsSize();
                return getConfigurationDescriptorOffsetFromTuple<I + 1>(
                    idx, offset + configDescriptorSize);
            }
        }
    }
};
} // namespace usb::descriptors