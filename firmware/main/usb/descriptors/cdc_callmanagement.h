#pragma once

#include "../types.h"

namespace usb::descriptors
{
struct ClassSpecific_CDC_CallManagement
{
    constexpr ClassSpecific_CDC_CallManagement(uint8_t bmCapabilities, int8_t dataInterfaceOffset)
        : _bmCapabilities(bmCapabilities), _dataInterfaceOffset(dataInterfaceOffset)
    {
    }

    static constexpr int DescriptorsSize() { return 5; }

    template <typename T>
    constexpr void fillDescriptor(T &array, int &idx, int &ifIdx, int endpointAddress) const
    {
        array[idx++] = static_cast<uint8_t>(DescriptorsSize());
        array[idx++] = static_cast<uint8_t>(DescriptorType::CS_Interface);
        array[idx++] = 0x01;
        array[idx++] = _bmCapabilities;
        array[idx++] = ifIdx + _dataInterfaceOffset;
    }

    template <typename T>
    constexpr void updateDescriptor(Speed speed, T &array, int &idx) const
    {
        idx += DescriptorsSize();
    }
    static constexpr int isEndpoint() { return 0; }

private:
    uint8_t _bmCapabilities;
    uint8_t _dataInterfaceOffset;
};
} // namespace usb::descriptors
