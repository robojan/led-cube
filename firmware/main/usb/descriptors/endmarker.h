#pragma once

#include "../types.h"

namespace usb::descriptors
{
struct EndMarker
{
    static constexpr int DescriptorsSize() { return 0; }
    template <typename T>
    constexpr void fillDescriptor(T &array, int &idx, int &ifIdx, int &endpointAddress) const
    {
    }
    template <typename T>
    constexpr void updateDescriptor(Speed speed, T &array, int &idx) const
    {
    }
    static constexpr int isInterface() { return 0; }
    static constexpr int NumInterfaces() { return 0; }
};
} // namespace usb::descriptors
