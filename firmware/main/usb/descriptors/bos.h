#pragma once

#include "../types.h"
#include <tuple>

namespace usb::descriptors
{
template <typename... Args>
struct BOS
{
    constexpr BOS(std::tuple<Args...> caps) : _caps(std::move(caps)) {}

    constexpr BOS(Args... caps) : _caps(std::make_tuple(caps...)) {}

    static constexpr int numCapabilities() { return sizeof...(Args); }
    static_assert(numCapabilities() < 256, "A maximum of 255 capabilities are supported.");

    constexpr int descriptorSize() const
    {
        return descriptorSize::bos +
               std::apply([](auto &&... p) { return (p.descriptorSize() + ...); }, _caps);
    }

    template <const BOS &d>
    static constexpr auto getDescriptor()
    {
        std::array<uint8_t, d.descriptorSize()> descriptor{};
        d.fillDescriptor(descriptor);
        return descriptor;
    }

private:
    std::tuple<Args...> _caps;

    template <typename T>
    constexpr void fillDescriptor(T &array) const
    {
        int      idx          = 0;
        uint16_t wTotalLength = descriptorSize();
        array[idx++]          = descriptorSize::bos;
        array[idx++]          = DescriptorType::BOS;
        array[idx++]          = static_cast<uint8_t>((wTotalLength >> 0) & 0xFF);
        array[idx++]          = static_cast<uint8_t>((wTotalLength >> 8) & 0xFF);
        array[idx++]          = numCapabilities();
        std::apply([&array, &idx](auto &&... args) { ((args.fillDescriptor(array, idx)), ...); },
                   _caps);
    }
};

} // namespace usb::descriptors
