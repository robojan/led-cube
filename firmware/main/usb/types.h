#pragma once

#include <cstdint>
#include "languages.h"

namespace usb
{
using BCD = uint16_t;

constexpr BCD make_BCD(uint8_t major, uint8_t minor, uint8_t subminor)
{
    return ((major & 0xFF) << 8) | ((minor & 0xF) << 4) | ((subminor & 0xF) << 0);
}

using LangID = uint16_t;

constexpr LangID make_LangID(MainLanguageID lang, uint8_t subLang)
{
    return ((subLang & 0xFF) << 8) | (lang & 0xff);
}


enum class LanguageIDs : LangID
{
    en_US = make_LangID(MainLanguageID::en, 0x09),
};

enum class Class : uint8_t
{
    InterfaceSpecific   = 0x00,
    Audio               = 0x01,
    CDC                 = 0x02,
    HID                 = 0x03,
    Physical            = 0x05,
    Image               = 0x06,
    Printer             = 0x07,
    MassStorage         = 0x08,
    Hub                 = 0x09,
    CDC_Data            = 0x0A,
    SmartCard           = 0x0B,
    ContentSecurity     = 0x0D,
    Video               = 0x0E,
    PersonalHealthCare  = 0x0F,
    AudioVideo          = 0x10,
    BillBoard           = 0x11,
    TypeCBridge         = 0x12,
    Diagnostic          = 0xDC,
    WirelessController  = 0xE0,
    Miscellaneous       = 0xEF,
    ApplicationSpecific = 0xFE,
    VendorSpecific      = 0xFF
};

struct SubClass
{
    enum
    {
        Common = 0x02
    };
};

struct Protocol
{
    enum
    {
        Interface_Association = 0x01
    };
};

enum class Version : BCD
{
    V200 = make_BCD(2, 0, 0),
    V210 = make_BCD(2, 1, 0),
};

struct DescriptorType
{
    enum : uint8_t
    {
        Device                     = 0x01,
        Configuration              = 0x02,
        String                     = 0x03,
        Interface                  = 0x04,
        Endpoint                   = 0x05,
        DeviceQualifier            = 0x06,
        OtherSpeedConfiguration    = 0x07,
        InterfacePower             = 0x08,
        OTG                        = 0x09,
        Debug                      = 0x0a,
        InterfaceAssociation       = 0x0b,
        BOS                        = 0x0f,
        DeviceCapability           = 0x10,
        CS_Interface               = 0x24,
        CS_Endpoint                = 0x25,
        SS_USB_Endpoint_Companion  = 0x30,
        SSP_ISO_Endpoint_Companion = 0x31,
    };
};

struct CapabilityType
{
    enum : uint8_t
    {
        WirelessUSB              = 0x01,
        USB20Extension           = 0x02,
        SuperSpeedUSB            = 0x03,
        ContainerID              = 0x04,
        Platform                 = 0x05,
        PowerDeliveryCapability  = 0x06,
        BatteryInfoCapability    = 0x07,
        PDConsumerPortCapability = 0x08,
        PDProviderPortCapability = 0x09,
        SuperSpeedPlus           = 0x0a,
        PrecisionTimeMeasurement = 0x0b,
        WirelessUsbExt           = 0x0c,
        Billboard                = 0x0d,
        Authentication           = 0x0e,
        BillboardEx              = 0x0f,
        ConfigurationSummary     = 0x10,
    };
};

enum class OSFeatureIdx
{
    Genre                   = 0x0001,
    ExtendedCompatID        = 0x0004,
    ExtendedProperties      = 0x0005,
    MSOS20Descriptor        = 0x0007,
    MSOS20SetAltEnumeration = 0x0008,
};

enum class ExtendedPropertyType
{
    REG_SZ                  = 1,
    REG_EXPAND_SZ           = 2,
    REG_BINARY              = 3,
    REG_DWORD_LITTLE_ENDIAN = 4,
    REG_DWORD_BIG_ENDIAN    = 5,
    REG_LINK                = 6,
    REG_MULTI_SZ            = 7,
};

enum class MaxPacketSize : uint8_t
{
    Size8  = 8,
    Size16 = 16,
    Size32 = 32,
    Size64 = 64
};

enum class Direction : uint8_t
{
    Out = 0x00,
    In  = 0x80
};

enum class TransferType : uint8_t
{
    Control     = 0x00,
    Isochronous = 0x01,
    Bulk        = 0x02,
    Interrupt   = 0x03
};

enum class SynchonisationType : uint8_t
{
    NoSynchronisation = 0x00,
    Asynchronous      = 0x04,
    Adaptive          = 0x08,
    Synchronous       = 0x0c
};

enum class UsageType : uint8_t
{
    DataEndpoint                 = 0x00,
    FeedbackEndpoint             = 0x10,
    ExplicitFeedbackDataEndpoint = 0x20,
};

enum Speed
{
    FullSpeed = 0, // 12Mbit
    LowSpeed  = 1, // 1.5Mbit
    HighSpeed = 2, // 480Mbit
    Gen1Speed = 3, // 5Gbit
};

enum SpeedBitMap : uint16_t
{
    LowSpeedBit  = 0x01,
    FullSpeedBit = 0x02,
    HighSpeedBit = 0x04,
    Gen1SpeedBit = 0x08,
};

struct UUID
{
    uint8_t uuid[16];
};

struct MsOs20DescriptorType
{
    enum : uint8_t
    {
        SetHeader                 = 0x00,
        SubsetHeaderConfiguration = 0x01,
        SubsetHeaderFunction      = 0x02,
        FeatureCompatibleId       = 0x03,
        FeatureRegProperty        = 0x04,
        FeatureMinResumeTime      = 0x05,
        FeatureModelId            = 0x06,
        FeatureCCGPDevice         = 0x07,
        FeatureVendorRevision     = 0x08,
    };
};

struct WindowsVersions
{
    enum : uint32_t
    {
        Nt6_3 = 0x06030000
    };
};

namespace descriptorSize
{
static constexpr int device                            = 18;
static constexpr int configuration                     = 9;
static constexpr int interface                         = 9;
static constexpr int interfaceAssociation              = 8;
static constexpr int endpoint                          = 7;
static constexpr int extendedCompatID                  = 16;
static constexpr int extendedCompatIDFunction          = 24;
static constexpr int extendedProperties                = 10;
static constexpr int extendedPropertiesProperty        = 12;
static constexpr int bos                               = 5;
static constexpr int usb20extension                    = 7;
static constexpr int superSpeedUsb                     = 10;
static constexpr int containerId                       = 20;
static constexpr int precisionTimeMeasurement          = 3;
static constexpr int platform                          = 20;
static constexpr int windowsPlatformSetInformation     = 8;
static constexpr int msos20SetHeader                   = 10;
static constexpr int msos20ConfigurationSubsetHeader   = 8;
static constexpr int msos20FunctionSubsetHeader        = 8;
static constexpr int msos20FeatureCompatibleId         = 20;
static constexpr int msos20FeatureRegistryProperty     = 10;
static constexpr int msos20FeatureMinimumUsbResumeTime = 6;
static constexpr int msos20FeatureModelID              = 20;
static constexpr int msos20FeatureCCGPDevice           = 4;
static constexpr int msos20FeatureVendorRevision       = 6;
} // namespace descriptorSize

} // namespace usb
