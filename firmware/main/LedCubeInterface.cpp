#include <LedCubeInterface.h>
#include <userboard.h>
#include <priorities.h>
#include <esp_log.h>
#include <esp_err.h>
#include <string.h>
#include <mutex>
#include <random>
#include <freertos/task.h>

static const char *TAG = "LedCube-IF";

using namespace drivers;

enum class CmdCode : uint8_t
{
    ReadRegister  = 0x00,
    WriteRegister = 0x40,
    ReadMemory    = 0xc0,
    WriteMemory   = 0x80,
};

enum class LedCubeInterface::RegisterAddr : uint8_t
{
    ID        = 0,
    Version   = 1,
    Control   = 2,
    Status    = 3,
    Intensity = 4,
    Position  = 5,
    Test      = 31,
};

static constexpr int CommandSize    = 8;
static constexpr int AddressSize    = 16;
static constexpr int ReadDummySize  = 8;
static constexpr int WriteDummySize = 0;

static constexpr uint16_t readRegisterCommand(LedCubeInterface::RegisterAddr reg)
{
    return static_cast<uint8_t>(CmdCode::ReadRegister) | static_cast<uint8_t>(reg);
}

static constexpr uint16_t writeRegisterCommand(LedCubeInterface::RegisterAddr reg)
{
    return static_cast<uint8_t>(CmdCode::WriteRegister) | static_cast<uint8_t>(reg);
}

static constexpr uint16_t readMemoryCommand(LedCubeInterface::address_type addr)
{
    assert(addr <= 0x1FFFF);
    return static_cast<uint8_t>(CmdCode::ReadMemory) | ((addr >> 16) & 1);
}

static constexpr uint16_t writeMemoryCommand(LedCubeInterface::address_type addr)
{
    assert(addr <= 0x1FFFF);
    return static_cast<uint8_t>(CmdCode::WriteMemory) | ((addr >> 16) & 1);
}

static constexpr uint64_t addressValue(LedCubeInterface::address_type addr)
{
    assert(addr <= 0x1FFFF);
    addr &= 0xFFFF;

    return ((addr & 0xFF) << 8) | ((addr >> 8) & 0xFF);
}

LedCubeInterface::LedCubeInterface()
{
    hostInitialize();
}

LedCubeInterface::~LedCubeInterface()
{
    if(_spiHandle)
    {
        spi_bus_remove_device(_spiHandle);
    }

    spi_bus_free(board::FPGA::spi);

    gpio_isr_handler_remove(board::FPGA::plane_blank_pin);
}

void LedCubeInterface::hostInitialize()
{
    std::scoped_lock lock(_mutex);
    ESP_ERROR_CHECK(gpio_set_direction(board::FPGA::cube_blank_pin, GPIO_MODE_INPUT));
    ESP_ERROR_CHECK(gpio_set_direction(board::FPGA::plane_blank_pin, GPIO_MODE_INPUT));

    // Configure the SPI peripheral
    spi_bus_config_t spiConfig{.mosi_io_num     = board::FPGA::mosi_pin,
                               .miso_io_num     = board::FPGA::miso_pin,
                               .sclk_io_num     = board::FPGA::sck_pin,
                               .quadwp_io_num   = -1,
                               .quadhd_io_num   = -1,
                               .data4_io_num    = -1,
                               .data5_io_num    = -1,
                               .data6_io_num    = -1,
                               .data7_io_num    = -1,
                               .max_transfer_sz = 4094,
                               .flags           = SPICOMMON_BUSFLAG_MASTER,
                               .intr_flags      = 0};
    ESP_ERROR_CHECK(spi_bus_initialize(board::FPGA::spi, &spiConfig, board::FPGA::dma));

    // Configure the SPI device
    spi_device_interface_config_t devConfig{};
    devConfig.mode           = 0;
    devConfig.clock_speed_hz = SPI_MASTER_FREQ_26M;
    devConfig.spics_io_num   = board::FPGA::cs_pin;
    devConfig.queue_size     = 20;
    devConfig.flags          = SPI_DEVICE_TXBIT_LSBFIRST | SPI_DEVICE_RXBIT_LSBFIRST;
    ESP_ERROR_CHECK(spi_bus_add_device(board::FPGA::spi, &devConfig, &_spiHandle));

    // setup blanking interrupt
    ESP_ERROR_CHECK(gpio_set_intr_type(board::FPGA::plane_blank_pin, GPIO_INTR_POSEDGE));
    ESP_ERROR_CHECK(gpio_isr_handler_add(
        board::FPGA::plane_blank_pin,
        reinterpret_cast<gpio_isr_t>(&LedCubeInterface::sIntrPlaneBlank), this));
}

void LedCubeInterface::writeMemory(address_type addr, const std::byte *data, size_t size)
{
    std::scoped_lock      lock(_mutex);
    spi_transaction_ext_t transaction{};
    transaction.base.flags     = SPI_TRANS_VARIABLE_CMD | SPI_TRANS_VARIABLE_ADDR;
    transaction.base.cmd       = writeMemoryCommand(addr);
    transaction.command_bits   = CommandSize;
    transaction.base.addr      = addressValue(addr);
    transaction.address_bits   = AddressSize;
    transaction.base.length    = size * 8;
    transaction.base.tx_buffer = data;
    transaction.base.rx_buffer = nullptr;
    ESP_ERROR_CHECK(spi_device_transmit(_spiHandle, &transaction.base));
}

void LedCubeInterface::readMemory(address_type addr, std::byte *data, size_t size)
{
    std::scoped_lock      lock(_mutex);
    spi_transaction_ext_t transaction{};
    transaction.base.flags     = SPI_TRANS_VARIABLE_CMD | SPI_TRANS_VARIABLE_ADDR;
    transaction.base.cmd       = readMemoryCommand(addr);
    transaction.command_bits   = CommandSize;
    transaction.base.addr      = addressValue(addr) | 0xaa0000;
    transaction.address_bits   = AddressSize + ReadDummySize;
    transaction.base.length    = size * 8;
    transaction.base.tx_buffer = nullptr;
    transaction.base.rx_buffer = data;
    ESP_ERROR_CHECK(spi_device_transmit(_spiHandle, &transaction.base));
}

std::byte LedCubeInterface::readRegister(RegisterAddr reg)
{
    std::scoped_lock      lock(_mutex);
    spi_transaction_ext_t transaction{};
    transaction.base.flags     = SPI_TRANS_VARIABLE_CMD | SPI_TRANS_USE_RXDATA;
    transaction.base.cmd       = readRegisterCommand(reg);
    transaction.command_bits   = CommandSize + ReadDummySize;
    transaction.base.length    = 8;
    transaction.base.tx_buffer = nullptr;
    ESP_ERROR_CHECK(spi_device_transmit(_spiHandle, &transaction.base));
    return static_cast<std::byte>(transaction.base.rx_data[0]);
}

void LedCubeInterface::writeRegister(RegisterAddr reg, std::byte val)
{
    std::scoped_lock      lock(_mutex);
    spi_transaction_ext_t transaction{};
    transaction.base.flags      = SPI_TRANS_VARIABLE_CMD | SPI_TRANS_USE_TXDATA;
    transaction.base.cmd        = writeRegisterCommand(reg);
    transaction.command_bits    = CommandSize;
    transaction.base.length     = 8;
    transaction.base.tx_data[0] = static_cast<uint8_t>(val);
    ESP_ERROR_CHECK(spi_device_transmit(_spiHandle, &transaction.base));
}

void LedCubeInterface::test()
{
    ESP_LOGI(TAG, "Testing FPGA");
    auto version = readRegister(RegisterAddr::Version);
    auto id      = readRegister(RegisterAddr::ID);
    ESP_LOGI(TAG, "FPGA ID: %02x, Version: %02x", static_cast<uint8_t>(id),
             static_cast<uint8_t>(version));
    auto testReg = readRegister(RegisterAddr::Test);
    ESP_LOGI(TAG, "Test register: %02x", static_cast<uint8_t>(testReg));
    testReg = static_cast<std::byte>(rand() & 0xFF);
    ESP_LOGI(TAG, "Writing %02x", static_cast<uint8_t>(testReg));
    writeRegister(RegisterAddr::Test, testReg);
    testReg = readRegister(RegisterAddr::Test);
    ESP_LOGI(TAG, "Test register: %02x", static_cast<uint8_t>(testReg));
    static constexpr int memSize = 640;
    std::byte           *buf     = (std::byte *)heap_caps_malloc(memSize, MALLOC_CAP_DMA);
    uint32_t             addr    = (rand() % 10) * 640;
    readMemory(addr, buf, memSize);
    ESP_LOGI(TAG, "Reading from address %04lx", addr);
    ESP_LOG_BUFFER_HEXDUMP(TAG, buf, memSize, ESP_LOG_INFO);
    for(int i = 0; i < memSize; i++)
    {
        buf[i] = static_cast<std::byte>(rand() & 0xFF);
        // buf[i] = std::byte{(uint8_t)(i+16)};
    }
    ESP_LOGI(TAG, "Writing the following data to the memory");
    ESP_LOG_BUFFER_HEXDUMP(TAG, buf, memSize, ESP_LOG_INFO);
    writeMemory(addr, buf, memSize);
    readMemory(addr, buf, memSize);
    ESP_LOGI(TAG, "Reading from address %04lx", addr);
    ESP_LOG_BUFFER_HEXDUMP(TAG, buf, memSize, ESP_LOG_INFO);

    heap_caps_free(buf);
}

void LedCubeInterface::test2()
{
    static int plane = 0;
    ESP_LOGI(TAG, "Plane %d", plane);

    static constexpr int memSize = 1000;
    std::byte           *buf     = (std::byte *)heap_caps_malloc(memSize, MALLOC_CAP_DMA);

    for(int i = 0; i < 10; i++)
    {
        int dist = abs(plane - i);
        if(dist > 3)
            dist = 3;
        uint8_t val = (3 - dist) * 10;
        // uint8_t val = i == 0 ? 0xFF : 0x00;
        memset(buf, val, memSize);
        writeMemory(1024 * i, buf, memSize);
    }
    plane = (plane + 1) % 10;
    heap_caps_free(buf);
}

void LedCubeInterface::clearMem()
{
    static constexpr int memSize = 1024;
    std::byte           *buf     = (std::byte *)heap_caps_malloc(memSize, MALLOC_CAP_DMA);

    memset(buf, 0, memSize);
    for(address_type addr = 0; addr < 0x20000; addr += memSize)
    {
        writeMemory(addr, buf, memSize);
    }

    heap_caps_free(buf);
}

void LedCubeInterface::dumpMemory()
{
    static constexpr int memSize = 4096;
    std::byte           *buf     = (std::byte *)heap_caps_malloc(memSize, MALLOC_CAP_DMA);

    for(address_type addr = 0; addr < 0x20000; addr += memSize)
    {
        readMemory(addr, buf, memSize);
        ESP_LOGI(TAG, "Dump %05lx", addr);
        ESP_LOG_BUFFER_HEXDUMP(TAG, buf, memSize, ESP_LOG_INFO);
    }

    heap_caps_free(buf);
}

void LedCubeInterface::testMem()
{
    static constexpr int memSize = 4096;
    std::byte           *buf     = (std::byte *)heap_caps_malloc(memSize, MALLOC_CAP_DMA);

    uint32_t addr = 0;

    readMemory(addr, buf, memSize);
    ESP_LOGI(TAG, "Dump %05lx", addr);
    ESP_LOG_BUFFER_HEXDUMP(TAG, buf, memSize, ESP_LOG_INFO);

    strcpy((char *)(buf), "hello world");
    writeMemory(addr + 5, buf, 20);

    readMemory(addr + 5, buf, 20);
    ESP_LOGI(TAG, "read back %s", (char *)buf);

    readMemory(addr, buf, memSize);
    ESP_LOGI(TAG, "Dump %05lx", addr);
    ESP_LOG_BUFFER_HEXDUMP(TAG, buf, memSize, ESP_LOG_INFO);

    heap_caps_free(buf);
}

void LedCubeInterface::setPlaneCallback(etl::delegate<void(int)> cb)
{
    _planeCallback = std::move(cb);
}

void LedCubeInterface::setGlobalIntensity(uint8_t intensity)
{
    assert(intensity < 8);
    writeRegister(RegisterAddr::Intensity, static_cast<std::byte>(intensity));
}

uint8_t LedCubeInterface::getGlobalIntensity()
{
    return static_cast<uint8_t>(readRegister(RegisterAddr::Intensity)) & 0x7;
}

void LedCubeInterface::enable(bool enable)
{
    _regCache.ctrl = enable ? (_regCache.ctrl | 0x1) : (_regCache.ctrl & ~0x1);
    writeRegister(RegisterAddr::Control, static_cast<std::byte>(_regCache.ctrl));
}

bool LedCubeInterface::isEnabled()
{
    auto ctrl = static_cast<uint8_t>(readRegister(RegisterAddr::Control));
    return (ctrl & 0x1) != 0;
}

bool LedCubeInterface::isRunning()
{
    return (static_cast<uint8_t>(readRegister(RegisterAddr::Status)) & 0x1) != 0;
}

void LedCubeInterface::disable()
{
    enable(false);
    waitUntilDisabled();
}

void LedCubeInterface::waitUntilDisabled()
{
    while(isRunning())
    {
        vTaskDelay(1);
    }
}

void LedCubeInterface::sIntrPlaneBlank(LedCubeInterface *self)
{
    self->intrPlaneBlank();
}

void LedCubeInterface::intrPlaneBlank()
{
    bool isFirstPlane = gpio_get_level(board::FPGA::cube_blank_pin) != 0;
    bool isSynced;

    // Try go get a sync and update the current plane counter
    if(isFirstPlane)
    {
        _currentPlane = 0;
        _hasSynced    = true;
        isSynced      = true;
    }
    else
    {
        auto currentPlane = _currentPlane.load();
        _currentPlane     = currentPlane == board::ledcube::dims[2] - 1 ? 0 : currentPlane + 1;
        isSynced          = _hasSynced.load();
    }

    // Call the callback when it is registered and we have a sync.
    if(isSynced && _planeCallback)
    {
        _planeCallback(_currentPlane.load());
    }
}
