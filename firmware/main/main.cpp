
#include <StatusLed.h>
#include <Buttons.h>
#include <FPGA.h>
#include <userboard.h>
#include <priorities.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <driver/gpio.h>
#include <esp_event.h>
#include <esp_spiffs.h>
#include <esp_log.h>
#include <RendererManager.h>
#include <Microphone.h>
#include <math.h>
#include <USB.h>
#include <nvs_flash.h>
#include <nvs.h>
#include <configuration.h>
#include <string>
#include <networkmanager.h>
#include <communication/server.h>
#include <optional>
#include <stats.h>

#include <pw_assert/assert.h>

static const char *TAG = "LED_Cube";

struct MainState
{
    // Main Blocks
    std::shared_ptr<Configuration>     config;
    std::optional<drivers::StatusLed>  statusled;
    std::optional<drivers::Buttons>    buttons;
    std::optional<drivers::Microphone> mic;
    std::optional<drivers::USB>        usb;
    std::optional<NetworkManager>      nm;
    std::optional<Server>              server;
    std::optional<RendererManager>     rm;
    std::optional<StatsService>        systemStats;

    // State
    std::atomic<std::underlying_type_t<Server::SettingsCategory>> changedSettings{0};
};

static MainState g_state;

void buttonPressHandler(void *arg, esp_event_base_t base, int32_t id, void *event_data)
{
    assert(base == drivers::DRIVER_EVENTS);
    switch(id)
    {
    case drivers::EventId::ButtonPressed:
    case drivers::EventId::ButtonReleased:
    {
        auto btnEvtData = reinterpret_cast<drivers::ButtonEventData *>(event_data);
        printf("Button %d %s.\n", btnEvtData->button, btnEvtData->state ? "pressed" : "released");
        break;
    }
    default:
        printf("Unknown event received: %s:%ld", base, id);
        break;
    }
}

static void init_fs()
{
    ESP_LOGI(TAG, "Initializing filesystem.");
    esp_vfs_spiffs_conf_t conf{
        .base_path              = "/storage",
        .partition_label        = "storage",
        .max_files              = 32,
        .format_if_mount_failed = true,
    };
    ESP_ERROR_CHECK(esp_vfs_spiffs_register(&conf));

    size_t totalBytes = 0, usedBytes = 0;
    ESP_ERROR_CHECK(esp_spiffs_info(conf.partition_label, &totalBytes, &usedBytes));
    int usage = (usedBytes * 100 + totalBytes / 2) / totalBytes;
    ESP_LOGI(TAG, "Storage partition %dkB/%dkB(%d%%)", usedBytes, totalBytes, usage);
}

static void init_nvs()
{
    ESP_LOGI(TAG, "Initializing NVS");
    esp_err_t err = nvs_flash_init();
    if(err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND)
    {
        ESP_LOGW(TAG, "NVS partition could not be read (%x). Reinitializing it.", err);
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
    }
    ESP_ERROR_CHECK(err);
}

static void dump_mem_stats()
{
    size_t dma_size    = heap_caps_get_total_size(MALLOC_CAP_DMA);
    size_t dma_free    = heap_caps_get_free_size(MALLOC_CAP_DMA);
    size_t dma_largest = heap_caps_get_largest_free_block(MALLOC_CAP_DMA);
    ESP_LOGI(TAG, "DMA Mem stats: %u/%u %u", dma_free, dma_size, dma_largest);

    size_t spiram_size    = heap_caps_get_total_size(MALLOC_CAP_SPIRAM);
    size_t spiram_free    = heap_caps_get_free_size(MALLOC_CAP_SPIRAM);
    size_t spiram_largest = heap_caps_get_largest_free_block(MALLOC_CAP_SPIRAM);
    ESP_LOGI(TAG, "SPI RAM Mem stats: %u/%u %u", spiram_free, spiram_size, spiram_largest);

    size_t ram_size    = heap_caps_get_total_size(MALLOC_CAP_DEFAULT);
    size_t ram_free    = heap_caps_get_free_size(MALLOC_CAP_DEFAULT);
    size_t ram_largest = heap_caps_get_largest_free_block(MALLOC_CAP_DEFAULT);
    ESP_LOGI(TAG, "RAM Mem stats: %u/%u %u", ram_free, ram_size, ram_largest);
}

static void processConfigurationChanges()
{
    auto categories = static_cast<Server::SettingsCategory>(
        g_state.changedSettings.exchange(0, std::memory_order::memory_order_acq_rel));
    if(categories & Server::NetworkSettings)
    {
        assert(g_state.nm);
        g_state.nm->reloadSettings();
    }
}

static void onConfigurationChanged(Server::SettingsCategory categories)
{
    g_state.changedSettings |= categories;
}

extern "C" void app_main()
{
    using namespace std::literals;
    extern int uxTopUsedPriority;

    auto &s = g_state;

    ESP_LOGI(TAG, "Started LED Cube App");
    // This is also a workaround to actually initialize this variable.
    ESP_LOGI(TAG, "FreeRTOS max priority: %d", uxTopUsedPriority);

    // Create the default event loop
    ESP_ERROR_CHECK(esp_event_loop_create_default());
    esp_log_level_set("*", ESP_LOG_INFO);
    esp_log_level_set("bus_lock", ESP_LOG_INFO);
    esp_log_level_set("TUSB:DCD", ESP_LOG_VERBOSE);

    // setup the global gpio isr service
    ESP_ERROR_CHECK(gpio_install_isr_service(priorities::isr::gpio));

    // Initialize status service
    ESP_LOGI(TAG, "Loading statistics service");
#ifdef CONFIG_CAPTURE_STATISTICS
#ifdef CONFIG_PRINT_STATISTICS
    s.systemStats.emplace(pdMS_TO_TICKS(5000), true);
#else
    s.systemStats.emplace(pdMS_TO_TICKS(5000), false);
#endif
#endif

    // Initialize the nvs
    init_nvs();
    s.config = std::make_shared<Configuration>();

    ESP_LOGI(TAG, "Loading statusled driver");
    s.statusled.emplace(board::statusled::pin);
    s.statusled->setGain(0.3f);

    // Initilize the filesystem
    init_fs();

    ESP_LOGI(TAG, "Loading button driver");
    s.buttons.emplace();

    ESP_ERROR_CHECK(esp_event_handler_register(drivers::DRIVER_EVENTS, ESP_EVENT_ANY_ID,
                                               &buttonPressHandler, nullptr));

    ESP_LOGI(TAG, "Loading USB driver");
    s.usb.emplace();

    ESP_LOGI(TAG, "Initilize networking");
    s.nm.emplace(s.config);

    ESP_LOGI(TAG, "Loading microphone driver");
    s.mic.emplace(48000, 960, 2);

    ESP_LOGI(TAG, "Loading LEDCube server");
    s.server.emplace(s.config);
    s.server->signal_settingsChanged =
        decltype(Server::signal_settingsChanged)::create<onConfigurationChanged>();

    ESP_LOGI(TAG, "Loading Renderer Manager");
    s.rm.emplace(s.config, s.server.value());

    vTaskDelay(pdMS_TO_TICKS(10000));
    dump_mem_stats();

    while(true)
    {
        vTaskDelay(pdMS_TO_TICKS(100));

        // Periodically check if settings have changed, if so update them.
        processConfigurationChanges();
    }
}
