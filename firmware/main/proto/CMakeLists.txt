include(proto)

pw_proto_library(proto_ledcube SOURCES 
    led-cube.proto 
    general.proto
    settings.proto
    INPUTS
    general.options
    settings.options)
