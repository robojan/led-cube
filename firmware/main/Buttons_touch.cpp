
#include <Buttons.h>
#include <userboard.h>
#include <priorities.h>
#include <assert.h>
#include <driver/touch_pad.h>
#include <driver/touch_sensor.h>
#include <esp_event.h>
#include <sdkconfig.h>

using namespace drivers;

static void touchsensor_filter_set(touch_filter_mode_t mode)
{
    /* Filter function */
    touch_filter_config_t filter_info{
        .mode         = mode, // Test jitter and filter 1/4.
        .debounce_cnt = 1,    // 1 time count.
        .noise_thr    = 0,    // 50%
        .jitter_step  = 4,    // use for jitter mode.
        .smh_lvl      = TOUCH_PAD_SMOOTH_IIR_2,
    };
    touch_pad_filter_set_config(&filter_info);
    touch_pad_filter_enable();
}

Buttons::Buttons()
{
    touch_pad_init();
    for(auto touchpad : board::buttons::touch)
    {
        touch_pad_config(touchpad);
        touch_pad_set_thresh(touchpad, board::buttons::touch_threshold);
    }

    // Filter settings
    touchsensor_filter_set(TOUCH_PAD_FILTER_IIR_32);

    // Idle channel settings
    touch_pad_set_idle_channel_connect(TOUCH_PAD_CONN_GND);

    // Denoise settings
    touch_pad_denoise_t denoise{
        .grade     = TOUCH_PAD_DENOISE_BIT4,
        .cap_level = TOUCH_PAD_DENOISE_CAP_L7,
    };
    touch_pad_denoise_set_config(&denoise);
    touch_pad_denoise_enable();

    // setup interrupt
    touch_pad_timeout_set(true, TOUCH_PAD_THRESHOLD_MAX);
    touch_pad_isr_register(reinterpret_cast<intr_handler_t>(touchsensor_interrupt_cb_s), this,
                           static_cast<touch_pad_intr_mask_t>(TOUCH_PAD_INTR_MASK_ALL));
    touch_pad_intr_enable(static_cast<touch_pad_intr_mask_t>(TOUCH_PAD_INTR_MASK_ACTIVE |
                                                             TOUCH_PAD_INTR_MASK_INACTIVE));

    // Enable touch sensor clock. Work mode is "timer trigger"
    touch_pad_set_fsm_mode(TOUCH_FSM_MODE_TIMER);
    touch_pad_fsm_start();

#ifdef CONFIG_BUTTONS_TOUCHPAD_DEBUG_TASK
    // Start debugging task
    auto success = xTaskCreate(reinterpret_cast<TaskFunction_t>(&Buttons::task_s), "Buttons test",
                               2048, this, priorities::task::buttons, &_task);
    assert(success == pdTRUE);
#endif
}

Buttons::~Buttons()
{
    touch_pad_deinit();
}

void Buttons::touchsensor_interrupt_cb_s(Buttons *self)
{
    self->touchsensor_interrupt_cb();
}

void Buttons::touchsensor_interrupt_cb()
{
    int task_awoken = pdFALSE;

    auto     channel = touch_pad_get_current_meas_channel();
    uint32_t status  = touch_pad_get_status();

    bool isActive = (status & (1 << channel));
    auto button   = board::buttons::touchToButton(channel);
    auto eventId  = isActive ? EventId::ButtonPressed : EventId::ButtonReleased;

    ButtonEventData data{.button = button, .state = isActive};

    esp_event_isr_post(DRIVER_EVENTS, eventId, &data, sizeof(data), &task_awoken);

    if(task_awoken == pdTRUE)
    {
        portYIELD_FROM_ISR();
    }
}

void Buttons::task_s(Buttons *self)
{
    self->task();
}

void Buttons::task()
{
    // Wait touch sensor init done
    vTaskDelay(pdMS_TO_TICKS(100));

    while(1)
    {
        uint32_t touch_value;
        for(auto touchpad : board::buttons::touch)
        {
            touch_pad_read_raw_data(touchpad, &touch_value); // read raw data.
            printf("%4ld ", touch_value);
        }
        uint32_t status = touch_pad_get_status();
        printf("%lx\nTouch: ", status);
        vTaskDelay(pdMS_TO_TICKS(200));
    }
}

bool Buttons::isButtonPressed(ButtonId id) const
{
    uint32_t status = touch_pad_get_status();
    auto     btn    = board::buttons::buttonToTouch(id);
    return (status & (1 << btn)) != 0;
}