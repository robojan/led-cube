#pragma once

#include <string_view>
#include <esp_err.h>
#include <driver/spi_master.h>

namespace drivers
{
class FPGA
{
public:
    FPGA();
    ~FPGA();

    static bool isConfigured();
    esp_err_t   configure(const char *filename);

private:
    void initialize();

    spi_device_handle_t _dev;
};

} // namespace drivers