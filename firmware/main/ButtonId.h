#pragma once

#include <cstdint>

namespace drivers
{
enum ButtonId : uint8_t
{
    Button0,
    Button1,
    Button2,
};

}
