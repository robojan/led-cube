#pragma once

#include <LedCube.h>
#include <array>
#include <userboard.h>
#include <etl/vector.h>
#include <etl/array_view.h>
#include <freertos/FreeRTOS.h>
#include <freertos/semphr.h>
#include <volatile.h>
#include <atomic>
#include <fpm/fixed.hpp>

union __attribute__((packed)) LedColor
{
    int rgb : 24;
    struct
    {
        uint8_t b;
        uint8_t g;
        uint8_t r;
    };

    constexpr LedColor(uint8_t r, uint8_t g, uint8_t b) : b(b), g(g), r(r) {}
    constexpr LedColor() : rgb(0) {}
    static LedColor fromHSV(float h, float s, float v);
    static LedColor fromHSV(fpm::fixed_16_16 h, fpm::fixed_16_16 s, fpm::fixed_16_16 v);
};
static_assert(sizeof(LedColor) == 3);

class RendererInterface
{
public:
    using renderPlaneCb_t =
        etl::delegate<void(RendererInterface *renderer, uint32_t frameNr, int planeNr)>;

    static constexpr auto strides  = board::ledcube::strides;
    static constexpr auto dims     = board::ledcube::dims;
    static constexpr auto isDouble = board::ledcube::doubleLed;

    RendererInterface();
    ~RendererInterface();

    bool init();
    void setRenderCallback(renderPlaneCb_t cb) { _renderCallback = cb; }

    etl::array_view<uint8_t> __always_inline plane(int p)
    {
        return etl::array_view<uint8_t>(&_frameBuffer[planeSize * p], planeSize);
    }
    etl::array_view<const uint8_t> __always_inline plane(int p) const
    {
        return etl::array_view<const uint8_t>(&_frameBuffer[planeSize * p], planeSize);
    }
    etl::array_view<uint8_t> __always_inline column(int c, int p)
    {
        return etl::array_view<uint8_t>(&plane(p)[strides[1] * c], strides[1]);
    }
    etl::array_view<const uint8_t> __always_inline column(int c, int p) const
    {
        return etl::array_view<const uint8_t>(&plane(p)[strides[1] * c], strides[1]);
    }
    etl::array_view<uint8_t> __always_inline rawLed(int r, int c, int p)
    {
        return etl::array_view<uint8_t>(&column(c, p)[strides[0] * r], strides[0]);
    }
    etl::array_view<const uint8_t> __always_inline rawLed(int r, int c, int p) const
    {
        return etl::array_view<const uint8_t>(&column(c, p)[strides[0] * r], strides[0]);
    }
    etl::array_view<LedColor> __always_inline doubleLed(int r, int c, int p)
    {
        int numLeds = strides[0] / sizeof(LedColor);
        return etl::array_view<LedColor>(reinterpret_cast<LedColor *>(rawLed(r, c, p).data()),
                                         numLeds);
    }
    etl::array_view<const LedColor> __always_inline doubleLed(int r, int c, int p) const
    {
        int numLeds = strides[0] / sizeof(LedColor);
        return etl::array_view<const LedColor>(
            reinterpret_cast<const LedColor *>(rawLed(r, c, p).data()), numLeds);
    }
    LedColor __always_inline led(int r, int c, int p) const { return doubleLed(r, c, p)[0]; };
    void                     setLed(bool front, int c, int r, int p, LedColor color)
    {
        doubleLed(r, c, p)[front ? 0 : 1] = color;
    }
    void setLed(int r, int c, int p, LedColor color)
    {
        setLed(true, r, c, p, color);
        setLed(false, r, c, p, color);
    };

    void clear(LedColor color)
    {
        for(size_t idx = 0; idx < _frameBuffer.size(); idx += sizeof(LedColor))
        {
            *reinterpret_cast<LedColor *>(&_frameBuffer[idx]) = color;
        }
    }


    void setGlobalIntensity(uint8_t intensity)
    {
        auto itf = interface();
        if(itf)
        {
            itf->setGlobalIntensity(intensity);
        }
    }
    uint8_t getGlobalIntensity()
    {
        auto itf = interface();
        return itf ? itf->getGlobalIntensity() : 0;
    }

    void start();
    void stop();

private:
    static constexpr int     planeSize = strides[1] * dims[1];
    static constexpr int     frameSize = dims[2] * planeSize;
    drivers::LedCube         _cube;
    etl::vector_ext<uint8_t> _frameBuffer;
    SemaphoreHandle_t        _stopSemaphore  = nullptr;
    bool                     _running        = false;
    SemaphoreHandle_t        _planeSemaphore = nullptr;
    volatile_variable<int>   _activePlane    = 0;
    renderPlaneCb_t          _renderCallback;
    std::atomic_bool         _active = false;

public:
    drivers::LedCubeInterface *interface() { return _cube.interface(); }

    static void sRenderingTask(RendererInterface *self);
    void        renderingTask();
    void        iPlaneCallback(int plane);
};
