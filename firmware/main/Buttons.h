#pragma once

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <esp_event_base.h>
#include <Drivers.h>
#include <ButtonId.h>

namespace drivers
{
struct ButtonEventData
{
    ButtonId button;
    bool     state;
};
static_assert(sizeof(ButtonEventData) < 4);

class Buttons
{
public:
    Buttons();
    ~Buttons();

    bool isButtonPressed(ButtonId id) const;

private:
    static void touchsensor_interrupt_cb_s(Buttons *self);
    void        touchsensor_interrupt_cb();

    static void task_s(Buttons *self);
    void        task();

    TaskHandle_t _task;
};

}; // namespace drivers