#include "RendererManager.h"
#include <communication/server.h>
#include <esp_log.h>

// Renderers
#include <renderers/PcRenderer.h>
#include <renderers/PlasmaRenderer.h>
#include <renderers/TestRenderer.h>
#include <renderers/FireRenderer.h>
#include <renderers/RainRenderer.h>

static const char *TAG = "Render Manager";

RendererManager::RendererManager(std::shared_ptr<Configuration> config, Server &server)
    : _config(std::move(config)), _server(server)
{
    if(!_itf.init())
    {
        ESP_LOGE(TAG, "Error loading renderer");
    }

    // Load current settings
    _currentMode     = _config->get_default<ConfigId::RendererMode>();
    _globalIntensity = _config->get_default<ConfigId::RendererGI>();

    // Set the server runtime state to the loaded config settings
    _server.runtimeConfig.globalIntensity = _globalIntensity;
    _server.runtimeConfig.mode            = _currentMode;

    // Connect the signals
    _server.signal_runtimeConfigChanged = decltype(Server::signal_runtimeConfigChanged)::create<
        RendererManager, &RendererManager::onRuntimeConfigChanged>(*this);
    _itf.setRenderCallback(
        RendererInterface::renderPlaneCb_t::create<RendererManager,
                                                   &RendererManager::onRenderPlane>(*this));

    // Start rendering
    _itf.setGlobalIntensity(_globalIntensity);
    loadRenderer();
}

RendererManager::~RendererManager()
{
    _itf.stop();
}

void RendererManager::loadRenderer()
{
    // Unload current renderer, when one is already loaded.
    if(_activeRenderer)
    {
        ESP_LOGI(TAG, "Stopping renderer %s", _activeRenderer->rendererName());
        _itf.stop();
        _activeRenderer.reset();
    }

    // Load new renderer
    switch(_currentMode)
    {
    default:
        ESP_LOGE(TAG, "Unknown renderer type specified: %d", _currentMode);
        [[fallthrough]];
    case ledcube_RuntimeConfig_Mode_Off:
        // nothing to do
        break;
    case ledcube_RuntimeConfig_Mode_Streaming:
        _activeRenderer = std::make_unique<PcRenderer>(_server);
        break;
    case ledcube_RuntimeConfig_Mode_Plasma:
        _activeRenderer = std::make_unique<PlasmaRenderer>();
        break;
    case ledcube_RuntimeConfig_Mode_Color:
        _activeRenderer = std::make_unique<TestRenderer>();
        break;
    case ledcube_RuntimeConfig_Mode_Fire:
        _activeRenderer = std::make_unique<FireRenderer>();
        break;
    case ledcube_RuntimeConfig_Mode_Rain:
        _activeRenderer = std::make_unique<RainRenderer>();
        break;
    }

    // Start rendering
    if(_activeRenderer)
    {
        ESP_LOGI(TAG, "Starting renderer %s", _activeRenderer->rendererName());
        _itf.start();
    }
}

void RendererManager::onRuntimeConfigChanged(ledcube_RuntimeConfig &config)
{
    if(_currentMode != config.mode)
    {
        _currentMode = config.mode;
        loadRenderer();
    }
    if(_globalIntensity != config.globalIntensity)
    {
        _globalIntensity = config.globalIntensity;
        _itf.setGlobalIntensity(_globalIntensity);
    }
}

void RendererManager::onRenderPlane(RendererInterface *renderer, uint32_t frameNr, int planeNr)
{
    if(_activeRenderer)
    {
        _activeRenderer->renderPlane(renderer, frameNr, planeNr);
        if(planeNr == RendererInterface::dims[2] - 1)
        {
            _activeRenderer->renderFrame(renderer, frameNr);
        }
    }
}