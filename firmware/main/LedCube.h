#pragma once

#include <string>
#include <optional>
#include <memory>
#include "FPGA.h"
#include "LedCubeInterface.h"

namespace drivers
{
class LedCube
{
    static constexpr const char *BitstreamFile = "/storage/fpga.bin";

public:
    enum class Mode
    {
        Unconfigured,
        LedCube,
    };

    LedCube();
    ~LedCube() = default;

    Mode getMode() const { return _mode; }
    bool isConfigured() const { return FPGA::isConfigured(); }

    bool reconfigure();

    LedCubeInterface *interface() { return _if.get(); }

private:
    Mode                              _mode = Mode::Unconfigured;
    std::unique_ptr<FPGA>             _fpga;
    std::unique_ptr<LedCubeInterface> _if;

    void initialize();
    void unconfigure();

    void reset(bool enable) const;
};

} // namespace drivers