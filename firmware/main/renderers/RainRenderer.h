#pragma once

#include "irenderer.h"
#include <array>
#include <RendererInterface.h>
#include <algorithm>
#include <freertos/semaphore.hpp>
#include <fpm/fixed.hpp>
#include <etl/list.h>

class RainRenderer : public IRenderer
{
    static constexpr auto kDims     = RendererInterface::dims;
    static constexpr auto kMaxDrops = 32;

public:
    inline const char *rendererName() const override { return "rain renderer"; }

    RainRenderer();
    virtual ~RainRenderer();

    // Do the rendering of one plane
    void renderPlane(RendererInterface *renderer, uint32_t frameNr, int planeNr) override;
    void renderFrame(RendererInterface *renderer, uint32_t frameNr) override;

protected:
    using Coord = fpm::fixed<int16_t, int32_t, 10>;
    static constexpr Coord   kMaxZ{kDims[2]};
    static constexpr Coord   kMaxY{1.5 * kDims[1]};
    static constexpr Coord   kMaxX{1.5 * kDims[0]};
    static constexpr Coord   kMinZ{0};
    static constexpr Coord   kMinY{-0.5 * kDims[1]};
    static constexpr Coord   kMinX{-0.5 * kDims[0]};
    static constexpr Coord   kInitialZ{0};
    static constexpr int16_t kMinIntensity{30};
    static constexpr int16_t kMaxIntensity{255};
    static constexpr Coord   kMaxHorizVel{0.1};

    struct RainDrop
    {
        Coord z, y, x;
    };

private:
    etl::list<RainDrop, kMaxDrops> _drops;
    Coord                          _velX{0}, _velY{0}, _velZ{0.35};
    int16_t                        _intensity{25}; // Global intensity, 0-255, chance of new drop
    LedColor                       _dropColor{LedColor::fromHSV(200.0f, 1.0f, 1.0f)};
};
