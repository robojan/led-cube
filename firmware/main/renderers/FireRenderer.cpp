#include "FireRenderer.h"
#include <RendererInterface.h>
#include <cmath>
#include <esp_log.h>
#include <priorities.h>

FireRenderer::FireRenderer()
{
    auto success = xTaskCreate(reinterpret_cast<TaskFunction_t>(&FireRenderer::sTask),
                               "FireRenderer", 1024, this, priorities::task::fireRenderer, &_task);
    assert(success == pdTRUE);
    _updateSemaphore.release();
}

FireRenderer::~FireRenderer()
{
    _working = false;
    for(int i = 0; i < 10; i++)
    {
        if(_exited)
            break;
        vTaskDelay(pdMS_TO_TICKS(10));
    }
}

LedColor FireRenderer::tempToColor(uint8_t temp)
{
    static constexpr std::array<LedColor, 8> colorMap{
        LedColor(0, 0, 0),    LedColor(5, 0, 0),    LedColor(77, 4, 0),   LedColor(112, 8, 0),
        LedColor(132, 12, 0), LedColor(150, 14, 2), LedColor(225, 35, 4), LedColor(255, 40, 6),
    };
    int lowIdx   = 0;
    int highIdx  = 0;
    int fraction = 0;
    if(temp < 30)
    {
        lowIdx   = 0;
        highIdx  = 0;
        fraction = 0;
    }
    else if(temp < 40)
    {
        lowIdx   = 0;
        highIdx  = 1;
        fraction = (temp - 30) * 64 / 10;
    }
    else if(temp < 60)
    {
        lowIdx   = 1;
        highIdx  = 2;
        fraction = (temp - 40) * 64 / 20;
    }
    else if(temp < 80)
    {
        lowIdx   = 2;
        highIdx  = 3;
        fraction = (temp - 60) * 64 / 20;
    }
    else if(temp < 105)
    {
        lowIdx   = 3;
        highIdx  = 4;
        fraction = (temp - 80) * 64 / 25;
    }
    else if(temp < 140)
    {
        lowIdx   = 4;
        highIdx  = 5;
        fraction = (temp - 105) * 64 / 35;
    }
    else if(temp < 180)
    {
        lowIdx   = 5;
        highIdx  = 6;
        fraction = (temp - 140) * 64 / 40;
    }
    else
    {
        lowIdx   = 6;
        highIdx  = 7;
        fraction = (temp - 180) * 64 / 20;
    }

    auto mix = [](uint8_t l, uint8_t h, uint8_t f) -> uint8_t {
        return (l * (64 - f) + h * f) / 64;
    };

    LedColor result;
    result.r = mix(colorMap[lowIdx].r, colorMap[highIdx].r, fraction);
    result.g = mix(colorMap[lowIdx].g, colorMap[highIdx].g, fraction);
    result.b = mix(colorMap[lowIdx].b, colorMap[highIdx].b, fraction);
    return result;
}

void FireRenderer::renderFrame(RendererInterface *renderer, uint32_t frameNr) {}

void FireRenderer::renderPlane(RendererInterface *renderer, uint32_t frameNr, int planeNr)
{
    if(frameNr % 2 != 0)
        return;

    if(planeNr == 0)
    {
        _renderThisFrame = _renderSemaphore.try_acquire();
        if(_renderThisFrame)
        {
            // Swap the working buffer for the render buffer and start the next render cycle. when
            // previous update was successful
            swapbuffer();
            _updateSemaphore.release();
        }
    }

    // If we missed the frame. drop it
    if(!_renderThisFrame)
        return;

    auto &buffer = this->displayBuffer();

    // Render plane
    for(int c = 0; c < dims[1]; c++)
    {
        for(int r = 0; r < dims[0]; r++)
        {
            uint8_t temp = getTemperature(r, c, planeNr, buffer);
            renderer->setLed(r, c, dims[2] - 1 - planeNr, tempToColor(temp));
        }
    }
}


void FireRenderer::sTask(FireRenderer *self)
{
    self->task();
}

void FireRenderer::task()
{
    constexpr auto dims = RendererInterface::dims;
    using namespace std::chrono_literals;
    int frameNr = 0;
    while(_working)
    {
        if(_updateSemaphore.try_acquire_for(10ms))
            continue;

        frameNr++;

        // get the working buffer
        auto &buffer    = this->workingBuffer();
        auto &oldBuffer = this->displayBuffer();

        // Assign embers
        for(int c = 0; c < dims[1]; c++)
        {
            for(int r = 0; r < dims[0]; r++)
            {
                int temp = (rand() % 10255) - 10128;
                if(temp < 0)
                {
                    temp = 0;
                }
                else
                {
                    temp *= 256;
                }
                temp += getTemperature(r, c, 0, oldBuffer) * 239;
                temp += getTemperature(r + 1, c, 0, oldBuffer) * 4;
                temp += getTemperature(r - 1, c, 0, oldBuffer) * 4;
                temp += getTemperature(r, c + 1, 0, oldBuffer) * 4;
                temp += getTemperature(r, c - 1, 0, oldBuffer) * 4;
                temp /= 256;
                if(temp > 255)
                    temp = 255;
                setTemperature(r, c, 0, temp, buffer);
            }
        }

        // Update fire
        for(int p = dims[2] - 1; p > 0; p--)
        {
            for(int r = 0; r < dims[1]; r++)
            {
                for(int c = 0; c < dims[0]; c++)
                {
                    int temp = 0;
                    temp += getTemperature(r - 1, c - 1, p, oldBuffer) * 0;
                    temp += getTemperature(r, c - 1, p, oldBuffer) * 0;
                    temp += getTemperature(r + 1, c - 1, p, oldBuffer) * 0;
                    temp += getTemperature(r - 1, c, p, oldBuffer) * 0;
                    temp += getTemperature(r, c, p, oldBuffer) * 760;
                    temp += getTemperature(r + 1, c, p, oldBuffer) * 0;
                    temp += getTemperature(r - 1, c + 1, p, oldBuffer) * 0;
                    temp += getTemperature(r, c + 1, p, oldBuffer) * 0;
                    temp += getTemperature(r + 1, c + 1, p, oldBuffer) * 0;

                    temp += getTemperature(r, c, p - 1, oldBuffer) * 200;
                    temp += getTemperature(r + 1, c, p - 1, oldBuffer) * 8;
                    temp += getTemperature(r - 1, c, p - 1, oldBuffer) * 8;
                    temp += getTemperature(r, c - 1, p - 1, oldBuffer) * 8;
                    temp += getTemperature(r, c + 1, p - 1, oldBuffer) * 8;
                    temp += getTemperature(r, c, p - 2, oldBuffer) * 0;
                    temp /= 1024;

                    setTemperature(r, c, p, temp, buffer);
                }
            }
        }

        _renderSemaphore.release();
    }
    _exited = true;
}