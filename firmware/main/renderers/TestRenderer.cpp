#include "TestRenderer.h"
#include <RendererInterface.h>
#include <cmath>

void TestRenderer::renderPlane(RendererInterface *renderer, uint32_t frameNr, int planeNr)
{
    for(int row = 0; row < renderer->dims[1]; row++)
    {
        for(int column = 0; column < renderer->dims[0]; column++)
        {
            uint32_t t       = frameNr / 90;
            uint32_t animVal = t % 4;
            LedColor rgb;
            switch(animVal)
            {
            default:
            case 0:
                rgb.r = 0xff;
                rgb.g = 0xff;
                rgb.b = 0xff;
                break;
            case 1:
                rgb.r = 0xff;
                rgb.g = 0x00;
                rgb.b = 0x00;
                break;
            case 2:
                rgb.r = 0x00;
                rgb.g = 0xff;
                rgb.b = 0x00;
                break;
            case 3:
                rgb.r = 0x00;
                rgb.g = 0x00;
                rgb.b = 0xff;
                break;
            }
            renderer->setLed(row, column, planeNr, rgb);
        }
    }
}
