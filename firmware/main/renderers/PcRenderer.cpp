#include "PcRenderer.h"
#include <communication/server.h>
#include <mutex>
#include <esp_log.h>

static const char *TAG = "PC Renderer";

PcRenderer::PcRenderer(Server &server) : _server(server)
{
    _server.signal_setFrame =
        decltype(Server::signal_setFrame)::create<PcRenderer, &PcRenderer::onFrameReceived>(*this);
}

PcRenderer::~PcRenderer()
{
    _server.signal_setFrame = {};
}

void PcRenderer::renderPlane(RendererInterface *renderer, uint32_t frameNr, int planeNr)
{
    std::lock_guard guard(_mutex);
    if(planeNr == 0)
    {
        // Check once per frame if there is new data.
        _state = _queue.empty() ? Waiting : Rendering;
    }

    // If we don't have data, do not update the frame.
    if(_state != Rendering)
        return;

    // The Queue should have at least one frame available.
    assert(!_queue.empty());

    // Copy one plane over
    auto &               frame     = _queue.front();
    static constexpr int colSize   = renderer->dims[0];
    static constexpr int planeSize = renderer->dims[1] * colSize;
    for(int col = 0; col < renderer->dims[1]; col++)
    {
        for(int row = 0; row < renderer->dims[0]; row++)
        {
            auto target = renderer->doubleLed(row, col, planeNr);
            auto source = frame[row + colSize * col + planeNr * planeSize];
            std::fill(target.begin(), target.end(), source);
        }
    }

    // If this is the last plane in the frame. pop the frame from the queue
    if(planeNr == renderer->dims[2] - 1)
    {
        _queue.pop();
        // Move state to waiting.
        _state = Waiting;
    }
}

void PcRenderer::onFrameReceived(const uint8_t *rawData, uint32_t size)
{
    std::lock_guard guard(_mutex);
    if(_queue.size() >= c_MaxQueueDepth)
    {
        ESP_LOGW(TAG, "Frame overrun");
        return;
    }
    assert(size == sizeof(decltype(_queue)::value_type));

    // Allocate the frame and copy data to it
    _queue.emplace();
    auto &frame = _queue.back();

    memcpy(frame.data(), rawData, size);
}