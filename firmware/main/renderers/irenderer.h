#pragma once

#include <cstdint>

class RendererInterface;

class IRenderer
{
public:
    virtual const char *rendererName() const = 0;

    inline virtual void renderFrame(RendererInterface *renderer, uint32_t frameNr){};
    inline virtual void renderPlane(RendererInterface *renderer, uint32_t frameNr, int planeNr){};
};
