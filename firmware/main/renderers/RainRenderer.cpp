#include "RainRenderer.h"
#include <RendererInterface.h>
#include <cmath>
#include <esp_log.h>
#include <priorities.h>
#include <fpm/math.hpp>
#include <fpm/literals.hpp>

RainRenderer::RainRenderer() {}

RainRenderer::~RainRenderer() {}

void RainRenderer::renderFrame(RendererInterface *renderer, uint32_t frameNr)
{
    // Once in a while, add a new raindrop
    if(_drops.size() < kMaxDrops && (rand() & 0xFF) < _intensity)
    {
        static constexpr auto xRange = kMaxX - kMinX;
        static constexpr auto yRange = kMaxY - kMinY;

        RainDrop drop;
        drop.z = kInitialZ;
        drop.x = Coord::from_raw_value(rand() % xRange.raw_value() + kMinX.raw_value());
        drop.y = Coord::from_raw_value(rand() % yRange.raw_value() + kMinY.raw_value());
        _drops.push_back(drop);
    }

    // Slowly modulate the direction of the rain and the intensity
    static constexpr auto kMaxHorizVelStep = Coord{0.001}.raw_value();
    _velX += Coord::from_raw_value((rand() % kMaxHorizVelStep) - kMaxHorizVelStep / 2);
    _velY += Coord::from_raw_value((rand() % kMaxHorizVelStep) - kMaxHorizVelStep / 2);
    _intensity += (rand() % 3) - 1;

    // Clamp the values
    _velX      = std::clamp(_velX, -kMaxHorizVel, kMaxHorizVel);
    _velY      = std::clamp(_velY, -kMaxHorizVel, kMaxHorizVel);
    _intensity = std::clamp(_intensity, kMinIntensity, kMaxIntensity);

    auto setLed = [&](const RainDrop &drop, const LedColor &color)
    {
        auto led_row   = static_cast<int>(drop.y);
        auto led_col   = static_cast<int>(drop.x);
        auto led_plane = static_cast<int>(drop.z);

        // Render the drop if it is in the frame
        if(led_row >= 0 && led_row < kDims[1] && led_col >= 0 && led_col < kDims[0] &&
           led_plane >= 0 && led_plane < kDims[2])
        {
            renderer->setLed(led_col, led_row, led_plane, color);
        }
    };

    // Draw and update the raindrops
    for(auto it = _drops.begin(); it != _drops.end();)
    {
        auto &drop = *it;

        // Clear the previous position
        setLed(drop, LedColor{0, 0, 0});

        drop.x += _velX;
        drop.y += _velY;
        drop.z += _velZ;

        // Draw the new position
        setLed(drop, _dropColor);

        // When the drop is out of bounds, remove it
        if(drop.x < kMinX || drop.x >= kMaxX || drop.y < kMinY || drop.y >= kMaxY ||
           drop.z < kMinZ || drop.z >= kMaxZ)
        {
            it = _drops.erase(it);
            continue;
        }

        ++it;
    }
}

void RainRenderer::renderPlane(RendererInterface *renderer, uint32_t frameNr, int planeNr) {}