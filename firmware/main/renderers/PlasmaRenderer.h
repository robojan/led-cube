#pragma once

#include "irenderer.h"

class PlasmaRenderer : public IRenderer
{
public:
    inline const char *rendererName() const override { return "plasma renderer"; }

    // Do the rendering of one plane
    void renderPlane(RendererInterface *renderer, uint32_t frameNr, int planeNr) override;
};
