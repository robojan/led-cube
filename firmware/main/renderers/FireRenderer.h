#pragma once

#include "irenderer.h"
#include <array>
#include <RendererInterface.h>
#include <algorithm>
#include <freertos/semaphore.hpp>

class FireRenderer : public IRenderer
{
    static constexpr auto dims         = RendererInterface::dims;
    static constexpr int  columnStride = dims[0];
    static constexpr int  planeStride  = dims[0] * dims[1];
    static constexpr int  numLeds      = dims[0] * dims[1] * dims[2];

public:
    inline const char *rendererName() const override { return "fire renderer"; }

    FireRenderer();
    virtual ~FireRenderer();

    // Do the rendering of one plane
    void renderPlane(RendererInterface *renderer, uint32_t frameNr, int planeNr) override;
    void renderFrame(RendererInterface *renderer, uint32_t frameNr) override;


private:
    using temparray = std::array<uint8_t, numLeds>;
    std::array<temparray, 2>  _tempBuffer{};
    uint8_t                   _activeBuffer = 0;
    freertos::BinarySemaphore _renderSemaphore{};
    freertos::BinarySemaphore _updateSemaphore;
    std::atomic_bool          _working         = true;
    std::atomic_bool          _exited          = false;
    bool                      _renderThisFrame = false;
    TaskHandle_t              _task            = nullptr;

    inline temparray &workingBuffer()
    {
        return _activeBuffer == 0 ? _tempBuffer[1] : _tempBuffer[0];
    }

    inline temparray &displayBuffer()
    {
        return _activeBuffer == 0 ? _tempBuffer[0] : _tempBuffer[1];
    }

    inline void swapbuffer() { _activeBuffer = (_activeBuffer + 1) & 1; }

    static inline uint8_t getTemperature(int r, int c, int p, temparray &tempbuffer)
    {
        r = std::clamp(r, 0, dims[0]);
        c = std::clamp(c, 0, dims[1]);
        p = std::clamp(p, 0, dims[2]);
        return tempbuffer[p * planeStride + c * columnStride + r];
    }

    static inline void setTemperature(int r, int c, int p, uint8_t temp, temparray &tempbuffer)
    {
        tempbuffer[p * planeStride + c * columnStride + r] = temp;
    }

    static LedColor tempToColor(uint8_t temp);

    static void sTask(FireRenderer *self);
    void        task();
};
