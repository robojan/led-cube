#include "PlasmaRenderer.h"
#include <RendererInterface.h>
#include <cmath>
#include <esp_log.h>
#include <esp_timer.h>
#include <fpm/fixed.hpp>
#include <fpm/math.hpp>
#include <fpm/literals.hpp>

void PlasmaRenderer::renderPlane(RendererInterface *renderer, uint32_t frameNr, int planeNr)
{
    using namespace fpm::literals;
    using fp = fpm::fixed_24_8;

    static constexpr uint32_t period = 90 * 60 * 10; // 10 minutes

    fp t{frameNr % period};
    t /= 90;
    auto circ_r = 4.5_fp8 + sin(fp::two_pi() * 0.1_fp8 * t);
    auto circ_c = 4.5_fp8 + sin(fp::two_pi() * 0.33_fp8 * t);
    auto circ_p = 4.5_fp8 + sin(fp::two_pi() * 0.025_fp8 * t);

    auto startTime = esp_timer_get_time();

    auto dp = circ_p - planeNr;
    for(int row = 0; row < renderer->dims[1]; row++)
    {
        auto v2  = 0.5_fp8 + 0.5_fp8 * sin(fp::two_pi() * 0.1_fp8 * t + row * 0.15_fp8);
        auto dr  = circ_r - row;
        auto dpr = dp * dp + dr * dr;
        auto v3  = 0.2_fp8 * t + row * 0.05_fp8 + planeNr * 0.02_fp8;
        for(int column = 0; column < renderer->dims[0]; column++)
        {
            auto dc        = circ_c - column;
            auto circ_dist = sqrt(dc * dc + dpr);

            auto v1 = 0.5_fp8 + 0.5_fp8 * sin(fp::two_pi() * 0.33_fp8 * t + circ_dist * 0.2_fp8);
            v3 += 0.025_fp8;
            auto v = fmod(v1 + v2 + v3, 1.0_fp8);

            renderer->setLed(row, column, planeNr,
                             LedColor::fromHSV(fpm::fixed_16_16{v * 360}, 1_fp16, 1_fp16));
        }
    }

    // auto endTime = esp_timer_get_time();
    // if(frameNr % 90 == 0 && planeNr == 0)
    // {
    //     ESP_LOGI("Plasma", "Frametime %d", (int)(endTime - startTime));
    // }
}
