#pragma once

#include <renderers/irenderer.h>
#include <cstdint>
#include <queue>
#include <RendererInterface.h>
#include <allocators.h>

class Server;

class PcRenderer : public IRenderer
{
    using LedFrame                       = std::array<LedColor,
                                RendererInterface::dims[0] * RendererInterface::dims[1] *
                                    RendererInterface::dims[2]>;
    static constexpr int c_MaxQueueDepth = 4;

    enum State
    {
        Waiting,
        Rendering,
    };

public:
    PcRenderer(Server &server);
    PcRenderer(const PcRenderer &) = delete;
    PcRenderer(PcRenderer &&)      = delete;
    ~PcRenderer();

    PcRenderer &operator=(const PcRenderer &) = delete;
    PcRenderer &operator=(PcRenderer &&) = delete;

    inline const char *rendererName() const override { return "streaming renderer"; }

    // Do the rendering of one frame
    void renderPlane(RendererInterface *renderer, uint32_t frameNr, int planeNr) override;

    // Receive a frame from the PC
    void onFrameReceived(const uint8_t *rawData, uint32_t size);

private:
    Server &                                                              _server;
    std::queue<LedFrame, std::deque<LedFrame, SpiRamAllocator<LedFrame>>> _queue;
    freertos::Mutex                                                       _mutex;
    State                                                                 _state = State::Waiting;
};
