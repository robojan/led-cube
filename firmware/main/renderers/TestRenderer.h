#pragma once

#include "irenderer.h"

class TestRenderer : public IRenderer
{
public:
    inline const char *rendererName() const override { return "test renderer"; }

    // Do the rendering of one plane
    void renderPlane(RendererInterface *renderer, uint32_t frameNr, int planeNr) override;
};
