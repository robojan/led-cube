#include <communication/server.h>
#include <mutex>
#include <esp_log.h>
#include <hwinfo.h>
#include <initializer_list>

static const char *TAG = "Server";

namespace pb_enum_conv
{
constexpr ledcube_WifiSettings_Mode toPB(wifi_mode_t m)
{
    return static_cast<ledcube_WifiSettings_Mode>(m);
}
constexpr wifi_mode_t fromPB(ledcube_WifiSettings_Mode m)
{
    return static_cast<wifi_mode_t>(m);
}

constexpr _ledcube_WifiSettings_Auth toPB(wifi_auth_mode_t m)
{
    return static_cast<_ledcube_WifiSettings_Auth>(m);
}
constexpr wifi_auth_mode_t fromPB(_ledcube_WifiSettings_Auth m)
{
    return static_cast<wifi_auth_mode_t>(m);
}

} // namespace pb_enum_conv

Server::Server(std::shared_ptr<Configuration> config) : _config(std::move(config))
{
    _conn.setMsgCallback(
        etl::delegate<ConnectionManager::RxMsgCallback>::create<Server, &Server::onMessage>(*this));
}

void Server::onMessage(int sender, const ledcube_Request &msg)
{
    std::lock_guard guard(_processingMutex);

    auto verifyData = [&](std::initializer_list<pb_size_t> expectedData)
    {
        bool isValid = false;
        for(auto t : expectedData)
        {
            if(msg.which_data == t)
                isValid = true;
        }

        if(!isValid)
        {
            respondError(sender, msg.id, ledcube_ErrorCode_INVALID_DATA, "Invalid data");
            return false;
        }
        return true;
    };

    switch(msg.request)
    {
    case ledcube_Request_RequestId_Echo:
        if(verifyData({ledcube_Request_echo_tag}))
        {
            handleRequestEcho(sender, msg.id, msg.data.echo);
        }
        break;
    case ledcube_Request_RequestId_NetworkSettings:
        if(verifyData({ledcube_Request_networkSettings_tag, ledcube_Request_null_tag}))
        {
            handleRequestNetworkSettings(sender, msg.id,
                                         msg.which_data == ledcube_Request_networkSettings_tag,
                                         msg.data.networkSettings);
        }
        break;
    case ledcube_Request_RequestId_SetFrame:
        if(verifyData({ledcube_Request_dataBlock_tag}))
        {
            handleRequestSetFrame(sender, msg.id, msg.data.dataBlock);
        }
        break;
    case ledcube_Request_RequestId_RuntimeConfig:
        if(verifyData({ledcube_Request_runtimeConfig_tag, ledcube_Request_null_tag}))
        {
            handleRequestRuntimeConfig(sender, msg.id,
                                       msg.which_data == ledcube_Request_runtimeConfig_tag,
                                       msg.data.runtimeConfig);
        }
        break;
    default:
        respondError(sender, msg.id, ledcube_ErrorCode_UNKNOWN_REQUEST, "Unknown message");
        break;
    }
}

void Server::handleRequestNetworkSettings(int                            sender,
                                          uint32_t                       msgId,
                                          bool                           set,
                                          const ledcube_NetworkSettings &msg)
{
    using namespace pb_enum_conv;

    ESP_LOGI(TAG, "Received network settings request request: %ld %d", msgId, set);
    if(set)
    {
        _config->set<ConfigId::Hostname>(msg.hostname);
        if(msg.has_wifi)
        {
            _config->set<ConfigId::WifiMode>(fromPB(msg.wifi.mode));
            _config->set<ConfigId::WifiSSID>(msg.wifi.ssid);
            _config->set<ConfigId::WifiPassword>(msg.wifi.password);
            _config->set<ConfigId::WifiAuthMode>(fromPB(msg.wifi.authmode));
            _config->set<ConfigId::WifiAPHidden>(msg.wifi.hiddenAP);
            _config->set<ConfigId::WifiAPChannel>(msg.wifi.channel);
        }
        signal_settingsChanged.call_if(NetworkSettings);
    }
    _response->id             = msgId;
    _response->which_response = ledcube_Response_networkSettings_tag;
    auto &networkSettings     = _response->response.networkSettings;

    // Set network settings
    strncpy(networkSettings.hostname, _config->get_default<ConfigId::Hostname>().c_str(),
            sizeof(networkSettings.hostname) - 1);
    networkSettings.hostname[sizeof(networkSettings.hostname) - 1] = '\0';
    networkSettings.has_wifi                                       = true;
    auto &wifiSettings                                             = networkSettings.wifi;

    // set Wi-Fi settings
    wifiSettings.mode = toPB(_config->get_default<ConfigId::WifiMode>());
    strncpy(wifiSettings.ssid, _config->get_default<ConfigId::WifiSSID>().c_str(),
            sizeof(wifiSettings.ssid) - 1);
    wifiSettings.ssid[sizeof(wifiSettings.ssid) - 1] = '\0';
    strncpy(wifiSettings.password, _config->get_default<ConfigId::WifiPassword>().c_str(),
            sizeof(wifiSettings.password) - 1);
    wifiSettings.password[sizeof(wifiSettings.password) - 1] = '\0';
    wifiSettings.authmode = toPB(_config->get_default<ConfigId::WifiAuthMode>());
    wifiSettings.hiddenAP = _config->get_default<ConfigId::WifiAPHidden>();
    wifiSettings.channel  = _config->get_default<ConfigId::WifiAPChannel>();

    // Send response
    _conn.sendMessage(*_response, sender);
}

void Server::handleRequestEcho(int sender, uint32_t msgId, const ledcube_Echo &msg)
{
    ESP_LOGI(TAG, "Received echo request: %ld %s", msgId, msg.serial);
    _response->id             = msgId;
    _response->which_response = ledcube_Response_echo_tag;
    strncpy(_response->response.echo.serial, hwinfo::getSerialNumber().c_str(),
            sizeof(_response->response.echo.serial) - 1);
    _response->response.echo.serial[sizeof(_response->response.echo.serial) - 1] = '\0';
    _conn.sendMessage(*_response, sender);
}

void Server::handleRequestSetFrame(int sender, uint32_t msgId, const ledcube_DataBlock &msg)
{
    static_assert(sizeof(msg.data.bytes[0]) == 1);

    if(!signal_setFrame)
    {
        respondError(sender, msgId, ledcube_ErrorCode_INVALID_REQUEST, "Pc Renderer not loaded");
    }
    else if(msg.data.size != 3000)
    {
        respondError(sender, msgId, ledcube_ErrorCode_INVALID_ARGUMENT, "Data must be 3000 bytes");
    }
    else
    {
        respondAck(sender, msgId);
        signal_setFrame(msg.data.bytes, msg.data.size);
    }
}

void Server::handleRequestRuntimeConfig(int                          sender,
                                        uint32_t                     msgId,
                                        bool                         set,
                                        const ledcube_RuntimeConfig &msg)
{
    using namespace pb_enum_conv;

    ESP_LOGI(TAG, "Received runtime config request: %ld %d", msgId, set);
    if(set)
    {
        this->runtimeConfig.globalIntensity = std::clamp(msg.globalIntensity, 0L, 4L);
        this->runtimeConfig.mode            = msg.mode;
        signal_runtimeConfigChanged.call_if(this->runtimeConfig);
    }
    _response->id             = msgId;
    _response->which_response = ledcube_Response_runtimeConfig_tag;
    auto &runtimeConfig       = _response->response.runtimeConfig;

    // Set runtime config
    runtimeConfig = this->runtimeConfig;

    // Send response
    _conn.sendMessage(*_response, sender);
}

void Server::respondError(int target, uint32_t msgId, ledcube_ErrorCode code, std::string errstr)
{
    _response->id                  = msgId;
    _response->which_response      = ledcube_Response_error_tag;
    _response->response.error.code = code;
    strncpy(_response->response.error.msg, errstr.c_str(),
            sizeof(_response->response.error.msg) - 1);
    _response->response.error.msg[sizeof(_response->response.error.msg) - 1] = '\0';
    _conn.sendMessage(*_response, target);
}

void Server::respondAck(int target, uint32_t msgId)
{
    _response->id             = msgId;
    _response->which_response = ledcube_Response_ack_tag;
    _conn.sendMessage(*_response, target);
}