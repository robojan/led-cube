
#include <lwip/sockets.h>
#include <lwip/err.h>

#include <communication/networkServer.h>

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

#include <esp_err.h>
#include <esp_log.h>
#include <cassert>

#include <priorities.h>

static const char *TAG = "NetworkServer";

static std::string sockaddrToIpString(const sockaddr_storage &addr)
{
    switch(addr.ss_family)
    {
    case AF_INET:
    {
        std::string result;
        result.resize(INET_ADDRSTRLEN + 1);
        assert(addr.s2_len == sizeof(sockaddr_in));
        auto sa = reinterpret_cast<const sockaddr_in *>(&addr);
        auto e  = inet_ntop(addr.ss_family, &sa->sin_addr, result.data(), result.size());
        assert(e != nullptr);
        result.resize(strlen(result.c_str()));
        return result;
    }
    case AF_INET6:
    {
        std::string result;
        result.resize(INET6_ADDRSTRLEN + 1);
        assert(addr.s2_len == sizeof(sockaddr_in6));
        auto sa = reinterpret_cast<const sockaddr_in6 *>(&addr);
        auto e  = inet_ntop(addr.ss_family, &sa->sin6_addr, result.data(), result.size());
        assert(e != nullptr);
        result.resize(strlen(result.c_str()));
        return result;
    }
    default:
        assert(false);
        return std::string();
    }
}
static uint16_t sockaddrToPort(const sockaddr_storage &addr)
{
    switch(addr.ss_family)
    {
    case AF_INET:
    {
        assert(addr.s2_len == sizeof(sockaddr_in));
        auto sa = reinterpret_cast<const sockaddr_in *>(&addr);
        return sa->sin_port;
    }
    case AF_INET6:
    {
        std::string result;
        result.resize(INET6_ADDRSTRLEN + 1);
        assert(addr.s2_len == sizeof(sockaddr_in6));
        auto sa = reinterpret_cast<const sockaddr_in6 *>(&addr);
        return sa->sin6_port;
    }
    default:
        assert(false);
        return 0;
    }
}

NetworkServer::NetworkServer()
{
    auto success =
        xTaskCreate(reinterpret_cast<TaskFunction_t>(&NetworkServer::sTask), "NetworkServer", 4096,
                    this, priorities::task::networkServer, nullptr);
    assert(success == pdTRUE);

    _startStopSemaphore.acquire();
}

NetworkServer::~NetworkServer()
{
    _taskRunning = false;
    _startStopSemaphore.acquire();
}

void NetworkServer::sTask(NetworkServer *self)
{
    self->task();
}

void NetworkServer::task()
{
    // Create the socket
    _socket = socket(AF_INET6, SOCK_STREAM, IPPROTO_IPV6);
    assert(_socket > 0);

    // Configure the socket
    int iOpt = 1;
    setsockopt(_socket, SOL_SOCKET, SO_REUSEADDR, &iOpt, sizeof(iOpt));
    iOpt = 0;
    setsockopt(_socket, IPPROTO_IPV6, IPV6_V6ONLY, &iOpt, sizeof(iOpt));

    ESP_LOGI(TAG, "Socket %d created", _socket);

    do
    {
        sockaddr_in6 listenAddr = {};
        listenAddr.sin6_family  = AF_INET6;
        listenAddr.sin6_port    = htons(cPort);
        int err = bind(_socket, reinterpret_cast<sockaddr *>(&listenAddr), sizeof(listenAddr));
        if(err != 0)
        {
            ESP_LOGE(TAG, "Socket unable to bind: %s", strerror(errno));
            break;
        }
        ESP_LOGI(TAG, "Socket bound to port: %d", cPort);

        err = listen(_socket, cNumMaxConnections);
        if(err != 0)
        {
            ESP_LOGE(TAG, "Could not listen on socket: %s", strerror(errno));
            break;
        }
        ESP_LOGI(TAG, "Socket listening");

        fd_set masterReadSet;
        fd_set masterWriteSet;
        FD_ZERO(&masterReadSet);
        FD_ZERO(&masterWriteSet);

        FD_SET(_socket, &masterReadSet);

        _startStopSemaphore.release();

        int maxFd = _socket;


        while(_taskRunning)
        {
            fd_set readSet  = masterReadSet;
            fd_set writeSet = masterWriteSet;

            timeval tv{0, 100000};
            err = select(maxFd + 1, &readSet, &writeSet, nullptr, &tv);
            assert(err >= 0);

            // Check for the timeout
            if(err == 0)
                continue;

            // Check if a new connection is available.
            if(FD_ISSET(_socket, &readSet))
            {
                sockaddr_storage clientAddr;
                socklen_t        clientAddrLen = sizeof(clientAddr);
                auto             clientSocket =
                    accept(_socket, reinterpret_cast<sockaddr *>(&clientAddr), &clientAddrLen);
                if(clientSocket < 0)
                {
                    ESP_LOGE(TAG, "Unable to accept connection: %s", strerror(errno));
                }
                else
                {
                    auto clientIp   = sockaddrToIpString(clientAddr);
                    auto clientPort = sockaddrToPort(clientAddr);
                    ESP_LOGI(TAG, "New client (%s:%d) connection ", clientIp.c_str(), clientPort);

                    // Create the new connection
                    NetworkConnection conn(clientSocket, std::move(clientIp), clientPort);
                    conn.registerRxCallback(_rxMsgCb);

                    // Start listening for this connection
                    FD_SET(clientSocket, &masterReadSet);
                    if(conn.writePending())
                        FD_SET(clientSocket, &masterWriteSet);
                    if(clientSocket > maxFd)
                        maxFd = clientSocket;

                    // Store the connection
                    _connections.insert(std::make_pair(clientSocket, std::move(conn)));
                }
            }

            // Check client connections
            for(auto it = _connections.begin(); it != _connections.end();)
            {
                int   fd        = it->first;
                auto &conn      = it->second;
                bool  connValid = true;
                if(connValid && FD_ISSET(fd, &readSet))
                {
                    connValid = conn.doRead();
                }
                if(connValid && FD_ISSET(fd, &writeSet))
                {
                    connValid = conn.doWrite();
                    if(conn.writePending())
                    {
                        FD_SET(fd, &masterWriteSet);
                    }
                    else
                    {
                        FD_CLR(fd, &masterWriteSet);
                    }
                }
                if(!connValid)
                {
                    ESP_LOGI(TAG, "Client connection lost");
                    // Connection lost
                    FD_CLR(fd, &masterReadSet);
                    FD_CLR(fd, &masterWriteSet);
                    _connections.erase(it++);
                }
                else
                {
                    ++it;
                }
            }
        }

    } while(false);

    ESP_LOGI(TAG, "Stopped server");

    close(_socket);

    _startStopSemaphore.release();

    vTaskDelete(nullptr);
}

void NetworkServer::sendMessage(int client, const ledcube_Response &msg)
{
    for(auto &c : _connections)
    {
        int   fd   = c.first;
        auto &conn = c.second;
        if(client < 0 || client == fd)
        {
            conn.sendMessage(msg);
        }
    }
}

void NetworkServer::setMsgCallback(etl::delegate<NetworkConnection::RxMsgCallback> cb)
{
    _rxMsgCb = cb;
    for(auto &c : _connections)
    {
        c.second.registerRxCallback(cb);
    }
}