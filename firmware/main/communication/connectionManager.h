#pragma once

#include <span>
#include <etl/delegate.h>
#include <communication/networkServer.h>
#include "led-cube.pb.h"

class ConnectionManager
{
public:
    enum SenderId
    {
        BroadCast    = -1,
        USB          = 0,
        NetworkStart = 100,
    };
    using RxMsgCallback = void(int sender, const ledcube_Request &msg);
    ConnectionManager();
    ~ConnectionManager() = default;

    void sendMessage(const ledcube_Response &msg, int target = BroadCast);
    void setMsgCallback(etl::delegate<RxMsgCallback> cb);

private:
    etl::delegate<RxMsgCallback> _msgCallback;
    NetworkServer                _network;

    void onNetworkMessage(int sender, const ledcube_Request &msg);
    void onUSBMessage(const ledcube_Request &msg);
};