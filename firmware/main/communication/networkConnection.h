#pragma once

#include <string>
#include <cstdint>
#include <etl/delegate.h>
#include <etl/state_chart.h>
#include <led-cube.pb.h>
#include <heapobject.h>
#include <allocators.h>

class NetworkConnection
{
    static constexpr uint32_t cMsgMagic          = 0x4C43f0e2;
    static constexpr uint32_t cMaxExpectedMsgLen = 9000;
    struct MsgHeader
    {
        uint32_t magic;
        uint32_t length;
    };
    static_assert(sizeof(MsgHeader) == 8);
    enum State
    {
        ReceivingHeader,
        ReceivingMsg,
    };

public:
    using RxMsgCallback = void(int clientId, const ledcube_Request &msg);
    NetworkConnection(int socket, std::string ip, uint16_t port);
    NetworkConnection(NetworkConnection &&other);
    ~NetworkConnection();

    void               operator=(const NetworkConnection &) = delete;
    NetworkConnection &operator                             =(NetworkConnection &&other);

    void sendMessage(const ledcube_Response &msg);
    void registerRxCallback(etl::delegate<RxMsgCallback> rxCallback);

    // Internal use only
    bool doRead();
    bool doWrite();
    bool writePending() const;

    int socket() const noexcept { return _socket; }

private:
    int                                                           _socket;
    std::string                                                   _ip;
    uint16_t                                                      _port;
    std::vector<std::byte, SpiRamAllocator<std::byte>>            _rxBuffer;
    HeapObject<ledcube_Request, SpiRamAllocator<ledcube_Request>> _requestMsg;
    etl::delegate<RxMsgCallback>                                  _rxMsgCb;
    State                                                         _rxState   = ReceivingHeader;
    uint32_t                                                      _rxDataLen = 0;

    bool doReadHeader();
    bool doReadMsg();

    static bool sPbWriteCallback(pb_ostream_t *stream, const uint8_t *buf, size_t count);
    bool        pbWriteCallback(pb_ostream_t *stream, const uint8_t *buf, size_t count);
};
