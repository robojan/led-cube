#pragma once

#include <freertos/mutex.hpp>
#include "connectionManager.h"
#include <configuration.h>
#include <etl/delegate.h>
#include <vector>
#include <heapobject.h>
#include <allocators.h>

class Server
{
public:
    enum SettingsCategory
    {
        NetworkSettings = 0x1,
    };

    Server(std::shared_ptr<Configuration> config);
    ~Server() = default;

    etl::delegate<void(const uint8_t *data, uint32_t size)> signal_setFrame;
    etl::delegate<void(SettingsCategory)>                   signal_settingsChanged;
    etl::delegate<void(ledcube_RuntimeConfig &)>            signal_runtimeConfigChanged;

    ledcube_RuntimeConfig runtimeConfig;

private:
    freertos::Mutex                                                 _processingMutex;
    ConnectionManager                                               _conn;
    HeapObject<ledcube_Response, SpiRamAllocator<ledcube_Response>> _response;
    std::shared_ptr<Configuration>                                  _config;

    void onMessage(int sender, const ledcube_Request &msg);

    void handleRequestNetworkSettings(int                            sender,
                                      uint32_t                       msgId,
                                      bool                           set,
                                      const ledcube_NetworkSettings &msg);
    void handleRequestEcho(int sender, uint32_t msgId, const ledcube_Echo &msg);
    void handleRequestSetFrame(int sender, uint32_t msgId, const ledcube_DataBlock &msg);
    void handleRequestRuntimeConfig(int                          sender,
                                    uint32_t                     msgId,
                                    bool                         set,
                                    const ledcube_RuntimeConfig &msg);

    void respondError(int target, uint32_t msgId, ledcube_ErrorCode code, std::string errstr);
    void respondAck(int target, uint32_t msgId);
};
