#pragma once

#include <led-cube.pb.h>
#include <etl/delegate.h>
#include <etl/map.h>
#include "networkConnection.h"
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/semaphore.hpp>

class NetworkServer
{
    static constexpr int      cNumMaxConnections = 1;
    static constexpr uint16_t cPort              = 25345;

public:
    NetworkServer();
    ~NetworkServer();

    void sendMessage(int client, const ledcube_Response &msg);
    void setMsgCallback(etl::delegate<NetworkConnection::RxMsgCallback> cb);

private:
    int                                                  _socket = -1;
    etl::map<int, NetworkConnection, cNumMaxConnections> _connections;
    bool                                                 _taskRunning = true;
    TaskHandle_t                                         _task        = nullptr;
    freertos::BinarySemaphore                            _startStopSemaphore;
    etl::delegate<NetworkConnection::RxMsgCallback>      _rxMsgCb;

    static void sTask(NetworkServer *self);
    void        task();
};