#include <communication/connectionManager.h>
#include <esp_log.h>

static const char *TAG = "ConnectionManager";

ConnectionManager::ConnectionManager()
{
    _network.setMsgCallback(etl::delegate<ConnectionManager::RxMsgCallback>::create<
                            ConnectionManager, &ConnectionManager::onNetworkMessage>(*this));
}

void ConnectionManager::sendMessage(const ledcube_Response &msg, int target)
{
    if(target == BroadCast || target == USB)
    {
        // TODO: implement USB
        ESP_LOGE(TAG, "USB not implemented");
    }
    if(target == BroadCast || target >= NetworkStart)
    {
        _network.sendMessage(target == BroadCast ? -1 : target - NetworkStart, msg);
    }
}

void ConnectionManager::setMsgCallback(etl::delegate<RxMsgCallback> cb)
{
    _msgCallback = cb;
}


void ConnectionManager::onNetworkMessage(int sender, const ledcube_Request &msg)
{
    if(_msgCallback)
    {
        _msgCallback(NetworkStart + sender, msg);
    }
}