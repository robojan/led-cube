#include <communication/networkConnection.h>
#include <pb_decode.h>
#include <pb_encode.h>
#include <lwip/sockets.h>
#include <esp_heap_caps.h>
#include <esp_log.h>

static const char *TAG = "NetworkConn";

NetworkConnection::NetworkConnection(int socket, std::string ip, uint16_t port)
    : _socket(socket), _ip(ip), _port(port)
{
}

NetworkConnection::NetworkConnection(NetworkConnection &&other)
    : _socket(other._socket),
      _ip(std::move(other._ip)),
      _port(other._port),
      _rxBuffer(std::move(other._rxBuffer)),
      _requestMsg(std::move(other._requestMsg)),
      _rxMsgCb(other._rxMsgCb),
      _rxState(other._rxState),
      _rxDataLen(other._rxDataLen)
{
    other._socket = -1;
}

NetworkConnection::~NetworkConnection()
{
    if(_socket >= 0)
    {
        close(_socket);
    }
}

NetworkConnection &NetworkConnection::operator=(NetworkConnection &&other)
{
    _socket       = other._socket;
    other._socket = -1;
    _ip           = std::move(other._ip);
    _port         = other._port;
    _rxBuffer     = std::move(other._rxBuffer);
    _requestMsg   = std::move(other._requestMsg);
    _rxMsgCb      = other._rxMsgCb;
    _rxState      = other._rxState;
    _rxDataLen    = other._rxDataLen;
    return *this;
}

void NetworkConnection::sendMessage(const ledcube_Response &msg)
{
    // Get the message size
    size_t size;
    bool   success = pb_get_encoded_size(&size, ledcube_Response_fields, &msg);
    assert(success);

    // Send the header
    MsgHeader header{.magic = cMsgMagic, .length = size};
    success = send(_socket, &header, sizeof(header), MSG_WAITALL) == sizeof(header);
    assert(success);

    // Send the data
    pb_ostream_t ostream = {&NetworkConnection::sPbWriteCallback, this, SIZE_MAX, 0, nullptr};
    success              = pb_encode(&ostream, ledcube_Response_fields, &msg);
    assert(success);
}

void NetworkConnection::registerRxCallback(etl::delegate<RxMsgCallback> rxCallback)
{
    _rxMsgCb = rxCallback;
}

bool NetworkConnection::doRead()
{
    switch(_rxState)
    {
    case ReceivingHeader:
        return doReadHeader();
    case ReceivingMsg:
        return doReadMsg();
    default:
        assert(false);
        return false;
    }
}

bool NetworkConnection::doWrite()
{
    // Not used
    return true;
}

bool NetworkConnection::writePending() const
{
    // Not used
    return false;
}

bool NetworkConnection::doReadHeader()
{
    static constexpr uint32_t headerSize = sizeof(MsgHeader);
    assert(_rxBuffer.size() < headerSize);
    uint32_t alreadyRead = _rxBuffer.size();
    uint32_t toRead      = headerSize - alreadyRead;

    // Prepare the buffer for the read operation
    _rxBuffer.resize(headerSize);

    // Read some data
    auto read = recv(_socket, _rxBuffer.data() + alreadyRead, toRead, 0);
    if(read < 0)
    {
        _rxBuffer.clear();
        ESP_LOGW(TAG, "TCP Reading error: %d", errno);
        return false;
    }
    _rxBuffer.resize(alreadyRead + read);

    // Check if the full header has been received
    if(_rxBuffer.size() == headerSize)
    {
        // Complete header has been received
        MsgHeader hdr;
        memcpy(&hdr, _rxBuffer.data(), headerSize);
        if(hdr.magic != cMsgMagic || hdr.length > cMaxExpectedMsgLen)
        {
            _rxBuffer.clear();
            ESP_LOGW(TAG, "Header corrupt: %08lx %lu", hdr.magic, hdr.length);
            return false;
        }
        // Start receiving the data
        _rxDataLen = hdr.length;
        _rxState   = ReceivingMsg;
        _rxBuffer.clear();
    }
    return true;
}

bool NetworkConnection::doReadMsg()
{
    assert(_rxBuffer.size() < _rxDataLen);
    uint32_t alreadyRead = _rxBuffer.size();
    uint32_t toRead      = _rxDataLen - alreadyRead;

    // Prepare the buffer for the read operation
    _rxBuffer.resize(_rxDataLen);

    // Read some data
    auto read = recv(_socket, _rxBuffer.data() + alreadyRead, toRead, 0);
    if(read < 0)
    {
        _rxBuffer.clear();
        _rxState = ReceivingHeader;
        ESP_LOGW(TAG, "TCP Reading error: %d", errno);
        return false;
    }
    _rxBuffer.resize(alreadyRead + read);

    if(_rxBuffer.size() == _rxDataLen)
    {
        // A complete message has been received
        auto istream = pb_istream_from_buffer(reinterpret_cast<const pb_byte_t *>(_rxBuffer.data()),
                                              _rxBuffer.size());
        if(!pb_decode(&istream, ledcube_Request_fields, _requestMsg.get()))
        {
            _rxDataLen = 0;
            _rxState   = ReceivingHeader;
            _rxBuffer.clear();
            ESP_LOGW(TAG, "Protobuf decoding error.");
            return false;
        }

        if(_rxMsgCb)
        {
            _rxMsgCb(_socket, *_requestMsg);
        }

        // Start receiving the header
        _rxDataLen = 0;
        _rxState   = ReceivingHeader;
        _rxBuffer.clear();
    }
    return true;
}

bool NetworkConnection::sPbWriteCallback(pb_ostream_t *stream, const uint8_t *buf, size_t count)
{
    auto self = reinterpret_cast<NetworkConnection *>(stream->state);
    return self->pbWriteCallback(stream, buf, count);
}

bool NetworkConnection::pbWriteCallback(pb_ostream_t *stream, const uint8_t *buf, size_t count)
{
    return send(_socket, buf, count, MSG_WAITALL) == count;
}