#include <userboard.h>
#include <FPGA.h>
#include <driver/gpio.h>
#include <driver/spi_master.h>
#include <soc/gpio_sig_map.h>
#include <esp_log.h>
#include <sdkconfig.h>
#include <vector>
#include <esp_timer.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <etl/array_view.h>

static const char *TAG = "FPGA";

using namespace drivers;

static void delayUs(int us)
{
    auto startTime = esp_timer_get_time();
    do
    {
        auto now         = esp_timer_get_time();
        auto elapsedTime = now - startTime;
        if(elapsedTime > us)
            break;
    } while(true);
}

FPGA::FPGA()
{
    initialize();
}

FPGA::~FPGA()
{
    if(_dev)
    {
        spi_bus_remove_device(_dev);
    }

    spi_bus_free(board::FPGA::spi);
}

void FPGA::initialize()
{
    // Setup general purpose IO
    ESP_ERROR_CHECK(gpio_set_direction(board::FPGA::done_pin, GPIO_MODE_INPUT));
    ESP_ERROR_CHECK(gpio_set_level(board::FPGA::config_reset_pin, 1));
    ESP_ERROR_CHECK(gpio_set_direction(board::FPGA::config_reset_pin, GPIO_MODE_INPUT_OUTPUT_OD));
    ESP_ERROR_CHECK(gpio_set_level(board::FPGA::cs_pin, 1));
    ESP_ERROR_CHECK(gpio_set_direction(board::FPGA::cs_pin, GPIO_MODE_OUTPUT));
    gpio_set_pull_mode(board::FPGA::miso_pin, GPIO_PULLDOWN_ONLY);
    gpio_set_pull_mode(board::FPGA::done_pin, GPIO_PULLUP_ONLY);
    gpio_set_pull_mode(board::FPGA::sck_pin, GPIO_PULLUP_ONLY);
    gpio_set_level(board::FPGA::sck_pin, 1);
    gpio_set_pull_mode(board::FPGA::mosi_pin, GPIO_PULLUP_ONLY);
    gpio_set_pull_mode(board::FPGA::config_reset_pin, GPIO_PULLUP_ONLY);

    // Configure the SPI peripheral
    spi_bus_config_t spiConfig{.mosi_io_num           = board::FPGA::mosi_pin,
                               .miso_io_num           = board::FPGA::miso_pin,
                               .sclk_io_num           = board::FPGA::sck_pin,
                               .quadwp_io_num         = -1,
                               .quadhd_io_num         = -1,
                               .data4_io_num          = -1,
                               .data5_io_num          = -1,
                               .data6_io_num          = -1,
                               .data7_io_num          = -1,
                               .data_io_default_level = true,
                               .max_transfer_sz       = 4094,
                               .flags = SPICOMMON_BUSFLAG_MASTER | SPICOMMON_BUSFLAG_IOMUX_PINS,
                               .isr_cpu_id = ESP_INTR_CPU_AFFINITY_AUTO,
                               .intr_flags = 0};
    ESP_ERROR_CHECK(spi_bus_initialize(board::FPGA::spi, &spiConfig, board::FPGA::dma));

    // Configure the SPI device
    spi_device_interface_config_t devConfig{};
    devConfig.mode           = 3;
    devConfig.clock_source   = SPI_CLK_SRC_APB;
    devConfig.clock_speed_hz = 20000000;
    devConfig.spics_io_num   = -1; // manually managing the chip select
    devConfig.queue_size     = 2;
    devConfig.flags          = 0;
    ESP_ERROR_CHECK(spi_bus_add_device(board::FPGA::spi, &devConfig, &_dev));
}

bool FPGA::isConfigured()
{
    return gpio_get_level(board::FPGA::done_pin) != 0;
}

esp_err_t FPGA::configure(const char *filename)
{
    ESP_LOGI(TAG, "%s the FPGA.", isConfigured() ? "Reconfiguring" : "Configuring");

    FILE *fp = fopen(filename, "rb");
    if(fp == nullptr)
    {
        ESP_LOGE(TAG, "Could not open bitstream %s.", filename);
        return ESP_ERR_NOT_FOUND;
    }

    int      bitstreamBufferSize = CONFIG_FPGA_BITSTREAM_BUFFER_SIZE / 2;
    uint8_t *rawBitstreamBuffer =
        (uint8_t *)heap_caps_malloc(CONFIG_FPGA_BITSTREAM_BUFFER_SIZE, MALLOC_CAP_DMA);
    assert(rawBitstreamBuffer != nullptr);
    std::array<etl::array_view<uint8_t>, 2> bitstreamBuffer{
        etl::array_view(rawBitstreamBuffer, bitstreamBufferSize),
        etl::array_view(rawBitstreamBuffer, bitstreamBufferSize)};
    auto sendingBuffer = bitstreamBuffer[0];
    auto loadingBuffer = bitstreamBuffer[1];

    size_t                           nextSendAmount;
    std::array<spi_transaction_t, 2> transactions{};
    spi_transaction_t               *completedTransaction;
    auto                            *newTransaction    = &transactions[0];
    auto                            *activeTransaction = &transactions[1];

    auto loadBuffer = [&]()
    {
        auto read      = fread(loadingBuffer.data(), 1, loadingBuffer.size(), fp);
        nextSendAmount = read;
        std::swap(sendingBuffer, loadingBuffer);
        return read < loadingBuffer.size();
    };

    auto sendDummyBytes = [&](int num)
    {
        spi_transaction_t trans{};
        trans.length    = num * 8;
        trans.tx_buffer = sendingBuffer.data();
        assert(num > 0 && num <= sendingBuffer.size());
        ESP_ERROR_CHECK(spi_device_polling_transmit(_dev, &trans));
    };

    auto sendChunk = [&]()
    {
        // Push the new transaction on the queue
        assert(nextSendAmount > 0);
        newTransaction->length    = nextSendAmount * 8;
        newTransaction->tx_buffer = sendingBuffer.data();
        newTransaction->rxlength  = 0;
        ESP_ERROR_CHECK(spi_device_queue_trans(_dev, newTransaction, 0));
    };

    bool isEnd = loadBuffer();

    // 1. Drive reset low
    gpio_set_level(board::FPGA::config_reset_pin, 0);
    // 2. Enable chip select
    gpio_set_level(board::FPGA::cs_pin, 0);
    // 3. Wait 200ns
    delayUs(1);
    // 4. Release reset
    gpio_set_level(board::FPGA::config_reset_pin, 1);
    // 5. Wait 1.2ms
    delayUs(1200);
    // 6. Disable chip select
    gpio_set_level(board::FPGA::cs_pin, 1);
    // 7. Send 8 clock cycles
    sendDummyBytes(1);
    // 8. Send bitstream to device, with chip select
    gpio_set_level(board::FPGA::cs_pin, 0);

    // Send the first chunk
    sendChunk();
    // Prepare the next transaction
    std::swap(newTransaction, activeTransaction);

    // Stop when this was the last transaction
    while(!isEnd)
    {
        // Wait until the active transaction is finished
        ESP_ERROR_CHECK(spi_device_get_trans_result(_dev, &completedTransaction, portMAX_DELAY));
        assert(completedTransaction == activeTransaction);
        // Load next data
        isEnd = loadBuffer();
        // Send the next chunk
        sendChunk();
        // Prepare the next transaction
        std::swap(newTransaction, activeTransaction);
    }

    // Wait until the final transaction is done
    ESP_ERROR_CHECK(spi_device_get_trans_result(_dev, &completedTransaction, portMAX_DELAY));
    assert(completedTransaction == activeTransaction);

    gpio_set_level(board::FPGA::cs_pin, 1);
    // 9. Send 104 clock cycles
    sendDummyBytes(13);
    // 10. Check CDone
    if(!isConfigured())
    {
        sendDummyBytes(7);
        ESP_LOGE(TAG, "Configuration failed");
        heap_caps_free(rawBitstreamBuffer);
        fclose(fp);
        return ESP_FAIL;
    }
    // 12. Send 56 clock cycles
    sendDummyBytes(7);
    ESP_LOGI(TAG, "Configuration succeeded");
    fclose(fp);

    heap_caps_free(rawBitstreamBuffer);

    return ESP_OK;
}