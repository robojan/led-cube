#pragma once

#include <freertos/FreeRTOS.h>
#include <task.h>
#include <volatile.h>
#include <esp_netif.h>

namespace drivers
{
class USB
{
public:
    USB();
    ~USB();

private:
    struct NetIf
    {
        esp_netif_driver_base_t base;
        USB                    *self;
    };
    static_assert(offsetof(NetIf, base) == 0, "Base must be the first member");

    void createNetIf();

    // Callbacks from USB network stack
    static esp_err_t onNetReceive(void *buffer, uint16_t len, USB *self);
    static void      onNetInit(USB *self);
    // Callbacks from ESP-NETIF
    static esp_err_t onNetTransmit(USB *self, void *buffer, size_t len);
    static void      onNetFreeRxBuffer(USB *self, void *buffer);
    static void      onNetPostAttach(esp_netif_t *netif, USB *self);

    esp_netif_t *_netif{nullptr};
    NetIf        _netif_driver{.base{}, .self = this};
};
} // namespace drivers