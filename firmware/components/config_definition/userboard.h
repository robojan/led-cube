#pragma once

#include <soc/soc.h>
#include <driver/gpio.h>
#include <driver/touch_pad.h>
#include <driver/spi_master.h>
#include <driver/i2s_std.h>
#include <array>
#include <ButtonId.h>

namespace board
{
namespace dmaAllocations
{
static constexpr int fpga = 1;
}

namespace statusled
{
static constexpr gpio_num_t pin = GPIO_NUM_18;
} // namespace statusled

namespace buttons
{
static constexpr int                                 numButtons = 3;
static constexpr std::array<gpio_num_t, numButtons>  pins{GPIO_NUM_1, GPIO_NUM_2, GPIO_NUM_3};
static constexpr std::array<touch_pad_t, numButtons> touch{TOUCH_PAD_NUM1, TOUCH_PAD_NUM2,
                                                           TOUCH_PAD_NUM3};
static constexpr int                                 touch_threshold = 500;
static constexpr drivers::ButtonId                   touchToButton(touch_pad_t channel)
{
    switch(channel)
    {
    case TOUCH_PAD_NUM1:
        return drivers::Button0;
    case TOUCH_PAD_NUM2:
        return drivers::Button1;
    case TOUCH_PAD_NUM3:
        return drivers::Button2;
    default:
        assert(false);
    }
}
static constexpr touch_pad_t buttonToTouch(drivers::ButtonId id)
{
    switch(id)
    {
    case drivers::Button0:
        return TOUCH_PAD_NUM1;
    case drivers::Button1:
        return TOUCH_PAD_NUM2;
    case drivers::Button2:
        return TOUCH_PAD_NUM3;
    default:
        assert(false);
    }
}
} // namespace buttons

namespace FPGA
{
static constexpr gpio_num_t done_pin         = GPIO_NUM_38;
static constexpr gpio_num_t config_reset_pin = GPIO_NUM_37;

static constexpr gpio_num_t sck_pin  = GPIO_NUM_12;
static constexpr gpio_num_t mosi_pin = GPIO_NUM_11;
static constexpr gpio_num_t miso_pin = GPIO_NUM_13;
static constexpr gpio_num_t cs_pin   = GPIO_NUM_10;

static constexpr gpio_num_t rst_pin         = GPIO_NUM_33;
static constexpr gpio_num_t cube_blank_pin  = GPIO_NUM_6;
static constexpr gpio_num_t plane_blank_pin = GPIO_NUM_7;

static constexpr spi_host_device_t spi = FSPI_HOST;

static constexpr gpio_num_t reserved0 = GPIO_NUM_9;
static constexpr gpio_num_t reserved1 = GPIO_NUM_4;
static constexpr gpio_num_t reserved2 = GPIO_NUM_5;
static constexpr gpio_num_t reserved3 = GPIO_NUM_8;
static constexpr gpio_num_t reserved4 = GPIO_NUM_14;

static constexpr int dma = dmaAllocations::fpga;
} // namespace FPGA

namespace microphone
{
static constexpr gpio_num_t clk_pin        = GPIO_NUM_36;
static constexpr gpio_num_t data_pin       = GPIO_NUM_35;
static constexpr gpio_num_t wordselect_pin = GPIO_NUM_34;
} // namespace microphone

namespace ledcube
{
static constexpr std::array<int, 3> dims{10, 10, 10};
static constexpr bool               doubleLed = true;

static constexpr std::array<int, 3> strides{6, 64, 1024};
} // namespace ledcube

namespace usb
{
static constexpr gpio_num_t usb_n    = GPIO_NUM_19;
static constexpr gpio_num_t usb_p    = GPIO_NUM_20;
static constexpr gpio_num_t usb_vbus = GPIO_NUM_21;
} // namespace usb

namespace unused
{
static constexpr gpio_num_t boot      = GPIO_NUM_0;
static constexpr gpio_num_t reserved0 = GPIO_NUM_15;
static constexpr gpio_num_t reserved1 = GPIO_NUM_16;
static constexpr gpio_num_t reserved2 = GPIO_NUM_17;

static constexpr gpio_num_t jtag_tck = GPIO_NUM_39;
static constexpr gpio_num_t jtag_tdo = GPIO_NUM_40;
static constexpr gpio_num_t jtag_tdi = GPIO_NUM_41;
static constexpr gpio_num_t jtag_tms = GPIO_NUM_42;
} // namespace unused

} // namespace board
