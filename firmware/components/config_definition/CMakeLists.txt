
idf_component_register(
    INCLUDE_DIRS "."
    REQUIRES 
        esp_wifi
)

target_link_libraries(${COMPONENT_LIB} INTERFACE proto_ledcube.nanopb)