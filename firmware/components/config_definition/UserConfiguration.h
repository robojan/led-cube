#pragma once

#include <typemap.h>
#include <string>
#include <esp_wifi.h>
#include <utility>
#include <staticmap.h>
#include <led-cube.pb.h>

// Don't change numeric values of this Enum to remain backwards compatible.
enum class ConfigId
{
    // Network settings
    WifiSSID      = 0,
    WifiMode      = 1,
    WifiPassword  = 2,
    WifiAuthMode  = 3,
    WifiAPHidden  = 4,
    WifiAPChannel = 5,
    Hostname      = 6,

    // Rendering settings
    RendererGI   = 32,
    RendererMode = 33,
};

using ConfigTypeMap = type_map<ConfigId,
                               ct_pair<ConfigId::WifiSSID, std::string>,
                               ct_pair<ConfigId::WifiMode, wifi_mode_t>,
                               ct_pair<ConfigId::WifiPassword, std::string>,
                               ct_pair<ConfigId::WifiAuthMode, wifi_auth_mode_t>,
                               ct_pair<ConfigId::WifiAPHidden, bool>,
                               ct_pair<ConfigId::WifiAPChannel, uint8_t>,
                               ct_pair<ConfigId::Hostname, std::string>,
                               ct_pair<ConfigId::RendererGI, uint8_t>,
                               ct_pair<ConfigId::RendererMode, ledcube_RuntimeConfig_Mode>>;

static constexpr StaticMap ConfigDefaultMap{
    makeStaticMapEntry<ConfigId::WifiSSID>("LED Cube"),
    makeStaticMapEntry<ConfigId::WifiMode>(WIFI_MODE_AP),
    makeStaticMapEntry<ConfigId::WifiPassword>("password_LED_Cube"),
    makeStaticMapEntry<ConfigId::WifiAuthMode>(WIFI_AUTH_WPA2_PSK),
    makeStaticMapEntry<ConfigId::WifiAPHidden>(false),
    makeStaticMapEntry<ConfigId::WifiAPChannel>(8),
    makeStaticMapEntry<ConfigId::Hostname>("LED_Cube"),
    makeStaticMapEntry<ConfigId::RendererGI>(1),
    makeStaticMapEntry<ConfigId::RendererMode>(ledcube_RuntimeConfig_Mode_Off),
};
