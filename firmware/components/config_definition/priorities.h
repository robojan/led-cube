#pragma once

#include <esp_intr_alloc.h>

namespace priorities
{
namespace task
{
static constexpr int buttons       = 5;
static constexpr int microphone    = 7;
static constexpr int usb           = 5;
static constexpr int renderer      = 6;
static constexpr int networkServer = 3;
static constexpr int fireRenderer  = 2;
static constexpr int stats         = 1;

} // namespace task

namespace isr
{
static constexpr int microphone = 4;
static constexpr int gpio       = ESP_INTR_FLAG_LEVEL2;
} // namespace isr

} // namespace priorities
