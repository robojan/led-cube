#!/usr/bin/python

import led_cube_pb2
import general_pb2
import socket
import struct
import random

ledcube_addr = '192.168.4.1'

class LedCubeComm:

    msg_magic = 0x4C43f0e2

    def __init__(self, addr, port = 25345):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.connect((addr, port))
        self.msgId = random.randint(0, 2000000000)
    
    @staticmethod
    def _createHeader(data):
        return struct.pack('<II', LedCubeComm.msg_magic, len(data))

    def _receiveHeader(self):
        data = self.sock.recv(8, socket.MSG_WAITALL)
        magic, size = struct.unpack('<II', data)
        if magic != LedCubeComm.msg_magic or size > 2048:
            raise ValueError("Received header is invalid")
        return size

    def getResponse(self):
        size = self._receiveHeader()
        data = self.sock.recv(size, socket.MSG_WAITALL)
        response = led_cube_pb2.Response()
        response.ParseFromString(data)
        return response

    def sendRequest(self, request):
        request.id = self.msgId
        self.msgId = self.msgId + random.randint(1,100)
        msgData = request.SerializeToString()
        data = self._createHeader(msgData) + msgData
        self.sock.sendall(data)
        return self.getResponse()

    

request = led_cube_pb2.Request()
request.request = request.Echo
request.echo.serial = 'Hello world'

lc = LedCubeComm(ledcube_addr)

response = lc.sendRequest(request)
print("Received response: " + str(response))
