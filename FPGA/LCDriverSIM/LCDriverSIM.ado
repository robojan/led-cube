setenv SIM_WORKING_FOLDER .
set newDesign 0
if {![file exists "G:/git/LED_cube/FPGA/LCDriverSIM/LCDriverSIM.adf"]} { 
	design create LCDriverSIM "G:/git/LED_cube/FPGA"
  set newDesign 1
}
design open "G:/git/LED_cube/FPGA/LCDriverSIM"
cd "G:/git/LED_cube/FPGA"
designverincludedir -clear
designverlibrarysim -PL -clear
designverlibrarysim -L -clear
designverlibrarysim -PL pmi_work
designverlibrarysim ovi_ice40up
designverdefinemacro -clear
if {$newDesign == 0} { 
  removefile -Y -D *
}
set readmempath "G:/git/LED_cube/FPGA/PlaneBufferRAM;G:/git/LED_cube/FPGA/syspll"
addfile "G:/git/LED_cube/FPGA/source/impl_1/LED_Serializer.v"
addfile "G:/git/LED_cube/FPGA/source/impl_1/FlagAligner.v"
addfile "G:/git/LED_cube/FPGA/source/impl_1/Impulsifier.v"
addfile "G:/git/LED_cube/FPGA/source/impl_1/ClockGenSerializer.v"
addfile "G:/git/LED_cube/FPGA/PlaneBufferRAM/rtl/PlaneBufferRAM.v"
addfile "G:/git/LED_cube/FPGA/source/impl_1/PlaneBufferRAMArbiter.v"
addfile "G:/git/LED_cube/FPGA/source/impl_1/IntensityGenerator.v"
addfile "G:/git/LED_cube/FPGA/source/impl_1/EdgeDetect.v"
addfile "G:/git/LED_cube/FPGA/source/impl_1/PlaneDriver.v"
addfile "G:/git/LED_cube/FPGA/source/impl_1/LCDriver.v"
addfile "G:/git/LED_cube/FPGA/source/impl_1/LCDriver_tb.sv"
vlib "G:/git/LED_cube/FPGA/LCDriverSIM/work"
set worklib work
adel -all
vlog -dbg  -work work "G:/git/LED_cube/FPGA/source/impl_1/LED_Serializer.v"
vlog -dbg  -work work "G:/git/LED_cube/FPGA/source/impl_1/FlagAligner.v"
vlog -dbg  -work work "G:/git/LED_cube/FPGA/source/impl_1/Impulsifier.v"
vlog -dbg  -work work "G:/git/LED_cube/FPGA/source/impl_1/ClockGenSerializer.v"
vlog -dbg  -work work "G:/git/LED_cube/FPGA/PlaneBufferRAM/rtl/PlaneBufferRAM.v"
vlog -dbg  -work work "G:/git/LED_cube/FPGA/source/impl_1/PlaneBufferRAMArbiter.v"
vlog -dbg  -work work "G:/git/LED_cube/FPGA/source/impl_1/IntensityGenerator.v"
vlog -dbg  -work work "G:/git/LED_cube/FPGA/source/impl_1/EdgeDetect.v"
vlog -dbg  -work work "G:/git/LED_cube/FPGA/source/impl_1/PlaneDriver.v"
vlog -dbg  -work work "G:/git/LED_cube/FPGA/source/impl_1/LCDriver.v"
vlog -sv2k12 -dbg  -work work "G:/git/LED_cube/FPGA/source/impl_1/LCDriver_tb.sv"
vsim  +access +r LCDriver_tb   -L pmi_work -L ovi_ice40up
add wave *
run 1000ns
