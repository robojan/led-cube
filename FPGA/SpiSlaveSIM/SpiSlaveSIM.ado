setenv SIM_WORKING_FOLDER .
set newDesign 0
if {![file exists "G:/git/LED_cube/FPGA/SpiSlaveSIM/SpiSlaveSIM.adf"]} { 
	design create SpiSlaveSIM "G:/git/LED_cube/FPGA"
  set newDesign 1
}
design open "G:/git/LED_cube/FPGA/SpiSlaveSIM"
cd "G:/git/LED_cube/FPGA"
designverincludedir -clear
designverlibrarysim -PL -clear
designverlibrarysim -L -clear
designverlibrarysim -PL pmi_work
designverlibrarysim ovi_ice40up
designverdefinemacro -clear
if {$newDesign == 0} { 
  removefile -Y -D *
}
set readmempath "G:/git/LED_cube/FPGA/PlaneBufferRAM;G:/git/LED_cube/FPGA/syspll;G:/git/LED_cube/FPGA/TestFrameBufferROM"
addfile "G:/git/LED_cube/FPGA/source/impl_1/Flag_CrossDomain.v"
addfile "G:/git/LED_cube/FPGA/source/impl_1/FlagAck_CrossDomain.v"
addfile "G:/git/LED_cube/FPGA/source/impl_1/Signal_CrossDomain.v"
addfile "G:/git/LED_cube/FPGA/source/impl_1/Data_CrossDomain.v"
addfile "G:/git/LED_cube/FPGA/source/impl_1/SpiSlave.v"
addfile "G:/git/LED_cube/FPGA/source/impl_1/SpiSlave_tb.sv"
vlib "G:/git/LED_cube/FPGA/SpiSlaveSIM/work"
set worklib work
adel -all
vlog -dbg  -work work "G:/git/LED_cube/FPGA/source/impl_1/Flag_CrossDomain.v"
vlog -dbg  -work work "G:/git/LED_cube/FPGA/source/impl_1/FlagAck_CrossDomain.v"
vlog -dbg  -work work "G:/git/LED_cube/FPGA/source/impl_1/Signal_CrossDomain.v"
vlog -dbg  -work work "G:/git/LED_cube/FPGA/source/impl_1/Data_CrossDomain.v"
vlog -dbg  -work work "G:/git/LED_cube/FPGA/source/impl_1/SpiSlave.v"
vlog -sv2k12 -dbg  -work work "G:/git/LED_cube/FPGA/source/impl_1/SpiSlave_tb.sv"
vsim  +access +r SpiSlave_tb   -L pmi_work -L ovi_ice40up
add wave *
run 1000ns
