module ClockGenSerializer(
	// 120MHz domain
	input clk,
	input nRst,
	
	output [5:0]a_addr,
	input [79:0]a_data,
	output a_req,
	input a_grnt,
	input a_sendSequence,
	output a_busy,
	
	// 30MHz domain
	output outClk,
	
	input [5:0]b_addr,
	output [79:0]b_data,
	input b_req,
	output b_grnt,
	output b_sendSequence,
	input b_busy	
	);
	
// Clock divider
reg [1:0] clkCntr;
assign outClk = clkCntr[1];
always @(posedge clk or negedge nRst) begin
	if(!nRst) begin
		clkCntr <= 2'b0;
	end else begin
		clkCntr <= clkCntr + 1'b1;
	end
end

// Simple connections
assign a_addr = b_addr;
assign b_data = a_data;
assign a_busy = b_busy;

// Shorten pulse
Impulsifier alignReq(.clk(clk), .nRst(nRst), .x(b_req), .y(a_req));

// Align pulse
FlagAligner alignGrnt(.clk(clk), .nRst(nRst), .x(a_grnt), .y(b_grnt));
FlagAligner alignSendSequence(.clk(clk), .nRst(nRst), .x(a_sendSequence), .y(b_sendSequence));
	
	
endmodule