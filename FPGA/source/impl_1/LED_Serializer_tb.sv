module LED_Serializer_tb();

reg clk, nRst, send_sequence;
reg [79:0]mem_di;
wire [5:0]mem_addr;
wire [9:0]lc_data;
wire lc_clk, lc_lat, busy, mem_req;
reg mem_grnt;

LED_Serializer inst(.clk(clk), .nRst(nRst), 
	.lc_clk(lc_clk), .lc_data(lc_data), .lc_lat(lc_lat), 
	.mem_addr(mem_addr), .mem_di(mem_di), .mem_req(mem_req), .mem_grnt(mem_grnt), 
	.send_sequence(send_sequence), .busy(busy));

always @(posedge clk or negedge nRst)
begin
	if(!nRst) begin
		mem_grnt <= 1'b0;
		mem_di <= 80'b0;
	end else begin
		if(mem_req) begin
			mem_grnt <= 1'b1;
			mem_di[9:0] <= {3'd0, 1'b0, mem_addr};
			mem_di[19:10] <= {3'd1, 1'b0, mem_addr};
			mem_di[29:20] <= {3'd2, 1'b0, mem_addr};
			mem_di[39:30] <= {3'd3, 1'b0, mem_addr};
			mem_di[49:40] <= {3'd4, 1'b0, mem_addr};
			mem_di[59:50] <= {3'd5, 1'b0, mem_addr};
			mem_di[69:60] <= {3'd6, 1'b0, mem_addr};
			mem_di[79:70] <= {3'd7, 1'b0, mem_addr};
		end else begin
			mem_grnt <= 1'b0;
			mem_di <= mem_di;			
		end
	end
end

initial begin
	clk = 0;
	nRst = 1;
	send_sequence = 0;
	
	#10	nRst = 0;
	#20 nRst = 1;
	$display("Reset done");
	#40;	 
	
	#100;
	send_sequence = 1;
	#20;
	send_sequence = 0;
	
	#25000;
	
	#20;
	$finish;
end

always #5 clk = !clk;

endmodule