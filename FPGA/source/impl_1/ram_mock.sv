module RamMock #( 
	parameter DATA_WIDTH = 16, 
	parameter ADDR_WIDTH = 5,
	parameter WRITE_DELAY = 0
	)
	(
		input clk,
		input nRst,
		
		input [DATA_WIDTH-1:0] data_in,
		input [ADDR_WIDTH-1:0] addr_in,
		input we,
		output reg written,
		output [DATA_WIDTH-1:0] data_out
	);
	
localparam MEM_SIZE = 2 ** ADDR_WIDTH;
localparam DELAY_CNTR_SIZE = $clog2(WRITE_DELAY);
	
reg [DATA_WIDTH-1:0] mem[0:MEM_SIZE-1];

assign data_out = mem[addr_in];

reg [DATA_WIDTH-1:0] data_in_l;
reg [ADDR_WIDTH-1:0] addr_in_l;

initial begin
	reg [ADDR_WIDTH:0] i;
	for(i = 0; i < MEM_SIZE; i = i + 1) begin
		mem[i] = {DATA_WIDTH{1'b0}};
	end
end							   

always @(posedge clk or negedge nRst)
begin
	if(!nRst) begin
		data_in_l <= {DATA_WIDTH{1'b0}};
		addr_in_l <= {ADDR_WIDTH{1'b0}};
	end else begin
		if(we) begin
			data_in_l <= data_in;
			addr_in_l <= addr_in;
		end else begin			 		
			data_in_l <= data_in_l;
			addr_in_l <= addr_in_l;
		end
	end
end				

reg [DELAY_CNTR_SIZE-1:0] delayCounter;
reg counting;
wire doWrite;
assign doWrite = delayCounter == 0 && counting;
always @(posedge clk or negedge nRst)
begin
	if(!nRst) begin
		counting <= 0;
		delayCounter <= 0;
	end else begin
		if(counting) begin
			if(delayCounter == 0) begin 
				counting <= 1'b0;
				delayCounter <= 0;
			end else begin	
				delayCounter <= delayCounter - 1'b1;
				counting <= 1'b1;
			end
		end else begin			 
			if(we) begin
				delayCounter <= WRITE_DELAY;
				counting <= 1'b1;
			end else begin		
				delayCounter <= 0;
				counting <= 1'b0;
			end			
		end
	end	
end	

always @(posedge clk or negedge nRst)
begin
	if(!nRst) begin
		written <= 1'b0;
	end else begin
		if(doWrite) begin
			written <= 1'b1;
			mem[addr_in_l] = data_in_l;
		end else begin
			written <= 1'b0;
		end
	end
end

	
endmodule