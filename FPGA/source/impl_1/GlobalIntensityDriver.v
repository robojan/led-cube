module GlobalIntensityDriver (
	input wire [2:0] intensity,
	output wire [2:0] lc_dim_p);
RGB
#(
  .CURRENT_MODE (0),
  .RGB0_CURRENT (6'b111111),
  .RGB1_CURRENT (6'b111111),
  .RGB2_CURRENT (6'b111111)
) ledDriver (
  .CURREN   (1'b1),  // I
  .RGBLEDEN (1'b1),  // I
  .RGB0PWM  (intensity[2]),  // I
  .RGB1PWM  (intensity[1]),  // I
  .RGB2PWM  (intensity[0]),  // I
  .RGB2     (lc_dim_p[0]),  // O
  .RGB1     (lc_dim_p[1]),  // O
  .RGB0     (lc_dim_p[2])   // O
);
	
endmodule