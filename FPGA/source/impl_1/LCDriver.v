module LCDriver #(
	parameter NUM_PLANES = 10,
	parameter NUM_ROWS = 10
	) (
	input clk,
	input nRst,
	
	input enable,
	output running,
	output frameSync,
	output planeSync,
	output [3:0] activePlane,
	
	// Memory interface
	input [15:0] mem_di,
	output [12:0] mem_addr,
	output mem_req,
	input mem_valid,
	
	// Ledcube interface
	output lc_clk,
	output [NUM_ROWS-1:0] lc_data,
	output lc_lat,
	output [NUM_PLANES-1:0]lc_plane,
	output reg lc_blank
	);

reg nextPlane;
reg lcEnable;
reg [4:0]compValueMsb;
reg loadPlane;
reg activePlaneBuffer;
reg sendSerializer_delayed;
reg sendSerializer;

wire genBusy;
wire serializerBusy;
wire serializerStepDone;
wire planeDriverBusy;
wire planeLcBlank;
wire [8:0]genInAddr;

// Interconnect
// gen <-> planeBuffer
wire [79:0] genDo;
wire [5:0] genAddr;
wire genReq;
wire genGrnt;

// planeBuffer <-> serializer
wire [5:0] bufAddr;
wire [79:0] bufData;
wire bufReq;
wire bufGrnt;

PlaneDriver planeDriver (.clk(clk), .nRst(nRst), 
	.plane(lc_plane), .blank(planeLcBlank), 
	.next(nextPlane), .enable(lcEnable), .busy(planeDriverBusy));

IntensityGenerator gen(.clk(clk), .nRst(nRst), .comp_msb(compValueMsb), .load_plane(loadPlane), .busy(genBusy), 
	.in_di(mem_di), .in_addr(genInAddr), .in_req(mem_req), .in_valid(mem_valid),
	.out_do(genDo), .out_addr(genAddr), .out_req(genReq), .out_grnt(genGrnt));
	
PlaneBufferRAMArbiter planeBuffer(.clk(clk), .nRst(nRst),
	.a_data(genDo), .a_addr({activePlaneBuffer, genAddr}), .a_req(genReq), .a_grnt(genGrnt),
	.b_addr({!activePlaneBuffer, bufAddr}), .b_do(bufData), .b_req(bufReq), .b_grnt(bufGrnt));
	
LED_Serializer serializer(.clk(clk), .nRst(nRst), 
	.lc_clk(lc_clk), .lc_data(lc_data), .lc_lat(lc_lat),
	.mem_addr(bufAddr), .mem_di(bufData), .mem_req(bufReq), .mem_grnt(bufGrnt),
	.busy(serializerBusy), .send_step(sendSerializer || sendSerializer_delayed), .step_done(serializerStepDone));
	
// Control logic

// Comp value counter
reg compValueCounterEn;
reg compValueCounterRst;
wire lastLoadCompValue;
assign lastLoadCompValue = compValueMsb == 5'd31;
always @(posedge clk or negedge nRst) begin
	if(!nRst) begin
		compValueMsb <= 5'b0;
	end else begin
		if(compValueCounterRst) begin
			compValueMsb <= 5'b0;
		end else if(compValueCounterEn) begin
			compValueMsb <= compValueMsb + 1'b1;
		end else begin
			compValueMsb <= compValueMsb;
		end
	end
end

// Sending Comp value
reg [4:0] sendingCompValue;
reg sendCompValueLatch;
wire lastSendCompValue;
assign lastSendCompValue = sendingCompValue == 5'd31;
always @(posedge clk or negedge nRst) begin
	if(!nRst) begin
		sendingCompValue <= 5'b0;
	end else begin
		if(sendCompValueLatch) begin
			sendingCompValue <= compValueMsb;
		end else begin
			sendingCompValue <= sendingCompValue;
		end
	end
end	

// Active serial buffer logic
reg swapPlaneBuffer;
always @(posedge clk or negedge nRst) begin
	if(!nRst) begin
		activePlaneBuffer <= 1'b0;
	end else begin
		if(swapPlaneBuffer) begin
			activePlaneBuffer <= !activePlaneBuffer;
		end else begin
			activePlaneBuffer <= activePlaneBuffer;
		end		
	end
end 

// Plane address
reg [3:0] planeAddr;
reg [3:0] nextPlaneAddr;
wire planeAddrRst;
wire planeAddrCount;

always @(posedge clk or negedge nRst)
begin
	if(!nRst) begin
		planeAddr <= 4'd0;
		nextPlaneAddr <= 4'd0;
	end else begin
		if(planeAddrRst) begin
			planeAddr <= 4'd0;	
		end else if (planeAddrCount) begin
			planeAddr <= nextPlaneAddr;
		end else begin
			planeAddr <= planeAddr;
		end
		if (lastLoadCompValue && lc_plane == 10'h200 && !genBusy) begin
			nextPlaneAddr <= 4'd0;
		end else if(lastLoadCompValue && !genBusy) begin
			nextPlaneAddr <= planeAddr + 4'd1;
		end else begin
			nextPlaneAddr <= planeAddr;
		end
	end
end	

// Blank logic
reg delayedLcLat;
always @(posedge clk) delayedLcLat <= lc_lat;
wire lcLatFalling;
assign lcLatFalling = !lc_lat && delayedLcLat;
	
always @(posedge clk or negedge nRst) begin
	if(!nRst) begin
		lc_blank <= 1'b1;
	end else begin
		if(planeLcBlank) begin
			lc_blank <= 1'b1;
		end else if(lcLatFalling) begin
			lc_blank  <= 1'b0;
		end	else begin
			lc_blank <= lc_blank;
		end
	end	
end

// Delay send serializer
reg doSendSerializer;
always @(posedge clk or negedge nRst) sendSerializer_delayed <= !nRst ? 1'b0 : doSendSerializer;

// Misc logic

wire planeDone;
assign planeDone = !genBusy && !serializerBusy;
assign planeSync = nextPlane;
assign frameSync = lc_plane == 10'b1;
assign planeAddrCount = lastLoadCompValue && loadPlane;
assign planeAddrRst = (planeAddrCount && (lc_plane == 10'h200)) || !enable;

assign mem_addr = {nextPlaneAddr, genInAddr};

assign activePlane = nextPlaneAddr;

// FSM
localparam IDLE = 3'd0, LOADING_PLANE = 3'd1, SENDING_FIRST_PLANE = 3'd2, SENDING_FIRST_STEP = 3'd3, SENDING_PLANE = 3'd4, SWAP_PLANE = 3'd5;
reg [2:0]state;
reg [2:0]nextState;

assign running = state != IDLE;

always @(posedge clk or negedge nRst) begin
	if(!nRst) begin
		state <= IDLE;
	end else begin
		state <= nextState;
	end
end

always @(*) begin
	case(state) 
	default: begin
		compValueCounterRst <= 1'b1;
		compValueCounterEn <= 1'b0;
		lcEnable <= 1'b0;
		swapPlaneBuffer <= 1'b0;
		sendSerializer <= 1'b0;
		doSendSerializer <= 1'b0;
		nextPlane <= 1'b0;
		sendCompValueLatch <= 1'b0;
		nextState <= IDLE;
		loadPlane <= 1'b0;
	end
	IDLE: begin
		compValueCounterRst <= 1'b1;
		compValueCounterEn <= 1'b0;
		lcEnable <= 1'b0;
		swapPlaneBuffer <= 1'b0;
		sendSerializer <= 1'b0;
		doSendSerializer <= 1'b0;
		nextPlane <= 1'b0;
		sendCompValueLatch <= 1'b0;
		if(enable) begin
			nextState <= LOADING_PLANE;
			loadPlane <= 1'b1;
		end else begin
			nextState <= IDLE;
			loadPlane <= 1'b0;
		end			
	end
	LOADING_PLANE: begin
		compValueCounterRst <= 1'b0;
		lcEnable <= 1'b0;
		nextPlane <= 1'b0;
		if(!genBusy && !enable) begin
			nextState <= IDLE;		
			swapPlaneBuffer <= 1'b0;
			sendSerializer <= 1'b0;	
			doSendSerializer <= 1'b0;
			sendCompValueLatch <= 1'b0;
			compValueCounterEn <= 1'b0;
			loadPlane <= 1'b0;
		end else if(!genBusy) begin
			nextState <= SENDING_FIRST_PLANE;	
			swapPlaneBuffer <= 1'b1;
			sendSerializer <= 1'b0;	
			doSendSerializer <= 1'b1;
			sendCompValueLatch <= 1'b1;
			compValueCounterEn <= 1'b1;	
			loadPlane <= 1'b1;
		end else begin
			nextState <= LOADING_PLANE;		
			swapPlaneBuffer <= 1'b0;
			sendSerializer <= 1'b0;	
			doSendSerializer <= 1'b0;
			sendCompValueLatch <= 1'b0;
			compValueCounterEn <= 1'b0;
			loadPlane <= 1'b0;
		end
	end
	SENDING_FIRST_PLANE: begin
		compValueCounterRst <= 1'b0;
		lcEnable <= 1'b0;
		sendCompValueLatch <= 1'b0;
		sendSerializer <= 1'b0;
		doSendSerializer <= 1'b0;
		if(serializerStepDone) begin
			nextState <= SENDING_PLANE;	
			swapPlaneBuffer <= 1'b0;
			compValueCounterEn <= 1'b0;
			loadPlane <= 1'b0;
			nextPlane <= 1'b0;
		end else begin
			nextState <= SENDING_FIRST_PLANE;		
			swapPlaneBuffer <= 1'b0;
			compValueCounterEn <= 1'b0;
			loadPlane <= 1'b0;
			nextPlane <= 1'b0;
		end
	end
	SENDING_FIRST_STEP: begin
		compValueCounterRst <= 1'b0;
		lcEnable <= 1'b1;
		sendCompValueLatch <= 1'b0;
		sendSerializer <= 1'b0;
		doSendSerializer <= 1'b0;
		if(serializerStepDone) begin
			nextState <= SWAP_PLANE;	
			swapPlaneBuffer <= 1'b0;
			compValueCounterEn <= 1'b0;
			loadPlane <= 1'b0;
			nextPlane <= 1'b1;
		end else begin
			nextState <= SENDING_FIRST_STEP;		
			swapPlaneBuffer <= 1'b0;
			compValueCounterEn <= 1'b0;
			loadPlane <= 1'b0;
			nextPlane <= 1'b0;
		end
	end
	SENDING_PLANE: begin
		compValueCounterRst <= 1'b0;
		lcEnable <= 1'b1;
		if(planeDone && !enable) begin
			nextState <= IDLE;		
			swapPlaneBuffer <= 1'b0;
			sendSerializer <= 1'b0;	
			doSendSerializer <= 1'b0;
			compValueCounterEn <= 1'b0;
			loadPlane <= 1'b0;
			nextPlane <= 1'b0;
			sendCompValueLatch <= 1'b0;
		end else if(planeDone && lastSendCompValue) begin
			nextState <= SENDING_FIRST_STEP;	
			swapPlaneBuffer <= 1'b1;
			sendSerializer <= 1'b0;	
			doSendSerializer <= 1'b1;
			compValueCounterEn <= 1'b1;
			loadPlane <= 1'b1;
			nextPlane <= 1'b0;
			sendCompValueLatch <= 1'b1;
		end else if(planeDone) begin
			nextState <= SENDING_PLANE;		
			swapPlaneBuffer <= 1'b1;
			sendSerializer <= 1'b0;	
			doSendSerializer <= 1'b1;
			compValueCounterEn <= 1'b1;
			loadPlane <= 1'b1;
			nextPlane <= 1'b0;
			sendCompValueLatch <= 1'b1;
		end else if(serializerStepDone) begin
			nextState <= SENDING_PLANE;		
			swapPlaneBuffer <= 1'b0;
			sendSerializer <= 1'b1;	
			doSendSerializer <= 1'b0;
			compValueCounterEn <= 1'b0;
			loadPlane <= 1'b0;
			nextPlane <= 1'b0;
			sendCompValueLatch <= 1'b0;
		end else begin
			nextState <= SENDING_PLANE;		
			swapPlaneBuffer <= 1'b0;
			sendSerializer <= 1'b0;	
			doSendSerializer <= 1'b0;
			compValueCounterEn <= 1'b0;
			loadPlane <= 1'b0;
			nextPlane <= 1'b0;
			sendCompValueLatch <= 1'b0;
		end
	end
	SWAP_PLANE: begin
		lcEnable <= 1'b1;
		nextPlane <= 1'b0;
		compValueCounterRst <= 1'b0;
		sendCompValueLatch <= 1'b0;
		
		if(!planeDriverBusy && !enable) begin
			nextState <= IDLE;		
			swapPlaneBuffer <= 1'b0;
			sendSerializer <= 1'b0;	
			doSendSerializer <= 1'b0;
			compValueCounterEn <= 1'b0;
			loadPlane <= 1'b0;
		end else if(!planeDriverBusy) begin
			nextState <= SENDING_PLANE;		
			swapPlaneBuffer <= 1'b0;
			sendSerializer <= 1'b1;	
			doSendSerializer <= 1'b0;
			compValueCounterEn <= 1'b0;
			loadPlane <= 1'b0;
		end else begin
			nextState <= SWAP_PLANE;		
			swapPlaneBuffer <= 1'b0;
			sendSerializer <= 1'b0;	
			doSendSerializer <= 1'b0;
			compValueCounterEn <= 1'b0;
			loadPlane <= 1'b0;
		end
	end
	endcase
end
	
endmodule