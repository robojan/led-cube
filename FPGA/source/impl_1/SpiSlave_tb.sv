module SpiSlave_tb();

	parameter WIDTH = 8;
	parameter CLK_PERIOD = 10;
	parameter SPI_CLK_PERIOD = 18;
	
	reg sclk = 0;
	reg sdi = 0;
	wire sdo;
	reg nCs = 1;
	
	reg clk = 0;
	reg nRst = 1;
	wire [WIDTH-1:0] rxData;
	wire rxValid;
	reg [WIDTH-1:0] txData = 0;
	reg txValid = 0;
	wire txBusy;
	
	SpiSlave #(.WIDTH(WIDTH)) uut (.sclk(sclk), .sdi(sdi), .sdo(sdo), .nCs(nCs), .clk(clk), .nRst(nRst), .rxData(rxData), .rxValid(rxValid), .txData(txData), .txValid(txValid), .txBusy(txBusy));
	
	always #(CLK_PERIOD/2) clk = !clk;
	initial begin
		#(CLK_PERIOD*2) nRst = 0;
		#(CLK_PERIOD*2) nRst = 1;
		#(CLK_PERIOD*2);
		transaction({8'hab, 8'h66, 8'h20, 8'h05, 8'hef}, {8'h55, 8'h01, 8'h80, 8'h08, 8'h10});
		#(CLK_PERIOD*5);
		transaction({8'h00, 8'h01, 8'h02, 8'h03, 8'h04}, {8'hff, 8'hfe, 8'hfd, 8'hfc, 8'hfb});
		#(CLK_PERIOD*5);
		$finish;
	end
	
	function string array2string(input bit [WIDTH-1:0] a[]);
		automatic string result = "";
		foreach(a[i]) begin
			result = {result, $sformatf("%02X ", a[i])};
		end
		return result;
	endfunction	

	task transaction(input bit [WIDTH-1:0] t_tx[], input bit [WIDTH-1:0] r_tx[]);
		automatic bit transaction_busy = 1;
		automatic bit [WIDTH-1:0] t_rx[];
		automatic bit [WIDTH-1:0] r_rx[];
		
		$display("----------------------------------------------------------------------");	
		fork
			// SPI master
			begin
				
				t_rx = new[t_tx.size()];
				
				// Start the transaction
				nCs = 0;
				
				foreach(t_tx[i]) begin
					for(int b = 0; b < WIDTH; b++) begin
						sclk = 0;
						sdi = t_tx[i][b];
						#(SPI_CLK_PERIOD / 2);
						sclk = 1;
						t_rx[i][b] = sdo;
						#(SPI_CLK_PERIOD / 2);
					end
				end
				nCs = 1;
				sclk = 0;
				$display("SPI Master done: %2d bytes TX: %s RX: %s", t_tx.size(), array2string(t_tx), array2string(t_rx));
				#(SPI_CLK_PERIOD*2);
				transaction_busy = 0;
			end
						
			// SPI slave
			begin
				automatic int sendPtr = 0;
				while(transaction_busy) begin
					@(negedge transaction_busy or posedge rxValid);
					if(!transaction_busy) break;
					if(rxValid)	begin
						r_rx = new[r_rx.size()+1](r_rx);
						r_rx[r_rx.size()-1] = rxData;
						txData = r_tx[sendPtr];
						sendPtr++;
						txValid = 1;
						@(posedge clk)
						txValid = 0;
						txData = 0;
					end	
				end				
				$display("SPI Slave done:  %2d bytes RX: %s TX: %s", r_rx.size(), array2string(r_rx), array2string(r_tx));
			end	
		join
		t_rx.delete;
		r_rx.delete;
		$display("----------------------------------------------------------------------");
	endtask

endmodule