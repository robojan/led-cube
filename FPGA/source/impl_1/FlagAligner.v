module FlagAligner(
	input clk,
	input nRst,
	input x,
	
	output y);
	
	
reg [1:0]counter;
reg counting;

assign y = counting || x;

always @(posedge clk or negedge nRst) begin
	if(!nRst) begin
		counting <= 1'b0;
		counter <= 2'b0;
	end else begin
		if(counting) begin
			if(counter == 2'd3) begin
				counting <= 1'b0;
			end else begin
				counting <= 1'b1;
			end
			counter <= counter + 1'b1;
		end else if(x) begin
			counting <= 1'b1;
			counter <= counter + 1'b1;
		end else begin
			counting <= 1'b0;
			counter <= 2'b0;
		end
	end
end
	
endmodule
	