module Data_CrossDomain #(
	parameter WIDTH = 8,
	parameter DATA_RST_VAL = {WIDTH{1'b0}}
	) (
	input rst,
	
	input clkA,
	input validA,
	input [WIDTH-1:0] dataA,
	
	output clkB,
	output validB,
	output [WIDTH-1:0] dataB
	);
	
	// Src data
	reg [WIDTH-1:0] dataClkA;
	always @(posedge clkA or posedge rst) begin
		if(rst) begin
			dataClkA <= DATA_RST_VAL;
		end else begin
			dataClkA <= validA ? dataA : dataClkA;
		end
	end
	
	// Src toggle to pulse converter
	reg toggleReg;
	always @(posedge clkA or posedge rst) begin
		if(rst) begin
			toggleReg <= 1'b0;
		end else begin
			toggleReg <= toggleReg ^ validA;
		end
	end
	
	// Synchronizer
	wire toggleClkB;
	Signal_CrossDomain destSynchronizer (rst, toggleReg, clkB, toggleClkB);
	
	// Toggle to pulse converter
	reg toggleState;
	assign validB = toggleState ^ toggleClkB;
	always @(posedge clkB or posedge rst) begin
		if(rst) begin
			toggleState <= 1'b0;
		end else begin
			toggleState <= toggleClkB;
		end
	end
	
	assign dataB = dataClkA;
	
endmodule