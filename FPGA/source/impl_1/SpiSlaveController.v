module SpiSlaveController(
	input clk,
	input nRst,
	
	// Spi slave interface
	input [7:0] spi_rxData,
	input spi_rxValid,
	output [7:0] spi_txData,
	output reg spi_txValid,
	input spi_busy,
	
	// Register interface
	output reg [4:0] reg_addr,
	output reg reg_we,
	output [7:0] reg_do,
	input [7:0] reg_di,
	
	// Memory interface
	output reg [16:0] mem_addr,
	output reg mem_we,
	output reg mem_req,
	output [7:0]mem_do,
	input [7:0]mem_di,
	input mem_grnt	
	);
	
	
localparam CMD_READ_REG = 2'h0, CMD_WRITE_REG = 2'h1, CMD_READ_MEM = 2'h3, CMD_WRITE_MEM = 2'h2;

// Command decoding
wire [1:0]cmd_cmd;
wire [4:0]cmd_reg;
wire cmd_memAddrMsb;
assign cmd_cmd = spi_rxData[7:6];
assign cmd_reg = spi_rxData[4:0];
assign cmd_memAddrMsb = spi_rxData[0];

// Register Address 
reg storeRegAddr;
reg incRegAddr;
always @(posedge clk or negedge nRst) begin
	if(!nRst) begin
		reg_addr <= 5'd0;
	end else begin
		reg_addr <= storeRegAddr ? cmd_reg : (incRegAddr ? reg_addr + 1'b1 : reg_addr);
	end
end

// Memory address
reg storeMemAddrMsb;
reg storeMemAddrHi;
reg storeMemAddrLo;
reg incMemAddr;
reg storeMemAddrMsb_d;
reg storeMemAddrHi_d;
reg storeMemAddrLo_d;
reg incMemAddr_d;
always @(posedge clk or negedge nRst) begin
	if(!nRst) begin
		storeMemAddrMsb_d <= 1'b0;
		storeMemAddrHi_d <= 1'b0;
		storeMemAddrLo_d <= 1'b0;
		incMemAddr_d <= 1'b0;
	end else begin
		storeMemAddrMsb_d <= storeMemAddrMsb;
		storeMemAddrHi_d <= storeMemAddrHi;
		storeMemAddrLo_d <= storeMemAddrLo;
		incMemAddr_d <= incMemAddr;
	end
end

always @(posedge clk or negedge nRst) begin
	if(!nRst) begin
		mem_addr <= 17'b0;
	end else begin
		if(storeMemAddrMsb_d || storeMemAddrHi_d || storeMemAddrLo_d) begin
			mem_addr[16] <= storeMemAddrMsb_d ? cmd_memAddrMsb : mem_addr[16];
			mem_addr[15:8] <= storeMemAddrHi_d ? spi_rxData : mem_addr[15:8];
			mem_addr[7:0] <= storeMemAddrLo_d ? spi_rxData : mem_addr[7:0];
		end else if(incMemAddr_d) begin
			mem_addr <= mem_addr + 1'b1;
		end else begin
			mem_addr <= mem_addr;
		end
	end
end

// SPI data output
reg outputReg;
assign spi_txData = outputReg ? reg_di : mem_di;

// Write connections
assign reg_do = spi_rxData;
assign mem_do = spi_rxData;

// FSM
localparam IDLE = 4'd0, READ_REG_LOAD = 4'd1, READ_REG_SEND = 4'd2, READ_REG = 4'd3, WRITE_REG = 4'd4, READ_MEM_ADDR_HI = 4'd5;
localparam READ_MEM_ADDR_LO = 4'd6, READ_MEM_LOAD = 4'd7, READ_MEM_SEND = 4'd8, READ_MEM = 4'd9, WRITE_MEM_ADDR_HI = 4'd10;
localparam WRITE_MEM_ADDR_LO = 4'd11, WRITE_MEM = 4'd12, READ_MEM_ADDR_CALC = 4'd13, WRITE_MEM_ADDR_CALC = 4'd14;

reg [3:0] state;
reg [3:0] nextState;

always @(posedge clk or negedge nRst) state <= !nRst ? IDLE : nextState;
	
always @(*) begin
	case(state) 
	IDLE: begin
		if(spi_busy && spi_rxValid) begin
			case(cmd_cmd)
			CMD_READ_REG: begin
				// Starting read register command
				// Load the register
				storeRegAddr <= 1'b1;
				incRegAddr <= 1'b0;
				storeMemAddrMsb <= 1'b0;
				storeMemAddrHi <= 1'b0;
				storeMemAddrLo <= 1'b0;
				incMemAddr <= 1'b0;
				outputReg <= 1'b0;
				spi_txValid <= 1'b0;
				nextState <= READ_REG_LOAD;
				reg_we <= 1'b0;
				mem_we <= 1'b0;
				mem_req <= 1'b0;
			end
			CMD_WRITE_REG: begin
				// Starting write register command
				// Load the register address
				storeRegAddr <= 1'b1;
				incRegAddr <= 1'b0;
				storeMemAddrMsb <= 1'b0;
				storeMemAddrHi <= 1'b0;
				storeMemAddrLo <= 1'b0;
				incMemAddr <= 1'b0;
				outputReg <= 1'b0;
				spi_txValid <= 1'b0;
				nextState <= WRITE_REG;
				reg_we <= 1'b0;
				mem_we <= 1'b0;
				mem_req <= 1'b0;
			end
			CMD_READ_MEM: begin		
				// Starting read memory command
				// Load the memory address msb
				storeRegAddr <= 1'b0;
				incRegAddr <= 1'b0;
				storeMemAddrMsb <= 1'b1;
				storeMemAddrHi <= 1'b0;
				storeMemAddrLo <= 1'b0;
				incMemAddr <= 1'b0;
				outputReg <= 1'b0;
				spi_txValid <= 1'b0;
				nextState <= READ_MEM_ADDR_HI;
				reg_we <= 1'b0;
				mem_we <= 1'b0;
				mem_req <= 1'b0;		
			end
			CMD_WRITE_MEM: begin	
				// Starting write memory command
				// Load the memory address msb
				storeRegAddr <= 1'b0;
				incRegAddr <= 1'b0;
				storeMemAddrMsb <= 1'b1;
				storeMemAddrHi <= 1'b0;
				storeMemAddrLo <= 1'b0;
				incMemAddr <= 1'b0;
				outputReg <= 1'b0;
				spi_txValid <= 1'b0;
				nextState <= WRITE_MEM_ADDR_HI;
				reg_we <= 1'b0;
				mem_we <= 1'b0;
				mem_req <= 1'b0;				
			end
			endcase
		end else begin
			storeRegAddr <= 1'b0;
			incRegAddr <= 1'b0;
			storeMemAddrMsb <= 1'b0;
			storeMemAddrHi <= 1'b0;
			storeMemAddrLo <= 1'b0;
			incMemAddr <= 1'b0;
			outputReg <= 1'b0;
			spi_txValid <= 1'b0;
			nextState <= IDLE;
			reg_we <= 1'b0;
			mem_we <= 1'b0;
			mem_req <= 1'b0;
		end
	end
	READ_REG_LOAD: begin
		if(spi_busy) begin
			// Get the value from the register map
			storeRegAddr <= 1'b0;
			incRegAddr <= 1'b0;
			storeMemAddrMsb <= 1'b0;
			storeMemAddrHi <= 1'b0;
			storeMemAddrLo <= 1'b0;
			incMemAddr <= 1'b0;
			outputReg <= 1'b0;
			spi_txValid <= 1'b0;
			nextState <= READ_REG_SEND;
			reg_we <= 1'b0;
			mem_we <= 1'b0;
			mem_req <= 1'b0;
		end else begin
			storeRegAddr <= 1'b0;
			incRegAddr <= 1'b0;
			storeMemAddrMsb <= 1'b0;
			storeMemAddrHi <= 1'b0;
			storeMemAddrLo <= 1'b0;
			incMemAddr <= 1'b0;
			outputReg <= 1'b0;
			spi_txValid <= 1'b0;
			nextState <= IDLE;
			reg_we <= 1'b0;
			mem_we <= 1'b0;
			mem_req <= 1'b0;
		end
	end
	READ_REG_SEND: begin
		if(spi_busy) begin
			// Set the register value ready for sending back to the master
			storeRegAddr <= 1'b0;
			incRegAddr <= 1'b0;
			storeMemAddrMsb <= 1'b0;
			storeMemAddrHi <= 1'b0;
			storeMemAddrLo <= 1'b0;
			incMemAddr <= 1'b0;
			outputReg <= 1'b1;
			spi_txValid <= 1'b1;
			nextState <= READ_REG;
			reg_we <= 1'b0;
			mem_we <= 1'b0;
			mem_req <= 1'b0;
		end else begin
			storeRegAddr <= 1'b0;
			incRegAddr <= 1'b0;
			storeMemAddrMsb <= 1'b0;
			storeMemAddrHi <= 1'b0;
			storeMemAddrLo <= 1'b0;
			incMemAddr <= 1'b0;
			outputReg <= 1'b0;
			spi_txValid <= 1'b0;
			nextState <= IDLE;
			reg_we <= 1'b0;
			mem_we <= 1'b0;
			mem_req <= 1'b0;
		end
	end
	READ_REG: begin
		if(spi_busy) begin
			if(spi_rxValid) begin	
				// Read the next register value
				storeRegAddr <= 1'b0;
				incRegAddr <= 1'b1;
				storeMemAddrMsb <= 1'b0;
				storeMemAddrHi <= 1'b0;
				storeMemAddrLo <= 1'b0;
				incMemAddr <= 1'b0;
				outputReg <= 1'b0;
				spi_txValid <= 1'b0;
				nextState <= READ_REG_LOAD;
				reg_we <= 1'b0;
				mem_we <= 1'b0;
				mem_req <= 1'b0;
			end else begin
				storeRegAddr <= 1'b0;
				incRegAddr <= 1'b0;
				storeMemAddrMsb <= 1'b0;
				storeMemAddrHi <= 1'b0;
				storeMemAddrLo <= 1'b0;
				incMemAddr <= 1'b0;
				outputReg <= 1'b0;
				spi_txValid <= 1'b0;
				nextState <= READ_REG;
				reg_we <= 1'b0;
				mem_we <= 1'b0;
				mem_req <= 1'b0;
			end
		end else begin
			storeRegAddr <= 1'b0;
			incRegAddr <= 1'b0;
			storeMemAddrMsb <= 1'b0;
			storeMemAddrHi <= 1'b0;
			storeMemAddrLo <= 1'b0;
			incMemAddr <= 1'b0;
			outputReg <= 1'b0;
			spi_txValid <= 1'b0;
			nextState <= IDLE;
			reg_we <= 1'b0;
			mem_we <= 1'b0;
			mem_req <= 1'b0;
		end
	end
	WRITE_REG: begin
		if(spi_busy) begin
			if(spi_rxValid) begin	
				// Write the next register value
				storeRegAddr <= 1'b0;
				incRegAddr <= 1'b1;
				storeMemAddrMsb <= 1'b0;
				storeMemAddrHi <= 1'b0;
				storeMemAddrLo <= 1'b0;
				incMemAddr <= 1'b0;
				outputReg <= 1'b0;
				spi_txValid <= 1'b0;
				nextState <= WRITE_REG;
				reg_we <= 1'b1;
				mem_we <= 1'b0;
				mem_req <= 1'b0;
			end else begin
				storeRegAddr <= 1'b0;
				incRegAddr <= 1'b0;
				storeMemAddrMsb <= 1'b0;
				storeMemAddrHi <= 1'b0;
				storeMemAddrLo <= 1'b0;
				incMemAddr <= 1'b0;
				outputReg <= 1'b0;
				spi_txValid <= 1'b0;
				nextState <= WRITE_REG;
				reg_we <= 1'b0;
				mem_we <= 1'b0;
				mem_req <= 1'b0;
			end
		end else begin
			storeRegAddr <= 1'b0;
			incRegAddr <= 1'b0;
			storeMemAddrMsb <= 1'b0;
			storeMemAddrHi <= 1'b0;
			storeMemAddrLo <= 1'b0;
			incMemAddr <= 1'b0;
			outputReg <= 1'b0;
			spi_txValid <= 1'b0;
			nextState <= IDLE;
			reg_we <= 1'b0;
			mem_we <= 1'b0;
			mem_req <= 1'b0;
		end
	end
	READ_MEM_ADDR_HI: begin
		if(spi_busy) begin
			if(spi_rxValid) begin	
				// Write the next register value
				storeRegAddr <= 1'b0;
				incRegAddr <= 1'b0;
				storeMemAddrMsb <= 1'b0;
				storeMemAddrHi <= 1'b1;
				storeMemAddrLo <= 1'b0;
				incMemAddr <= 1'b0;
				outputReg <= 1'b0;
				spi_txValid <= 1'b0;
				nextState <= READ_MEM_ADDR_LO;
				reg_we <= 1'b0;
				mem_we <= 1'b0;
				mem_req <= 1'b0;
			end else begin
				storeRegAddr <= 1'b0;
				incRegAddr <= 1'b0;
				storeMemAddrMsb <= 1'b0;
				storeMemAddrHi <= 1'b0;
				storeMemAddrLo <= 1'b0;
				incMemAddr <= 1'b0;
				outputReg <= 1'b0;
				spi_txValid <= 1'b0;
				nextState <= READ_MEM_ADDR_HI;
				reg_we <= 1'b0;
				mem_we <= 1'b0;
				mem_req <= 1'b0;
			end
		end else begin
			storeRegAddr <= 1'b0;
			incRegAddr <= 1'b0;
			storeMemAddrMsb <= 1'b0;
			storeMemAddrHi <= 1'b0;
			storeMemAddrLo <= 1'b0;
			incMemAddr <= 1'b0;
			outputReg <= 1'b0;
			spi_txValid <= 1'b0;
			nextState <= IDLE;
			reg_we <= 1'b0;
			mem_we <= 1'b0;
			mem_req <= 1'b0;
		end
	end
	READ_MEM_ADDR_LO: begin
		if(spi_busy) begin
			if(spi_rxValid) begin	
				// Write the next register value
				storeRegAddr <= 1'b0;
				incRegAddr <= 1'b0;
				storeMemAddrMsb <= 1'b0;
				storeMemAddrHi <= 1'b0;
				storeMemAddrLo <= 1'b1;
				incMemAddr <= 1'b0;
				outputReg <= 1'b0;
				spi_txValid <= 1'b0;
				nextState <= READ_MEM_ADDR_CALC;
				reg_we <= 1'b0;
				mem_we <= 1'b0;
				mem_req <= 1'b0;
			end else begin
				storeRegAddr <= 1'b0;
				incRegAddr <= 1'b0;
				storeMemAddrMsb <= 1'b0;
				storeMemAddrHi <= 1'b0;
				storeMemAddrLo <= 1'b0;
				incMemAddr <= 1'b0;
				outputReg <= 1'b0;
				spi_txValid <= 1'b0;
				nextState <= READ_MEM_ADDR_LO;
				reg_we <= 1'b0;
				mem_we <= 1'b0;
				mem_req <= 1'b0;
			end
		end else begin
			storeRegAddr <= 1'b0;
			incRegAddr <= 1'b0;
			storeMemAddrMsb <= 1'b0;
			storeMemAddrHi <= 1'b0;
			storeMemAddrLo <= 1'b0;
			incMemAddr <= 1'b0;
			outputReg <= 1'b0;
			spi_txValid <= 1'b0;
			nextState <= IDLE;
			reg_we <= 1'b0;
			mem_we <= 1'b0;
			mem_req <= 1'b0;
		end
	end
	READ_MEM_ADDR_CALC: begin
		if(spi_busy) begin
			storeRegAddr <= 1'b0;
			incRegAddr <= 1'b0;
			storeMemAddrMsb <= 1'b0;
			storeMemAddrHi <= 1'b0;
			storeMemAddrLo <= 1'b0;
			incMemAddr <= 1'b0;
			outputReg <= 1'b0;
			spi_txValid <= 1'b0;
			nextState <= READ_MEM_LOAD;
			reg_we <= 1'b0;
			mem_we <= 1'b0;
			mem_req <= 1'b0;
		end else begin
			storeRegAddr <= 1'b0;
			incRegAddr <= 1'b0;
			storeMemAddrMsb <= 1'b0;
			storeMemAddrHi <= 1'b0;
			storeMemAddrLo <= 1'b0;
			incMemAddr <= 1'b0;
			outputReg <= 1'b0;
			spi_txValid <= 1'b0;
			nextState <= IDLE;
			reg_we <= 1'b0;
			mem_we <= 1'b0;
			mem_req <= 1'b0;
		end
	end
	READ_MEM_LOAD: begin
		if(spi_busy) begin
			// Send out the memory request
			storeRegAddr <= 1'b0;
			incRegAddr <= 1'b0;
			storeMemAddrMsb <= 1'b0;
			storeMemAddrHi <= 1'b0;
			storeMemAddrLo <= 1'b0;
			incMemAddr <= 1'b0;
			outputReg <= 1'b0;
			spi_txValid <= 1'b0;
			nextState <= READ_MEM_SEND;
			reg_we <= 1'b0;
			mem_we <= 1'b0;
			mem_req <= 1'b1;
		end else begin
			storeRegAddr <= 1'b0;
			incRegAddr <= 1'b0;
			storeMemAddrMsb <= 1'b0;
			storeMemAddrHi <= 1'b0;
			storeMemAddrLo <= 1'b0;
			incMemAddr <= 1'b0;
			outputReg <= 1'b0;
			spi_txValid <= 1'b0;
			nextState <= IDLE;
			reg_we <= 1'b0;
			mem_we <= 1'b0;
			mem_req <= 1'b0;
		end
	end
	READ_MEM_SEND: begin
		if(spi_busy) begin
			// Set the memory value ready for sending back
			if(mem_grnt) begin
				storeRegAddr <= 1'b0;
				incRegAddr <= 1'b0;
				storeMemAddrMsb <= 1'b0;
				storeMemAddrHi <= 1'b0;
				storeMemAddrLo <= 1'b0;
				incMemAddr <= 1'b0;
				outputReg <= 1'b0;
				spi_txValid <= 1'b1;
				nextState <= READ_MEM;
				reg_we <= 1'b0;
				mem_we <= 1'b0;
				mem_req <= 1'b0;
			end else begin
				storeRegAddr <= 1'b0;
				incRegAddr <= 1'b0;
				storeMemAddrMsb <= 1'b0;
				storeMemAddrHi <= 1'b0;
				storeMemAddrLo <= 1'b0;
				incMemAddr <= 1'b0;
				outputReg <= 1'b0;
				spi_txValid <= 1'b0;
				nextState <= READ_MEM_SEND;
				reg_we <= 1'b0;
				mem_we <= 1'b0;
				mem_req <= 1'b0;
			end
		end else begin
			storeRegAddr <= 1'b0;
			incRegAddr <= 1'b0;
			storeMemAddrMsb <= 1'b0;
			storeMemAddrHi <= 1'b0;
			storeMemAddrLo <= 1'b0;
			incMemAddr <= 1'b0;
			outputReg <= 1'b0;
			spi_txValid <= 1'b0;
			nextState <= IDLE;
			reg_we <= 1'b0;
			mem_we <= 1'b0;
			mem_req <= 1'b0;
		end
	end
	READ_MEM: begin
		if(spi_busy) begin
			if(spi_rxValid) begin	
				// Read the next memory value
				storeRegAddr <= 1'b0;
				incRegAddr <= 1'b0;
				storeMemAddrMsb <= 1'b0;
				storeMemAddrHi <= 1'b0;
				storeMemAddrLo <= 1'b0;
				incMemAddr <= 1'b1;
				outputReg <= 1'b0;
				spi_txValid <= 1'b0;
				nextState <= READ_MEM_ADDR_CALC;
				reg_we <= 1'b0;
				mem_we <= 1'b0;
				mem_req <= 1'b0;
			end else begin
				storeRegAddr <= 1'b0;
				incRegAddr <= 1'b0;
				storeMemAddrMsb <= 1'b0;
				storeMemAddrHi <= 1'b0;
				storeMemAddrLo <= 1'b0;
				incMemAddr <= 1'b0;
				outputReg <= 1'b0;
				spi_txValid <= 1'b0;
				nextState <= READ_MEM;
				reg_we <= 1'b0;
				mem_we <= 1'b0;
				mem_req <= 1'b0;
			end
		end else begin
			storeRegAddr <= 1'b0;
			incRegAddr <= 1'b0;
			storeMemAddrMsb <= 1'b0;
			storeMemAddrHi <= 1'b0;
			storeMemAddrLo <= 1'b0;
			incMemAddr <= 1'b0;
			outputReg <= 1'b0;
			spi_txValid <= 1'b0;
			nextState <= IDLE;
			reg_we <= 1'b0;
			mem_we <= 1'b0;
			mem_req <= 1'b0;
		end
	end
	WRITE_MEM_ADDR_HI: begin
		if(spi_busy) begin
			if(spi_rxValid) begin	
				// Write the next register value
				storeRegAddr <= 1'b0;
				incRegAddr <= 1'b0;
				storeMemAddrMsb <= 1'b0;
				storeMemAddrHi <= 1'b1;
				storeMemAddrLo <= 1'b0;
				incMemAddr <= 1'b0;
				outputReg <= 1'b0;
				spi_txValid <= 1'b0;
				nextState <= WRITE_MEM_ADDR_LO;
				reg_we <= 1'b0;
				mem_we <= 1'b0;
				mem_req <= 1'b0;
			end else begin
				storeRegAddr <= 1'b0;
				incRegAddr <= 1'b0;
				storeMemAddrMsb <= 1'b0;
				storeMemAddrHi <= 1'b0;
				storeMemAddrLo <= 1'b0;
				incMemAddr <= 1'b0;
				outputReg <= 1'b0;
				spi_txValid <= 1'b0;
				nextState <= WRITE_MEM_ADDR_HI;
				reg_we <= 1'b0;
				mem_we <= 1'b0;
				mem_req <= 1'b0;
			end
		end else begin
			storeRegAddr <= 1'b0;
			incRegAddr <= 1'b0;
			storeMemAddrMsb <= 1'b0;
			storeMemAddrHi <= 1'b0;
			storeMemAddrLo <= 1'b0;
			incMemAddr <= 1'b0;
			outputReg <= 1'b0;
			spi_txValid <= 1'b0;
			nextState <= IDLE;
			reg_we <= 1'b0;
			mem_we <= 1'b0;
			mem_req <= 1'b0;
		end
	end
	WRITE_MEM_ADDR_LO: begin
		if(spi_busy) begin
			if(spi_rxValid) begin	
				// Write the next register value
				storeRegAddr <= 1'b0;
				incRegAddr <= 1'b0;
				storeMemAddrMsb <= 1'b0;
				storeMemAddrHi <= 1'b0;
				storeMemAddrLo <= 1'b1;
				incMemAddr <= 1'b0;
				outputReg <= 1'b0;
				spi_txValid <= 1'b0;
				nextState <= WRITE_MEM;
				reg_we <= 1'b0;
				mem_we <= 1'b0;
				mem_req <= 1'b0;
			end else begin
				storeRegAddr <= 1'b0;
				incRegAddr <= 1'b0;
				storeMemAddrMsb <= 1'b0;
				storeMemAddrHi <= 1'b0;
				storeMemAddrLo <= 1'b0;
				incMemAddr <= 1'b0;
				outputReg <= 1'b0;
				spi_txValid <= 1'b0;
				nextState <= WRITE_MEM_ADDR_LO;
				reg_we <= 1'b0;
				mem_we <= 1'b0;
				mem_req <= 1'b0;
			end
		end else begin
			storeRegAddr <= 1'b0;
			incRegAddr <= 1'b0;
			storeMemAddrMsb <= 1'b0;
			storeMemAddrHi <= 1'b0;
			storeMemAddrLo <= 1'b0;
			incMemAddr <= 1'b0;
			outputReg <= 1'b0;
			spi_txValid <= 1'b0;
			nextState <= IDLE;
			reg_we <= 1'b0;
			mem_we <= 1'b0;
			mem_req <= 1'b0;
		end
	end
	WRITE_MEM: begin
		if(spi_busy) begin
			if(spi_rxValid) begin	
				// Write the next register value
				storeRegAddr <= 1'b0;
				incRegAddr <= 1'b0;
				storeMemAddrMsb <= 1'b0;
				storeMemAddrHi <= 1'b0;
				storeMemAddrLo <= 1'b0;
				incMemAddr <= 1'b1;
				outputReg <= 1'b0;
				spi_txValid <= 1'b0;
				nextState <= WRITE_MEM;
				reg_we <= 1'b0;
				mem_we <= 1'b1;
				mem_req <= 1'b1;
			end else begin
				storeRegAddr <= 1'b0;
				incRegAddr <= 1'b0;
				storeMemAddrMsb <= 1'b0;
				storeMemAddrHi <= 1'b0;
				storeMemAddrLo <= 1'b0;
				incMemAddr <= 1'b0;
				outputReg <= 1'b0;
				spi_txValid <= 1'b0;
				nextState <= WRITE_MEM;
				reg_we <= 1'b0;
				mem_we <= 1'b0;
				mem_req <= 1'b0;
			end
		end else begin
			storeRegAddr <= 1'b0;
			incRegAddr <= 1'b0;
			storeMemAddrMsb <= 1'b0;
			storeMemAddrHi <= 1'b0;
			storeMemAddrLo <= 1'b0;
			incMemAddr <= 1'b0;
			outputReg <= 1'b0;
			spi_txValid <= 1'b0;
			nextState <= IDLE;
			reg_we <= 1'b0;
			mem_we <= 1'b0;
			mem_req <= 1'b0;
		end
	end
	default: begin
		storeRegAddr <= 1'b0;
		incRegAddr <= 1'b0;
		storeMemAddrMsb <= 1'b0;
		storeMemAddrHi <= 1'b0;
		storeMemAddrLo <= 1'b0;
		incMemAddr <= 1'b0;
		outputReg <= 1'b0;
		nextState <= IDLE;
		spi_txValid <= 1'b0;
		reg_we <= 1'b0;
		mem_we <= 1'b0;
		mem_req <= 1'b0;
	end
	endcase
end
	
endmodule