module SpiSlave #(
		parameter WIDTH = 8
	) (
		// SPI interface
		input sclk,
		input sdi,
		output sdo,
		input nCs,
		
		// core interface
		input clk,
		input nRst,
		
		output [WIDTH-1:0] rxData,
		output rxValid,
		
		input [WIDTH-1:0] txData,
		input txValid,
		
		output txBusy
	);
	
	localparam COUNTER_WIDTH = $clog2(WIDTH);
	
	// Input shift register
	reg [WIDTH-1:0] shiftInReg;
	wire [WIDTH-1:0] nextInData;
	assign nextInData = {sdi, shiftInReg[WIDTH-1:1]};
	always @(posedge sclk or posedge nCs)
	begin
		if(nCs) begin
			shiftInReg <= {WIDTH{1'b1}};
		end else begin
			shiftInReg <= nextInData;
		end
	end
	
	// Output shift register	
	reg [WIDTH-1:0] shiftOutReg;
	reg [WIDTH-1:0] nextOut;
	wire loadNextOut;
	assign sdo = nCs ? 1'bZ : shiftOutReg[0];
	always @(posedge sclk or posedge nCs)
	begin
		if(nCs) begin
			shiftOutReg <= {WIDTH{1'b1}};
		end else begin
			if(loadNextOut) begin
				shiftOutReg <= nextOut;
			end else begin
				shiftOutReg <= {1'b1, shiftOutReg[WIDTH-1:1]};
			end
		end
	end
	
	// Shift counter
	reg [COUNTER_WIDTH - 1:0] counter;
	wire lastRxBit;
	wire dataReady;
	assign loadNextOut = counter == WIDTH - 1;
	assign dataReady = counter == WIDTH - 1;
	assign lastRxBit = counter == WIDTH - 1;
	always @(posedge sclk or posedge nCs)
	begin
		if(nCs) begin
			counter <= {COUNTER_WIDTH{1'b0}};
		end else begin
			counter <= dataReady ? {COUNTER_WIDTH{1'b0}} : counter + 1'b1;
		end
	end
	
	// Output data logic
	Data_CrossDomain #(.WIDTH(WIDTH)) dataInSynchronizer (
		.rst(!nRst), 
		.clkA(sclk), .validA(lastRxBit), .dataA(nextInData),
		.clkB(clk), .validB(rxValid), .dataB(rxData));
		
	// Input data logic
	wire nextOutValid;
	wire [WIDTH-1:0] nextOutValue;
	Data_CrossDomain #(.WIDTH(WIDTH)) dataOutSynchronizer (
		.rst(!nRst || nCs),
		.clkA(clk), .validA(txValid), .dataA(txData),
		.clkB(sclk), .validB(nextOutValid), .dataB(nextOutValue));
	always @(posedge sclk or posedge nCs) nextOut <= nCs ? {WIDTH{1'b1}} : (nextOutValid ? nextOutValue : nextOut);
		
	// TxBusy logic
	wire txBusy1;
	reg txBusy2;
	Signal_CrossDomain csSynchronizer (!nRst, !nCs, clk, txBusy1);
	always @(posedge clk or negedge nRst) txBusy2 <= !nRst ? 1'b0 : txBusy1;
	assign txBusy = txBusy1 || txBusy2;
	
endmodule