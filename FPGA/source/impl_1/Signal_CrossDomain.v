module Signal_CrossDomain #(
	parameter RESET_VAL = 1'b0
	) (
	input rst,
    input SignalIn_clkA,
    input clkB,
    output SignalOut_clkB
);

// We use a two-stages shift-register to synchronize SignalIn_clkA to the clkB clock domain
reg [1:0] SyncA_clkB;
always @(posedge clkB or posedge rst) SyncA_clkB[0] <= rst ? RESET_VAL : SignalIn_clkA;   // notice that we use clkB
always @(posedge clkB or posedge rst) SyncA_clkB[1] <= rst ? RESET_VAL : SyncA_clkB[0];   // notice that we use clkB

assign SignalOut_clkB = SyncA_clkB[1];  // new signal synchronized to (=ready to be used in) clkB domain
endmodule