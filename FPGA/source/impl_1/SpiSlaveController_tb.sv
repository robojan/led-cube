module SpiSlaveController_tb();

	parameter CLK_PERIOD = 10;
	parameter SPI_CLK_PERIOD = 18;
	
	reg sclk = 0;
	reg sdi = 0;
	wire sdo;
	reg nCs = 1;
	
	reg clk = 0;
	reg nRst = 1;
	wire [7:0] rxData;
	wire rxValid;
	wire [7:0] txData;
	wire txValid;
	wire txBusy;
	
	wire [4:0] reg_addr;
	wire reg_we;
	wire[7:0] reg_do;
	reg [7:0] reg_di = 0;
	
	wire [16:0] mem_addr;
	wire mem_we;
	wire mem_req;
	wire [7:0] mem_do;
	reg [7:0] mem_di = 0;
	reg mem_grnt = 0;
	
	
	SpiSlave #(.WIDTH(8)) uutIf (.sclk(sclk), .sdi(sdi), .sdo(sdo), .nCs(nCs), .clk(clk), .nRst(nRst), .rxData(rxData), .rxValid(rxValid), .txData(txData), .txValid(txValid), .txBusy(txBusy));
	SpiSlaveController uut(.clk(clk), .nRst(nRst), .spi_rxData(rxData), .spi_rxValid(rxValid), .spi_txData(txData), .spi_txValid(txValid), .spi_busy(txBusy), 
		.reg_addr(reg_addr), .reg_we(reg_we), .reg_do(reg_do), .reg_di(reg_di),
		.mem_addr(mem_addr), .mem_we(mem_we), .mem_req(mem_req), .mem_do(mem_do), .mem_di(mem_di), .mem_grnt(mem_grnt));
		
	always @(posedge clk) reg_di <= {3'b100, reg_addr};
		
	always @(posedge clk or negedge nRst) begin
		if(mem_req) begin
			mem_di <= mem_addr[7:0];
			mem_grnt <= 1;
		end else begin
			mem_di <= 0;
			mem_grnt <= 0;
		end
	end
	
	always #(CLK_PERIOD/2) clk = !clk;
	initial begin
		#(CLK_PERIOD*2) nRst = 0;
		#(CLK_PERIOD*2) nRst = 1;
		#(CLK_PERIOD*2);
		// Read register 4
		transaction({8'h04, 8'hff, 8'hff, 8'hff});
		#(CLK_PERIOD*5);
		// Read register f
		transaction({8'h0f, 8'hff, 8'hff, 8'hff, 8'hff});
		#(CLK_PERIOD*5);
		// Write register 0
		transaction({8'h40, 8'h01, 8'h23, 8'h45, 8'h67, 8'h89});
		#(CLK_PERIOD*5);
		// Write register c
		transaction({8'h4c, 8'hcc, 8'hdd, 8'hee});
		#(CLK_PERIOD*5);
		// Read memory 0x12345
		transaction({8'hc1, 8'h23, 8'h45, 8'hff, 8'hff, 8'hff, 8'hff, 8'hff, 8'hff});
		#(CLK_PERIOD*5);
		// Read memory 0x0
		transaction({8'hc0, 8'h00, 8'h00, 8'hff, 8'hff, 8'hff});
		#(CLK_PERIOD*5);
		// Read memory 0x1FFFF
		transaction({8'hc1, 8'hFF, 8'hFF, 8'hff, 8'hff, 8'hff});
		#(CLK_PERIOD*5);
		// Write memory 0xFFFF
		transaction({8'h80, 8'hFF, 8'hFF, 8'haa, 8'h88, 8'h66});
		#(CLK_PERIOD*5);
		// Write memory 0x0
		transaction({8'h80, 8'h00, 8'h00, 8'h01, 8'h23, 8'h45, 8'h67});
		#(CLK_PERIOD*5);
		// Write memory 0x1FFFF
		transaction({8'h81, 8'hFF, 8'hFF, 8'h22, 8'h11, 8'hab, 8'h92});
		#(CLK_PERIOD*5);
		$finish;
	end
	
	function string array2string(input bit [7:0] a[]);
		automatic string result = "";
		foreach(a[i]) begin
			result = {result, $sformatf("%02X ", a[i])};
		end
		return result;
	endfunction	

	task transaction(input bit [7:0] t_tx[]);
		automatic bit transaction_busy = 1;
		automatic bit [7:0] t_rx[];
		
		$display("----------------------------------------------------------------------");	
		fork
			// SPI master
			begin
				t_rx = new[t_tx.size()];
				
				// Start the transaction
				nCs = 0;
				
				foreach(t_tx[i]) begin
					for(int b = 0; b < 8; b++) begin
						sclk = 0;
						sdi = t_tx[i][b];
						#(SPI_CLK_PERIOD / 2);
						sclk = 1;
						t_rx[i][b] = sdo;
						#(SPI_CLK_PERIOD / 2);
					end
				end
				nCs = 1;
				sclk = 0;
				$display("SPI Master done: %2d bytes TX: %s RX: %s", t_tx.size(), array2string(t_tx), array2string(t_rx));
				#(SPI_CLK_PERIOD*2);
				transaction_busy = 0;
			end
		join
		t_rx.delete;
		$display("----------------------------------------------------------------------");
	endtask

endmodule