module SP256K (
	input [13:0] AD,
	input [15:0] DI,
	input [3:0] MASKWE,
	input WE,
	input CS,
	input CK,
	input STDBY,
	input SLEEP,
	input PWROFF_N,
	output reg [15:0] DO);
	
	
reg [15:0]mem[0:16384];

initial begin : SPRAM_INIT
        integer i;
        for (i = 0; i < 16384; i = i + 1) begin
            mem[i] = i;
        end
    end

always @(posedge CK) begin
	if(CS & WE) begin
		if(MASKWE[3]) mem[AD][15:12] <= DI[15:12];
		if(MASKWE[2]) mem[AD][11:8] <= DI[11:8];
		if(MASKWE[1]) mem[AD][7:4] <= DI[7:4];
		if(MASKWE[0]) mem[AD][3:0] <= DI[3:0];
	end
end
always @(posedge CK) begin
	if(CS) begin
		DO <= mem[AD];
	end
end
	
endmodule