module SlowClockDivider(
	input clk,
	input nRst,
	output slowClk);
	
	reg [7:0] stage1;
	always @(posedge clk or negedge nRst)
	begin
		if(!nRst) begin
			stage1 <= 0;
		end else begin
			stage1 <= stage1 + 1'b1;
		end
	end
	wire stage1_clk;
	assign stage1_clk = stage1[7];
	
	reg [7:0] stage2;
	always @(posedge stage1_clk or negedge nRst)
	begin
		if(!nRst) begin
			stage2 <= 0;
		end else begin
			stage2 <= stage2 + 1'b1;
		end
	end
	wire stage2_clk;
	assign stage2_clk = stage2[7];
	
	reg [7:0] stage3;
	always @(posedge stage2_clk or negedge nRst)
	begin
		if(!nRst) begin
			stage3 <= 0;
		end else begin
			stage3 <= stage3 + 1'b1;
		end
	end
	wire stage3_clk;
	assign stage3_clk = stage3[7];
	
	reg [7:0] stage4;
	always @(posedge stage3_clk or negedge nRst)
	begin
		if(!nRst) begin
			stage4 <= 0;
		end else begin
			stage4 <= stage4 + 1'b1;
		end
	end
	wire stage4_clk;
	assign stage4_clk = stage4[7];
	
	assign slowClk = stage4[1];
	
endmodule