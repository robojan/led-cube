module EdgeDetect(
	input clk,
	input nRst,
	
	input i,
	output o);
	
	parameter ACTIVE_HIGH = 1'b1;
	
	localparam DELAY_RESET_VAL = ACTIVE_HIGH ? 1'b0 : 1'b1;
	
	// Delay input
	reg d;
	always @(posedge clk or negedge nRst)
	begin
		if(!nRst) begin
			d <= DELAY_RESET_VAL;
		end else begin
			d <= i;
		end
	end
	
	// Output
	assign o = i != d && i == ACTIVE_HIGH;

endmodule