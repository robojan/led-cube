module RegisterMap (
	input clk,
	input nRst,
	
	// SPI interface
	input [4:0] spi_addr,
	input spi_we,
	input [7:0] spi_di,
	output reg [7:0] spi_do,
	
	// Control register 
	output reg ctrl_en,
	
	// Status register
	input sr_running,
	
	// Intensity register
	output reg [2:0] intensity,
	
	// Position register
	input reg [3:0] pr_active_plane
	);
	
// Registers
wire [7:0] reg_id;
assign reg_id = 8'h15;

wire [7:0] reg_version;
assign reg_version = 8'h01;

wire [7:0] reg_ctrl;
assign reg_ctrl = {7'b0, ctrl_en};

wire [7:0] reg_sr;
assign reg_sr = {7'b0, sr_running};

wire [7:0] reg_intensity;
assign reg_intensity = {5'b0, intensity };

wire [7:0] reg_pos;
assign reg_pos = {4'b0, pr_active_plane};

reg [7:0] reg_test;
wire [7:0] reg_testWriteVal;
assign reg_testWriteVal = spi_di;


// SPI Read logic
always @(posedge clk or negedge nRst) begin
	if(!nRst) begin
		spi_do <= 8'hff;
	end else begin
		case(spi_addr) 
		5'h0: spi_do <= reg_id;
		5'h1: spi_do <= reg_version; 
		5'h2: spi_do <= reg_ctrl;
		5'h3: spi_do <= reg_sr;
		5'h4: spi_do <= reg_intensity;
		5'h5: spi_do <= reg_pos;
		5'h1f: spi_do <= reg_test;
		default: spi_do <= 8'hff;
		endcase
	end
end

// SPI write logic
always @(posedge clk or negedge nRst) begin
	if(!nRst) begin
		intensity <= 3'h0;
		reg_test <= 8'h0;
		ctrl_en <= 1'b0;
	end else begin
		if(spi_we) begin
			case(spi_addr)
			5'h2: begin
				intensity <= intensity;
				reg_test <= reg_test;
				ctrl_en <= spi_di[0];
			end
			5'h4: begin
				intensity <= spi_di[2:0];
				reg_test <= reg_test;
				ctrl_en <= ctrl_en;
			end
			5'h1f: begin
				intensity <= intensity;
				reg_test <= spi_di;
				ctrl_en <= ctrl_en;				
			end
			default: begin
				intensity <= intensity;
				reg_test <= reg_test;
				ctrl_en <= ctrl_en;
			end
			endcase
		end else begin
			intensity <= intensity;
			reg_test <= reg_test;
			ctrl_en <= ctrl_en;
		end
	end
end
	
endmodule