`timescale 1ns/1ps
module LCDriver_tb ();
	
parameter NUM_ROWS = 10;
parameter NUM_COLUMNS = 60;
parameter NUM_STEPS = 8;

reg clk, nRst;

reg enable;
wire running;
wire frameSync;
wire planeSync;

reg [15:0] mem_di;
wire [12:0] mem_addr;
wire mem_req;
reg mem_valid;

wire lc_clk;
wire [9:0] lc_data;
wire lc_lat;
wire [9:0] lc_plane;
wire lc_blank;

integer i = 0;

LCDriver inst (
	.clk(clk), .nRst(nRst), .enable(enable), .running(running), .frameSync(frameSync), .planeSync(planeSync), 
	.mem_di(mem_di), .mem_addr(mem_addr), 
	.mem_req(mem_req), .mem_valid(mem_valid), .lc_clk(lc_clk), .lc_data(lc_data), .lc_lat(lc_lat), .lc_plane(lc_plane), .lc_blank(lc_blank));	

reg [1:0]delayedMemValid;
reg [16:0]delayedMemDi1;
reg [16:0]delayedMemDi2;
	
always @(posedge clk)
begin
	if(mem_req) begin
		//delayedMemDi1[7:0] <= (((mem_addr[8:0]*2) % 64)*10 + ((mem_addr[8:0]*2) / 64))%256;
		//delayedMemDi1[15:8] <= (((mem_addr[8:0]*2 + 1) % 64)*10 + ((mem_addr[8:0]*2) / 64))%256;
		delayedMemDi1[7:0] <= mem_addr[12:9] == 4'd0 ? 8'hff : 8'h00; 
		delayedMemDi1[15:8] <= mem_addr[12:9] == 4'd0 ? 8'hff : 8'h00;
		//delayedMemDi1[7:0] <= {mem_addr[12:9], 4'b0};
		//delayedMemDi1[15:8] <= {mem_addr[12:9], 4'b0};
		delayedMemValid[0] <= 1'b1;
	end else begin
		delayedMemDi1 <= 16'b0;
		delayedMemValid <= 1'b0;
	end
	delayedMemValid[1] <= delayedMemValid[0];
	mem_valid <= delayedMemValid[1];
	delayedMemDi2 <= delayedMemDi1;
	mem_di <= delayedMemDi2;
end

initial begin
	clk = 0;
	nRst = 1;
	enable = 0;
	
	#10	nRst = 0;
	#20 nRst = 1;
	$display("Reset done");
	#40;
	
	
	enable = 1; 
	
	#10000000;
	
	$finish;
end

always #4.166 clk = !clk;
endmodule