module top_tb();

	parameter CLK_PERIOD = 10;
	parameter SPI_CLK_PERIOD = 18;

	reg clk = 0;
	reg nRst = 1;
	
	reg spi_sck = 0;
	reg spi_di = 0;
	wire spi_do;
	reg spi_nCs = 1;
	wire cubeBlank;
	wire planeBlank;
	
	wire [9:0] lc_p;
	wire lc_blank;
	wire lc_latch;
	wire lc_clk;
	wire [9:0] lc_d;


	// Main RAM
	reg lcEnable = 0;
	wire lcRunning;
	
	wire [16:0] spiMemAddr;
	wire spiMemWe;
	wire spiMemReq;
	wire [7:0]spiMemDo;
	wire [7:0]spiMemDi;
	wire spiMemGrnt;
	
	wire [12:0]lcMemAddr;
	wire lcMemReq;
	wire lcMemGrnt;
	wire [15:0]lcMemData;
	MainRamArbiter ram(.clk(clk), .nRst(nRst), 
		.spi_addr(spiMemAddr), .spi_di(spiMemDo), .spi_do(spiMemDi), 
		.spi_we(spiMemWe), .spi_req(spiMemReq), .spi_grnt(spiMemGrnt),
		.lc_addr({3'b000, lcMemAddr}), .lc_do(lcMemData), .lc_req(lcMemReq), .lc_grnt(lcMemGrnt));
	
	// LED Cube driver
	LCDriver lcDriver(.clk(clk), .nRst(nRst), 
		.enable(lcEnable), .running(lcRunning), .frameSync(cubeBlank), .planeSync(planeBlank),
		.mem_di(lcMemData), .mem_addr(lcMemAddr), .mem_req(lcMemReq), .mem_valid(lcMemGrnt), 
		.lc_clk(lc_clk), .lc_data(lc_d), .lc_lat(lc_latch), .lc_plane(lc_p), .lc_blank(lc_blank));
	
	// SPI interface
	wire [7:0] spiRxData;
	wire spiRxValid;
	wire [7:0] spiTxData;
	wire spiTxValid;
	wire spiTxBusy;
	SpiSlave spi (
		.sclk(spi_sck), .sdi(spi_di), .sdo(spi_do), .nCs(spi_nCs),
		.clk(clk), .nRst(nRst), .rxData(spiRxData), .rxValid(spiRxValid),
		.txData(spiTxData), .txValid(spiTxValid), .txBusy(spiTxBusy));
		
	wire [4:0] spiRegAddr;
	wire spiRegWe;
	wire [7:0] spiRegDo;
	wire [7:0] spiRegDi;
	
	SpiSlaveController spiController (
		.clk(clk), .nRst(nRst),
		.spi_rxData(spiRxData), .spi_rxValid(spiRxValid), .spi_txData(spiTxData), .spi_txValid(spiTxValid), .spi_busy(spiTxBusy),
		.reg_addr(spiRegAddr), .reg_we(spiRegWe), .reg_do(spiRegDo), .reg_di(spiRegDi), 
		.mem_addr(spiMemAddr), .mem_we(spiMemWe), .mem_req(spiMemReq), .mem_do(spiMemDo), .mem_di(spiMemDi), .mem_grnt(spiMemGrnt));
		
	RegisterMap regmap(.clk(clk), .nRst(nRst),
		.spi_addr(spiRegAddr), .spi_we(spiRegWe), .spi_di(spiRegDo), .spi_do(spiRegDi));
		
	
	always #(CLK_PERIOD/2) clk = !clk;
	initial begin
		#(CLK_PERIOD*2) nRst = 0;
		#(CLK_PERIOD*2) nRst = 1;
		#(CLK_PERIOD*2);
		fillPlane(0, 8'hff);
		fillPlane(1, 8'h00);
		fillPlane(2, 8'h00);
		fillPlane(3, 8'h00);
		fillPlane(4, 8'h00);
		fillPlane(5, 8'h00);
		fillPlane(6, 8'h00);
		fillPlane(7, 8'h00);
		fillPlane(8, 8'h00);
		fillPlane(9, 8'h00);
		#(CLK_PERIOD*5);
		lcEnable = 1;
		#(CLK_PERIOD*5);
		@(negedge lcRunning);
		#(CLK_PERIOD*5);		
		$finish;
	end
	
	function string array2string(input bit [7:0] a[]);
		automatic string result = "";
		foreach(a[i]) begin
			result = {result, $sformatf("%02X ", a[i])};
		end
		return result;
	endfunction	
	
	task fillPlane(input int plane, input bit [7:0] val);
		automatic bit [7:0] txData [1024+3];
		automatic bit [16:0] addr = plane * 1024;
		foreach(txData[j]) txData[j] = val;
		txData[0] = {7'h40, addr[16]};
		txData[1] = addr[15:8];
		txData[2] = addr[7:0];
		transaction(txData);
	endtask

	task transaction(input bit [7:0] t_tx[]);
		automatic bit transaction_busy = 1;
		automatic bit [7:0] t_rx[];
		
		$display("----------------------------------------------------------------------");	
		fork
			// SPI master
			begin
				t_rx = new[t_tx.size()];
				
				// Start the transaction
				spi_nCs = 0;
				
				foreach(t_tx[i]) begin
					for(int b = 0; b < 8; b++) begin
						spi_sck = 0;
						spi_di = t_tx[i][b];
						#(SPI_CLK_PERIOD / 2);
						spi_sck = 1;
						t_rx[i][b] = spi_do;
						#(SPI_CLK_PERIOD / 2);
					end
				end
				spi_nCs = 1;
				spi_sck = 0;
				$display("SPI Master done: %2d bytes TX: %s RX: %s", t_tx.size(), array2string(t_tx), array2string(t_rx));
				#(SPI_CLK_PERIOD*2);
				transaction_busy = 0;
			end
		join
		t_rx.delete;
		$display("----------------------------------------------------------------------");
	endtask
	
endmodule