module IntensityGenerator_tb();

parameter NUM_ROWS = 10;
parameter NUM_COLUMNS = 60;
parameter NUM_STEPS = 8;

reg clk, nRst;
reg [4:0]comp_msb;
reg load_plane;
wire busy;
reg [15:0] in_di;
wire [8:0] in_addr;
wire in_req;
reg in_valid;
wire [NUM_ROWS * NUM_STEPS - 1:0] out_do;
wire [5:0] out_addr;
wire out_req;
reg out_grnt;
integer i = 0;

IntensityGenerator 
	#(.NUM_ROWS(NUM_ROWS), .NUM_COLUMNS(NUM_COLUMNS), .NUM_STEPS(NUM_STEPS)) 
	inst 
	(.clk(clk), .nRst(nRst), .comp_msb(comp_msb), .load_plane(load_plane), .busy(busy), .in_di(in_di), 
	.in_addr(in_addr), .in_valid(in_valid), .in_req(in_req), .out_do(out_do), .out_addr(out_addr), .out_req(out_req), .out_grnt(out_grnt));

RamMock #(.DATA_WIDTH(80),.ADDR_WIDTH(6)) mem(.clk(clk), .nRst(nRst), .data_in(out_do), .addr_in(out_addr), .we(out_req), .written(out_grnt), .data_out());		 

reg pre_in_valid;
reg [15:0] pre_in_di;
always @(posedge clk or negedge nRst)
begin								
	if(!nRst) begin
		in_valid <= 0;
		in_di <= 0;
	end else begin
		in_valid <= pre_in_valid;
		in_di <= pre_in_di;
	end
end	
	

always @(posedge clk)
begin
	if(in_req) begin
		in_di[7:0] <= (((in_addr[7:0]*2) % 60)*10 + ((in_addr[7:0]*2) / 60))%256;
		in_di[15:8] <= (((in_addr[7:0]*2 + 1) % 60)*10 + ((in_addr[7:0]*2) / 60))%256;
		in_valid <= 1'b1;
	end else begin
		in_di <= 16'b0;
		in_valid <= 1'b0;
	end
end

initial begin
	clk = 0;
	nRst = 1;
	load_plane = 0;
	in_di = 0;	
	comp_msb =0 ;
	
	#10	nRst = 0;
	#20 nRst = 1;
	$display("Reset done");
	#40;
	
	
	load_plane = 1; 
	@(posedge clk)
	load_plane = 0;
	@(negedge busy)
	
	
	#10000;
	
	$finish;
end

always #5 clk = !clk;

endmodule