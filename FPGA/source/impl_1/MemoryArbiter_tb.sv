module MemoryArbiter_tb();
	reg clk = 0;
	reg nRst = 1;

	reg [16:0] spi_addr = 0;
	reg spi_we = 0;
	reg spi_req = 0;
	wire [7:0]spi_do;
	reg [7:0]spi_di = 0;
	wire spi_grnt;
	
	reg [15:0] lc_addr = 0;
	wire [15:0] lc_do;
	reg lc_req = 0;
	wire lc_grnt;
	
	MainRamArbiter uut(.clk(clk), .nRst(nRst), 
		.spi_addr(spi_addr), .spi_di(spi_di), .spi_do(spi_do), 
		.spi_we(spi_we), .spi_req(spi_req), .spi_grnt(spi_grnt),
		.lc_addr(lc_addr), .lc_do(lc_do), .lc_req(lc_req), .lc_grnt(lc_grnt));
		
	always #5 clk = !clk;
		
	initial begin
		#20 nRst = 0;
		#20 nRst = 1;
		#20;
		@(posedge clk)
		fork
			begin
				for(int i = 0; i < 100; i++) begin
					automatic int delaySteps = $urandom_range(5);
					for(int j = 0; j < delaySteps; j++) @(posedge clk);
					spi_addr = $urandom_range(17'h1ffff);
					spi_di = $urandom_range(8'hff);
					spi_we = $urandom_range(1);
					spi_req = 1;
					do begin
						@(posedge clk);
						spi_req = 0;
					end while(!spi_grnt);
				end				
			end 
			begin
				for(int i = 0; i < 100; i++) begin
					automatic int delaySteps = $urandom_range(5);
					for(int j = 0; j < delaySteps; j++) @(posedge clk);
					lc_addr = $urandom_range(17'h1ffff);
					lc_req = 1;
					do begin
						@(posedge clk);
						lc_req = 0;
					end while(!lc_grnt);
				end		
			end
		join
		$finish;
	end
endmodule