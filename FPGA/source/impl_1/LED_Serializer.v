module LED_Serializer #(
	parameter NUM_ROWS = 10,
	parameter NUM_COLUMNS = 20,
	parameter LEDS_PER_DRIVER = 15,
	parameter NUM_ROW_LEDS = NUM_COLUMNS * 3,
	parameter NR_STEPS = 8
	) (
	input clk,
	input nRst,
	
	// Interface to cube
	output lc_clk,
	output reg [NUM_ROWS-1:0] lc_data,
	output lc_lat,
	
	// Interface to plane generator
	output [($clog2(NUM_ROW_LEDS))-1:0] mem_addr,
	input [(NR_STEPS*NUM_ROWS-1):0] mem_di,
	output reg mem_req,
	input mem_grnt,
	
	// Control lines	
	output reg busy,
	input send_step,
	output reg step_done
	);
	
	localparam NUM_DRIVERS = (NUM_ROW_LEDS + LEDS_PER_DRIVER - 1)/LEDS_PER_DRIVER;
	localparam DRIVER_PADDING_BITS = 16 - LEDS_PER_DRIVER;
	localparam ROW_BITS = NUM_ROW_LEDS + NUM_DRIVERS * DRIVER_PADDING_BITS;
	localparam STEPS_SIZE = $clog2(NR_STEPS);
	localparam COUNTER_SIZE=$clog2(ROW_BITS);
	
	// Transmitter
	reg [STEPS_SIZE-1:0]step_idx;			
	always @(*) begin
		case(step_idx)
		3'd0: lc_data = mem_di[(0 + 1) * NUM_ROWS - 1 : 0 * NUM_ROWS];
		3'd1: lc_data = mem_di[(1 + 1) * NUM_ROWS - 1 : 1 * NUM_ROWS];
		3'd2: lc_data = mem_di[(2 + 1) * NUM_ROWS - 1 : 2 * NUM_ROWS];
		3'd3: lc_data = mem_di[(3 + 1) * NUM_ROWS - 1 : 3 * NUM_ROWS];
		3'd4: lc_data = mem_di[(4 + 1) * NUM_ROWS - 1 : 4 * NUM_ROWS];
		3'd5: lc_data = mem_di[(5 + 1) * NUM_ROWS - 1 : 5 * NUM_ROWS];
		3'd6: lc_data = mem_di[(6 + 1) * NUM_ROWS - 1 : 6 * NUM_ROWS];
		3'd7: lc_data = mem_di[(7 + 1) * NUM_ROWS - 1 : 7 * NUM_ROWS];
		endcase
	end
	
	// Step counter
	reg stepCntrEn;
	wire stepsDone;
	assign stepsDone = step_idx == NR_STEPS - 1;
	always @(posedge clk or negedge nRst)
	begin
		if(!nRst) begin
			step_idx <= {(STEPS_SIZE-1){1'b0}};
		end else begin
			if(stepCntrEn) begin
				step_idx <= step_idx + 1'b1;
			end else begin
				step_idx <= step_idx;
			end
		end
	end	
	
	// TX clock
	wire shift_enable;
	reg [1:0] out_clk_counter;
	reg outClkCounting;
	wire outClkDone;
	assign lc_clk = out_clk_counter[1];
	assign outClkDone = out_clk_counter == 2'b11;
	always @(posedge clk or negedge nRst) 
	begin
		if(!nRst) begin
			out_clk_counter <= 2'b11;
			outClkCounting <= 1'b0;
		end else begin
			if((outClkCounting && !outClkDone) || shift_enable || mem_req) begin
				out_clk_counter <= out_clk_counter + 1'b1;
			end
			outClkCounting <= (outClkCounting && !outClkDone) || shift_enable;
		end
	end
	
	// Latching counter
	reg [1:0] latching_counter;
	reg doLatch;
	reg latchCounting;
	wire latchDone;
	assign lc_lat = latching_counter[1];
	assign latchDone = latching_counter == 2'b11;
	always @(posedge clk or negedge nRst) begin
		if(!nRst) begin
			latching_counter <= 2'b00;
			latchCounting <= 1'b0;
		end else begin
			if(doLatch || latchCounting) begin
				latching_counter <= latching_counter + 1'b1;			
			end
			latchCounting <= (latchCounting && !latchDone) || doLatch;
		end
	end
	
	// TX counter
	reg [COUNTER_SIZE-1:0]counter;
	reg [COUNTER_SIZE-1:0]addrCounter;
	wire [($clog2(NUM_ROW_LEDS))-1:0]nextAddr;
	reg counter_enable;
	wire counterStep;
	wire planeDone;
	assign planeDone = counter == ROW_BITS - 1 && outClkDone;
	wire isPaddingBit;
	assign isPaddingBit = counter[3:0] < (16-LEDS_PER_DRIVER);
	assign nextAddr = counterStep ? (isPaddingBit ? addrCounter : (addrCounter + 1'b1)) : addrCounter;
	always @(posedge clk or negedge nRst) begin
		if(!nRst) begin
			counter <= {COUNTER_SIZE{1'b0}};
			addrCounter <= {COUNTER_SIZE{1'b0}};
		end else begin
			if(counter_enable) begin
				if(counterStep) begin
					counter <= counter + 1'b1; 
				end else begin
					counter <= counter;			  
				end
				addrCounter <= nextAddr;
			end else begin
				counter <= {COUNTER_SIZE{1'b0}};
				addrCounter <= {COUNTER_SIZE{1'b0}};
			end
		end
	end
	assign mem_addr = nextAddr;
	
	// Memory access
	reg next_word_received; 
	assign counterStep = next_word_received && outClkDone;
	assign shift_enable = mem_grnt;
	always @(posedge clk or negedge nRst)
	begin
		if(!nRst) begin
			next_word_received <= 1'b0;
		end else begin
			if(mem_req) begin
				next_word_received <= 1'b0;
			end else begin
				next_word_received <= next_word_received || mem_grnt;
			end
		end
	end
	
	// FSM
	localparam IDLE = 0, SENDING = 1, WAIT_STEP = 2, LATCHING = 3;
	reg [1:0] state, next_state;
	
	always @(posedge clk or negedge nRst) begin
		if(!nRst) begin
			state <= IDLE;
		end else begin
			state <= next_state;
		end
	end
	
	always @(*) begin
		case(state)
		IDLE: begin
			counter_enable <= 1'b0;
			doLatch <= 1'b0;
			stepCntrEn <= 1'b0;
			step_done <= 1'b0;
			if(send_step) begin
				next_state <= SENDING;
				busy <= 1'b0;
				mem_req <= 1'b1;
			end else begin
				next_state <= IDLE;
				busy <= 1'b0;
				mem_req <= 1'b0;
			end
		end
		SENDING: begin
			busy <= 1'b1;
			stepCntrEn <= 1'b0;
			doLatch <= 1'b0;
			if(planeDone) begin
				counter_enable <= 1'b0;
				next_state <= WAIT_STEP;
				step_done <= 1'b0;
				mem_req <= 1'b0;
			end else begin
				counter_enable <= 1'b1;
				next_state <= SENDING;
				step_done <= 1'b0;
				mem_req <= outClkDone;
			end
		end
		WAIT_STEP: begin
			busy <= 1'b1;
			counter_enable <= 1'b0;
			stepCntrEn <= 1'b0;	
			mem_req <= 1'b0;	
			step_done <= 1'b1;
			if(send_step) begin
				next_state <= LATCHING;
				doLatch <= 1'b1;	
			end else begin
				next_state <= WAIT_STEP;	
				doLatch <= 1'b0;			
			end	
		end
		LATCHING: begin
			counter_enable <= 1'b0;
			doLatch <= 1'b0;
			step_done <= 1'b0;
			if(latchDone) begin
				stepCntrEn <= 1'b1;
				if(stepsDone) begin
					next_state <= IDLE;
					busy <= 1'b1;
					mem_req <= 1'b0;
				end else begin
					next_state <= SENDING;
					busy <= 1'b1;
					mem_req <= 1'b1;
				end
			end else begin
				stepCntrEn <= 1'b0;
				next_state <= LATCHING;
				busy <= 1'b1;
				mem_req <= 1'b0;
		 	end
		end
		default: begin
			counter_enable <= 1'b0;
			doLatch <= 1'b0;	
			stepCntrEn <= 1'b0;	
			next_state <= IDLE;
			busy <= 1'b0;
			mem_req <= 1'b0;
			step_done <= 1'b0;
		end
		endcase
	end

endmodule