module top(
	// General signals
	input clk_in,
	input nRst,
	
	// Reserved signals
	inout [4:0] res_io,
	
	// Communication signals
	input spi_sck,
	input spi_di,
	output spi_do,
	input spi_nCs,
	output cubeBlank,
	output planeBlank,
	
	// LED Cube control signals
	output [9:0] lc_p,
	output lc_blank,
	output lc_latch,
	output lc_clk,
	output [9:0] lc_d,
	output [2:0] lc_dim_p
	
	);
	
	// PLL
	wire clk;
	wire clkCore;
	syspll syspll_inst(clk_in, nRst, clkCore, clk);
	
	// Intensity driver
	wire [2:0] lc_dim;
	GlobalIntensityDriver intensityDriver(.intensity(lc_dim), .lc_dim_p(lc_dim_p));
	
	// Main RAM
	wire [16:0] spiMemAddr;
	wire spiMemWe;
	wire spiMemReq;
	wire [7:0]spiMemDo;
	wire [7:0]spiMemDi;
	wire spiMemGrnt;
	
	wire [12:0]lcMemAddr;
	wire lcMemReq;
	wire lcMemGrnt;
	wire [15:0]lcMemData;
	MainRamArbiter ram(.clk(clk), .nRst(nRst), 
		.spi_addr(spiMemAddr), .spi_di(spiMemDo), .spi_do(spiMemDi), 
		.spi_we(spiMemWe), .spi_req(spiMemReq), .spi_grnt(spiMemGrnt),
		.lc_addr({3'b000, lcMemAddr}), .lc_do(lcMemData), .lc_req(lcMemReq), .lc_grnt(lcMemGrnt));
	
	// LED Cube driver
	wire lcEnable;
	wire lcRunning;
	wire [3:0] lcActivePlane;
	LCDriver lcDriver(.clk(clk), .nRst(nRst), 
		.enable(lcEnable), .running(lcRunning), .frameSync(cubeBlank), .planeSync(planeBlank), .activePlane(lcActivePlane),
		.mem_di(lcMemData), .mem_addr(lcMemAddr), .mem_req(lcMemReq), .mem_valid(lcMemGrnt), 
		.lc_clk(lc_clk), .lc_data(lc_d), .lc_lat(lc_latch), .lc_plane(lc_p), .lc_blank(lc_blank));
	
	// SPI interface
	wire [7:0] spiRxData;
	wire spiRxValid;
	wire [7:0] spiTxData;
	wire spiTxValid;
	wire spiTxBusy;
	SpiSlave spi (
		.sclk(spi_sck), .sdi(spi_di), .sdo(spi_do), .nCs(spi_nCs),
		.clk(clk), .nRst(nRst), .rxData(spiRxData), .rxValid(spiRxValid),
		.txData(spiTxData), .txValid(spiTxValid), .txBusy(spiTxBusy));
		
	wire [4:0] spiRegAddr;
	wire spiRegWe;
	wire [7:0] spiRegDo;
	wire [7:0] spiRegDi;
	
	SpiSlaveController spiController (
		.clk(clk), .nRst(nRst),
		.spi_rxData(spiRxData), .spi_rxValid(spiRxValid), .spi_txData(spiTxData), .spi_txValid(spiTxValid), .spi_busy(spiTxBusy),
		.reg_addr(spiRegAddr), .reg_we(spiRegWe), .reg_do(spiRegDo), .reg_di(spiRegDi), 
		.mem_addr(spiMemAddr), .mem_we(spiMemWe), .mem_req(spiMemReq), .mem_do(spiMemDo), .mem_di(spiMemDi), .mem_grnt(spiMemGrnt));
		
	RegisterMap regmap(.clk(clk), .nRst(nRst),
		.spi_addr(spiRegAddr), .spi_we(spiRegWe), .spi_di(spiRegDo), .spi_do(spiRegDi),
		// Ctrl reg
		.ctrl_en(lcEnable),
		// Status reg
		.sr_running(lcRunning),
		// Intensity reg
		.intensity(lc_dim),
		// Position reg
		.pr_active_plane(lcActivePlane));
		
	
endmodule