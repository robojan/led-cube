module MainRamArbiter (
	input clk,
	input nRst,
	
	// port 1 SPI port
	input [16:0]spi_addr,
	input [7:0]spi_di,
	output [7:0]spi_do,
	input spi_we,
	input spi_req,
	output reg spi_grnt,
	
	// port 2 LED cube driver
	input [15:0]lc_addr,
	output [15:0]lc_do,
	input lc_req,
	output reg lc_grnt
	);
	
	// RAM
	reg [15:0] memAddr;
	reg [15:0] memDi;
	wire [15:0] memDo;
	reg memWe;
	reg [3:0] memMask;
	reg memCs;
	
	MainRam mem (.clk(clk), .addr(memAddr), .dataIn(memDi), 
		.dataOut(memDo), .we(memWe), .maskWe(memMask), .cs(memCs));
	
	// Spi byte select
	reg spi_hi_byte;
	assign spi_do = spi_hi_byte ? memDo[15:8] : memDo[7:0];
	
	// LC output
	assign lc_do = memDo;
	
	// Arbitration
	localparam INPUT_SPI = 2'd0, INPUT_LC = 2'd1, INPUT_PEND_SPI = 2'd2;
	reg [1:0] activeInput;
		
	// Spi waiting
	reg [16:0] pending_spi_addr;
	reg [7:0] pending_spi_di;
	reg pending_spi_we;
	reg pending_spi_req;
	
	// Output delay
	reg lc_grnt_1;
	reg spi_grnt_1;
	
	reg spi_hi_byte_1;
	always @(posedge clk or negedge nRst) begin
		if(!nRst) begin
			lc_grnt <= 1'b0;
			spi_grnt <= 1'b0;
			spi_hi_byte <= 1'b0;
		end else begin
			lc_grnt <= lc_grnt_1;
			spi_grnt <= spi_grnt_1;
			spi_hi_byte <= spi_hi_byte_1;
		end
	end	
		
	always @(posedge clk or negedge nRst) begin
		if(!nRst) begin 
			spi_grnt_1 <= 1'b0;
			lc_grnt_1 <= 1'b0;
			pending_spi_addr <= 17'b0;
			pending_spi_di <= 8'b0;
			pending_spi_we <= 1'b0;
			pending_spi_req <= 1'b0;
			activeInput <= INPUT_SPI;
			memCs <= 1'b0;
			spi_hi_byte_1 <= 1'b0;
		end else begin
			pending_spi_addr <= spi_addr;
			pending_spi_di <= spi_di;
			pending_spi_we <= spi_we;
			case({lc_req, spi_req, pending_spi_req})
			3'b000: begin
				spi_grnt_1 <= 1'b0;
				lc_grnt_1 <= 1'b0;
				pending_spi_req <= 1'b0;
				activeInput <= INPUT_SPI;
				memCs <= 1'b0;
				memAddr <= spi_addr[16:1];
				memDi <= {spi_di, spi_di};
				memMask <= spi_addr[0] ? 4'b1100 : 4'b0011;
				memWe <= spi_we;
				spi_hi_byte_1 <= spi_addr[0];
			end
			3'b001: begin
				spi_grnt_1 <= 1'b1;
				lc_grnt_1 <= 1'b0;
				pending_spi_req <= 1'b0;
				activeInput <= INPUT_PEND_SPI;
				memCs <= 1'b1;
				memAddr <= pending_spi_addr[16:1];
				memDi <= {pending_spi_di, pending_spi_di};
				memMask <= pending_spi_addr[0] ? 4'b1100 : 4'b0011;
				memWe <= pending_spi_we;
				spi_hi_byte_1 <= pending_spi_addr[0];
			end
			3'b010: begin
				spi_grnt_1 <= 1'b1;
				lc_grnt_1 <= 1'b0;
				pending_spi_req <= 1'b0;
				activeInput <= INPUT_SPI;
				memCs <= 1'b1;
				memAddr <= spi_addr[16:1];
				memDi <= {spi_di, spi_di};
				memMask <= spi_addr[0] ? 4'b1100 : 4'b0011;
				memWe <= spi_we;
				spi_hi_byte_1 <= spi_addr[0];
			end
			3'b011: begin
				spi_grnt_1 <= 1'b1;
				lc_grnt_1 <= 1'b0;
				pending_spi_req <= 1'b1;
				activeInput <= INPUT_PEND_SPI;
				memCs <= 1'b1;
				memAddr <= pending_spi_addr[16:1];
				memDi <= {pending_spi_di, pending_spi_di};
				memMask <= pending_spi_addr[0] ? 4'b1100 : 4'b0011;
				memWe <= pending_spi_we;
				spi_hi_byte_1 <= pending_spi_addr[0];
			end
			3'b100: begin
				spi_grnt_1 <= 1'b0;
				lc_grnt_1 <= 1'b1;
				pending_spi_req <= 1'b0;
				activeInput <= INPUT_LC;
				memCs <= 1'b1;
				memAddr <= lc_addr;
				memDi <= 16'b0;
				memMask <= 4'b1111;
				memWe <= 1'b0;
				spi_hi_byte_1 <= 1'b0;
			end
			3'b101: begin
				spi_grnt_1 <= 1'b0;
				lc_grnt_1 <= 1'b1;
				pending_spi_req <= 1'b0;
				activeInput <= INPUT_LC;
				memCs <= 1'b1;
				memAddr <= lc_addr;
				memDi <= 16'b0;
				memMask <= 4'b1111;
				memWe <= 1'b0;
				spi_hi_byte_1 <= 1'b0;
			end
			3'b110, 3'b111: begin
				spi_grnt_1 <= 1'b0;
				lc_grnt_1 <= 1'b1;
				pending_spi_req <= 1'b1;
				activeInput <= INPUT_LC;
				memCs <= 1'b1;
				memAddr <= lc_addr;
				memDi <= 16'b0;
				memMask <= 4'b1111;
				memWe <= 1'b0;
				spi_hi_byte_1 <= 1'b0;
			end
			endcase
		end
	end
	
endmodule