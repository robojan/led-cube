module MainRam(input clk, input [15:0] addr, input [15:0] dataIn, output reg [15:0] dataOut, input we, input [3:0] maskWe, input cs);

	wire [13:0] ram_addr;
	wire [1:0] ram_hiAddr;
	assign ram_addr = addr[13:0];
	assign ram_hiAddr = addr[15:14];
		
	reg ram1_cs, ram2_cs, ram3_cs, ram4_cs;
	wire [15:0] ram1_do, ram2_do, ram3_do, ram4_do;
	
	// RAM
	SP256K ram1 (
		.AD(ram_addr), .DI(dataIn), .MASKWE(maskWe), .WE(we), .CS(ram1_cs && cs),
		.CK(clk), .STDBY(1'b0), .SLEEP(1'b0), .PWROFF_N(1'b1), .DO(ram1_do));
	SP256K ram2 (
		.AD(ram_addr), .DI(dataIn), .MASKWE(maskWe), .WE(we), .CS(ram2_cs && cs),
		.CK(clk), .STDBY(1'b0), .SLEEP(1'b0), .PWROFF_N(1'b1), .DO(ram2_do));
	SP256K ram3 (
		.AD(ram_addr), .DI(dataIn), .MASKWE(maskWe), .WE(we), .CS(ram3_cs && cs),
		.CK(clk), .STDBY(1'b0), .SLEEP(1'b0), .PWROFF_N(1'b1), .DO(ram3_do));
	SP256K ram4 (
		.AD(ram_addr), .DI(dataIn), .MASKWE(maskWe), .WE(we), .CS(ram4_cs && cs),
		.CK(clk), .STDBY(1'b0), .SLEEP(1'b0), .PWROFF_N(1'b1), .DO(ram4_do));
		
	always @(ram_hiAddr) begin
		case(ram_hiAddr)
		//case(0)
		0: begin
			ram1_cs <= 1'b1;
			ram2_cs <= 1'b0;
			ram3_cs <= 1'b0;
			ram4_cs <= 1'b0;
		end
		1: begin
			ram1_cs <= 1'b0;
			ram2_cs <= 1'b1;
			ram3_cs <= 1'b0;
			ram4_cs <= 1'b0; 
		end
		2: begin
			ram1_cs <= 1'b0;
			ram2_cs <= 1'b0;
			ram3_cs <= 1'b1;
			ram4_cs <= 1'b0; 
		end
		3: begin
			ram1_cs <= 1'b0;
			ram2_cs <= 1'b0;
			ram3_cs <= 1'b0;
			ram4_cs <= 1'b1; 
		end
		endcase
	end
	
	reg [1:0] delayedRamHiAddr;
	always @(posedge clk) delayedRamHiAddr <= ram_hiAddr;
	
	always @(*) begin
		case(delayedRamHiAddr) 
		//case(0)
		0: dataOut <= ram1_do;
		1: dataOut <= ram2_do;
		2: dataOut <= ram3_do;
		3: dataOut <= ram4_do;
		endcase
	end 
		
endmodule