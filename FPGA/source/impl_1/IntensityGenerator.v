module IntensityGenerator #(
	parameter NUM_ROWS = 10,
	parameter NUM_COLUMNS = 60,
	parameter NUM_STEPS = 8
	) (
	input clk,
	input nRst,
	
	// Control
	input [4:0] comp_msb,
	input load_plane,
	output reg busy,
	
	// Memory interface input
	input [15:0] in_di,
	output [8:0] in_addr,
	output reg in_req,
	input in_valid,
	
	// Memory interface output
	output [NUM_ROWS * NUM_STEPS - 1:0] out_do,
	output [5:0] out_addr,
	output reg out_req,
	input out_grnt
	);

localparam IN_COLUMN_ADDR_WIDTH = $clog2((NUM_COLUMNS / 2));
localparam IN_ROW_ADDR_WIDTH = $clog2((NUM_ROWS - 1));
localparam [IN_ROW_ADDR_WIDTH-1:0] ROW_ADDR_STEP_SIZE = 1;
localparam [IN_COLUMN_ADDR_WIDTH-1:0] COLUMN_ADDR_STEP_SIZE = 1;

localparam OUT_ADDR_WIDTH = $clog2(NUM_COLUMNS);

reg [NUM_ROWS * NUM_STEPS - 1:0] nextValue1;
reg [NUM_ROWS * NUM_STEPS - 1:0] nextValue2;
wire [NUM_ROWS - 1:0] activeBit;
	
		
// Pipeline delay
reg [NUM_ROWS - 1:0]delayedActiveBit;
reg [7:0] led1;
reg [7:0] led2;
always @(posedge clk) begin
	delayedActiveBit <= activeBit;
	led1 <= in_di[7:0];
	led2 <= in_di[15:8];
end

genvar i;
genvar j;
generate
	for (i = 0; i < NUM_STEPS; i = i + 1) begin
		// Comparison value
		wire [7:0]comp_val;
		assign comp_val[7:3] = comp_msb;
		assign comp_val[2:0] = i;
		
		// Comparison for the intensity
		wire comp_l1;
		wire comp_l2;
		assign comp_l1 = led1 > comp_val;
		assign comp_l2 = led2 > comp_val;
		
		// Update bits in the nextValue
		for(j = 0; j < NUM_ROWS; j = j + 1) begin
			always @(posedge clk or negedge nRst)
			begin
				if(!nRst) begin
					nextValue1[i*NUM_ROWS+j] <= 1'bx;
					nextValue2[i*NUM_ROWS+j] <= 1'bx;
				end else begin
					nextValue1[i*NUM_ROWS+j] <= delayedActiveBit[j] ? comp_l1 : nextValue1[i*NUM_ROWS+j];
					nextValue2[i*NUM_ROWS+j] <= delayedActiveBit[j] ? comp_l2 : nextValue2[i*NUM_ROWS+j];
				end
			end
		end
	end
endgenerate
	
// Input address logic
reg [IN_ROW_ADDR_WIDTH - 1:0] inRowAddrCounter;
reg [IN_COLUMN_ADDR_WIDTH - 1:0] inColumnAddrCounter;
wire [IN_COLUMN_ADDR_WIDTH - 1:0] nextInColumnAddr;
wire [IN_COLUMN_ADDR_WIDTH - 1:0] nextInColumnAddrCalc;
reg inRowAddrCounterEn;
reg inColumnAddrCounterEn;
reg inColumnAddrCounterRst;
wire loadingColumnDone;  
wire loadingRowDone;
assign nextInColumnAddrCalc = inColumnAddrCounterRst ? {IN_COLUMN_ADDR_WIDTH{1'b0}} : inColumnAddrCounter + COLUMN_ADDR_STEP_SIZE;
assign nextInColumnAddr = inColumnAddrCounterEn ? nextInColumnAddrCalc : inColumnAddrCounter;
assign in_addr = {inRowAddrCounter, inColumnAddrCounter};
assign loadingColumnDone = inRowAddrCounter == ROW_ADDR_STEP_SIZE * (NUM_ROWS) && in_valid;
assign loadingRowDone = inColumnAddrCounter == COLUMN_ADDR_STEP_SIZE * (NUM_COLUMNS/2);

always @(posedge clk or negedge nRst)
begin
	if(!nRst) begin
		inRowAddrCounter <= 0;
	end else begin
		if(loadingColumnDone) begin
			inRowAddrCounter <= 0;
		end else begin
			inRowAddrCounter <= inRowAddrCounterEn ? inRowAddrCounter + ROW_ADDR_STEP_SIZE : inRowAddrCounter;
		end
	end
end

always @(posedge clk or negedge nRst)
begin
	if(!nRst) begin
		inColumnAddrCounter <= 0;
	end else begin
		inColumnAddrCounter <= nextInColumnAddr;
	end
end

// Output address logic
reg [OUT_ADDR_WIDTH - 2:0] outAddrCounter;
reg outAddrCounterEn;
reg outAddrCounterRst;				   

always @(posedge clk or negedge nRst)
begin
	if(!nRst) begin
		outAddrCounter <= 0;
	end else begin
		if(outAddrCounterRst) begin
			outAddrCounter <= 0;
		end else if(outAddrCounterEn) begin
			outAddrCounter <= outAddrCounter + 1'b1;
		end else begin
			outAddrCounter <= outAddrCounter;
		end
	end
end
reg outAddrLsb;
assign out_addr = {outAddrCounter, outAddrLsb};
assign out_do = outAddrLsb ? nextValue2 : nextValue1;

// Activebit logic
reg activeBitEnable;
reg activeBitCount;
reg [NUM_ROWS - 1:0] activeBitState;
assign activeBit = activeBitEnable ? activeBitState : {NUM_ROWS{1'b0}};
always @(posedge clk or negedge nRst)
begin
	if(!nRst) begin
		activeBitState <= {{(NUM_ROWS - 1){1'b0}},1'b1};
	end else begin
		if(activeBitCount) begin
			activeBitState <= {activeBitState[NUM_ROWS-2:0],activeBitState[NUM_ROWS-1]};
		end else begin
			activeBitState <= activeBitState;
		end
	end
end

// FSM
localparam IDLE = 0, LOADING_COLUMN = 1, LOADING_DONE = 2, PIPELINE_WAIT = 3, STORE1 = 4, STORE2 = 5, STORE2_LASTROW=6;

reg [2:0] state;
reg [2:0] nextState;

always @(posedge clk or negedge nRst)
begin
	if(!nRst) begin
		state <= IDLE;
	end else begin
		state <= nextState;
	end
end


// Outputs:
// busy
// out_req
// inRowAddrCounterEn
// inColumnAddrCounterEn
// inColumnAddrCounterRst
// outAddrCounterEn
// outAddrCounterRst
// outAddrLsb
// activeBitCount
// activeBitEnable
// in_req
//
// 
// Inputs:
// in_valid
// load_plane
// out_grnt
// loadingColumnDone
// loadingRowDone
always @(*)
begin
	case (state)
	IDLE: begin
		inColumnAddrCounterEn <= 1'b0;
		outAddrCounterEn <= 1'b0;
		outAddrCounterRst <= 1'b0;
		outAddrLsb <= 1'b0;
		out_req <= 1'b0;	 
		activeBitEnable <= 1'b0;
		activeBitCount <= 1'b0;
		inColumnAddrCounterRst <= 1'b1;
		if(load_plane) begin
			nextState <= LOADING_COLUMN;
			busy <= 1'b0;	
			in_req <= 1'b1;
			inRowAddrCounterEn <= 1'b1;
		end else begin
			nextState <= IDLE;
			busy <= 1'b0;	  
			in_req <= 1'b0; 
			inRowAddrCounterEn <= 1'b0;
		end
	end
	LOADING_COLUMN: begin
		outAddrCounterEn <= 1'b0;
		outAddrCounterRst <= 1'b0;
		busy <= 1'b1;
		inColumnAddrCounterRst <= 1'b0;
		outAddrLsb <= 1'b0;
		if(in_valid) begin
			inRowAddrCounterEn <= 1'b1;
			activeBitEnable <= 1'b1;
			activeBitCount <= 1'b1;
			if(loadingColumnDone) begin
				nextState <= LOADING_DONE;
				inColumnAddrCounterEn <= 1'b1;
				out_req <= 1'b0;
				in_req <= 1'b0;
			end else begin
				nextState <= LOADING_COLUMN;
				inColumnAddrCounterEn <= 1'b0;
				out_req <= 1'b0;
				in_req <= 1'b1;				
			end
		end else begin
			inColumnAddrCounterEn <= 1'b0;
			inRowAddrCounterEn <= 1'b0;
			activeBitEnable	<= 1'b0;
			activeBitCount <= 1'b0;
			nextState <= LOADING_COLUMN;
			out_req <= 1'b0;
			in_req <= 1'b0;
		end
	end
	LOADING_DONE: begin		   
		busy <= 1'b1;
		inRowAddrCounterEn <= 1'b0;
		inColumnAddrCounterEn <= 1'b0;
		outAddrCounterEn <= 1'b0;
		outAddrCounterRst <= 1'b0;
		activeBitEnable	<= 1'b0;
		activeBitCount <= 1'b0;
		in_req <= 1'b0;
		out_req <= 1'b0; 
		outAddrLsb <= 1'b0;
		nextState <= PIPELINE_WAIT;  
		inColumnAddrCounterRst <= 1'b0;
	end
	PIPELINE_WAIT: begin		   
		busy <= 1'b1;
		inRowAddrCounterEn <= 1'b0;
		inColumnAddrCounterEn <= 1'b0;
		outAddrCounterEn <= 1'b0;
		outAddrCounterRst <= 1'b0;
		activeBitEnable	<= 1'b0;
		activeBitCount <= 1'b0;
		in_req <= 1'b0;
		out_req <= 1'b1; 
		outAddrLsb <= 1'b0;
		nextState <= STORE1;  
		inColumnAddrCounterRst <= 1'b0;
	end
	STORE1: begin
		busy <= 1'b1;
		inRowAddrCounterEn <= 1'b0;
		outAddrCounterEn <= 1'b0;
		outAddrCounterRst <= 1'b0;
		activeBitEnable	<= 1'b0;
		activeBitCount <= 1'b0;
		in_req <= 1'b0;
		if(out_grnt) begin
			outAddrLsb <= 1'b1;
			out_req <= 1'b1;
			inColumnAddrCounterRst <= loadingRowDone;
			inColumnAddrCounterEn <= loadingRowDone;
			nextState <= loadingRowDone ? STORE2_LASTROW : STORE2;
		end else begin
			outAddrLsb <= 1'b0;
			out_req <= 1'b1;
			inColumnAddrCounterRst <= 1'b0;
			inColumnAddrCounterEn <= 1'b0;
			nextState <= STORE1;
		end
	end
	STORE2: begin
		activeBitEnable	<= 1'b0;
		inColumnAddrCounterEn <= 1'b0;
		activeBitCount <= 1'b0;
		busy <= 1'b1;
		inColumnAddrCounterRst <= 1'b0;
		if(out_grnt) begin
			out_req <= 1'b0;
			outAddrLsb <= 1'b0;
			nextState <= LOADING_COLUMN;
			outAddrCounterEn <= 1'b1;
			outAddrCounterRst <= 1'b0;
			in_req <= 1'b1;
			inRowAddrCounterEn <= 1'b1;
		end else begin
			out_req <= 1'b1;
			outAddrLsb <= 1'b1;
			nextState <= STORE2;
			outAddrCounterEn <= 1'b0;
			outAddrCounterRst <= 1'b0;
			in_req <= 1'b0;
			inRowAddrCounterEn <= 1'b0;
		end
	end
	STORE2_LASTROW: begin
		activeBitEnable	<= 1'b0;
		inColumnAddrCounterEn <= 1'b0;
		activeBitCount <= 1'b0;
		busy <= 1'b1;
		inColumnAddrCounterRst <= 1'b0;
		if(out_grnt) begin
			out_req <= 1'b0;
			outAddrLsb <= 1'b0;
			nextState <= IDLE;
			outAddrCounterEn <= 1'b0;
			outAddrCounterRst <= 1'b1;
			in_req <= 1'b0;
			inRowAddrCounterEn <= 1'b0;
		end else begin
			out_req <= 1'b1;
			outAddrLsb <= 1'b1;
			nextState <= STORE2_LASTROW;
			outAddrCounterEn <= 1'b0;
			outAddrCounterRst <= 1'b0;
			in_req <= 1'b0;
			inRowAddrCounterEn <= 1'b0;
		end
	end
	default: begin
		busy <= 1'b0;
		inRowAddrCounterEn <= 1'b0;
		inColumnAddrCounterEn <= 1'b0;
		outAddrCounterEn <= 1'b0;
		outAddrCounterRst <= 1'b0;
		activeBitEnable	<= 1'b0;
		activeBitCount <= 1'b0;
		in_req <= 1'b0;
		out_req <= 1'b0; 
		outAddrLsb <= 1'b0;
		nextState <= IDLE;  
		inColumnAddrCounterRst <= 1'b0;
	end
	endcase
end
	
endmodule