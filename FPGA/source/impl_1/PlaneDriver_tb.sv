module PlaneDriver_tb();

reg clk, nRst, next, enable;
wire [9:0]plane;
wire blank;
wire busy;
integer i = 0;

PlaneDriver inst(.clk(clk), .nRst(nRst), .plane(plane), .blank(blank), .next(next), .enable(enable), .busy(busy));

initial begin
	$display("next,enable|plane,blank");
	$monitor("%1b,%1b|%10b,%1b", next, enable, plane, blank);
	clk = 0;
	nRst = 1;
	next = 0;
	enable = 0;
	#10	nRst = 0;
	#20 nRst = 1;
	$display("Reset done");
	#10 enable = 1;
	
	for (i=0; i<20; i=i+1) begin
		$display("Step %d", i);
		#10 next = 1;
		#60 next = 0; enable = 0;
		#10 enable = 1;
	end
	$finish;
end

always #5 clk = !clk;

endmodule