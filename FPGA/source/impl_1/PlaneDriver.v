module PlaneDriver #(
	parameter NUM_PLANES = 10
	) (
	input clk,
	input nRst,
	
	output [NUM_PLANES-1:0]plane,
	output reg blank,
	
	input next,
	input enable,
	output busy);
	
	localparam IDLE = 0, PRESWITCH = 1, SWITCH = 2, POSTSWITCH = 3;
	
	wire do_next;
	reg [NUM_PLANES-1:0] plane_active;
	reg do_step;
	reg [1:0] state, next_state;
	
	assign busy = state != IDLE;
	
	
	// Plane output
	assign plane = enable ? plane_active : {NUM_PLANES{1'b0}};
	
	// Edge detect
	EdgeDetect next_ed(clk, nRst, next, do_next);
	
	// Plane shifter
	always @(posedge clk or negedge nRst)
	begin
		if(!nRst) begin
			plane_active <= {{NUM_PLANES-1{1'b0}}, 1'b1};
		end else begin
			plane_active <= do_step ? {plane_active[NUM_PLANES-2:0], plane_active[NUM_PLANES-1]} : plane_active;
		end
	end
	
	// FSM
	always @(posedge clk or negedge nRst)
	begin
		if(!nRst) begin
			state <= IDLE;
		end else begin
			state <= next_state;
		end
	end
	
	always @(state or do_next)
	begin
		case(state)
		IDLE: begin
			next_state <= do_next ? PRESWITCH : IDLE;
			blank <= 1'b0;
			do_step <= 1'b0;
		end
		PRESWITCH: begin
			blank <= 1'b1;
			next_state <= SWITCH;
			do_step <= 1'b0;
		end
		SWITCH: begin
			blank <= 1'b1;
			next_state <= POSTSWITCH;
			do_step <= 1'b1;
		end
		POSTSWITCH: begin
			blank <= 1'b1;
			next_state <= IDLE;
			do_step <= 1'b0;
		end
		endcase
	end

endmodule