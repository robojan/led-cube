module PlaneBufferRAMArbiter(
	input clk, input nRst, 
	
	// Generator Interface
	input [79:0] a_data,
	input [6:0] a_addr,
	input a_req,
	output reg a_grnt,
	
	// Serializer interface
	input [6:0] b_addr,
	output [79:0] b_do,
	input b_req,
	output reg b_grnt
	);
	
	PlaneBufferRAM mem(
		.wr_clk_i(clk),
        .rd_clk_i(clk),
        .rst_i(!nRst),
        .wr_clk_en_i(1'b1),
        .rd_en_i(b_req),
        .rd_clk_en_i(1'b1),
        .wr_en_i(a_req),
        .wr_data_i(a_data),
        .wr_addr_i(a_addr),
        .rd_addr_i(b_addr),
        .rd_data_o(b_do));
		
	always @(posedge clk or negedge nRst) a_grnt <= !nRst ? 1'b0 : a_req;
	always @(posedge clk or negedge nRst) b_grnt <= !nRst ? 1'b0 : b_req;
		
endmodule