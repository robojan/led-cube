module Impulsifier(input clk, input nRst, input x, output y);

reg lastX;
always @(posedge clk or negedge nRst) begin
	if(!nRst) begin
		lastX <= 1'b0;
	end else begin
		lastX <= x;
	end
end

assign y = x && !lastX;

endmodule