setenv SIM_WORKING_FOLDER .
set newDesign 0
if {![file exists "G:/git/LED_cube/FPGA/completeSIM/completeSIM.adf"]} { 
	design create completeSIM "G:/git/LED_cube/FPGA"
  set newDesign 1
}
design open "G:/git/LED_cube/FPGA/completeSIM"
cd "G:/git/LED_cube/FPGA"
designverincludedir -clear
designverlibrarysim -PL -clear
designverlibrarysim -L -clear
designverlibrarysim -PL pmi_work
designverlibrarysim ovi_ice40up
designverdefinemacro -clear
if {$newDesign == 0} { 
  removefile -Y -D *
}
set readmempath "G:/git/LED_cube/FPGA/PlaneBufferRAM;G:/git/LED_cube/FPGA/syspll;G:/git/LED_cube/FPGA/TestFrameBufferROM"
addfile "G:/git/LED_cube/FPGA/impl_1/LED_Cube_impl_1_vo.vo"
addfile "G:/git/LED_cube/FPGA/source/impl_1/ram_mock.sv"
addfile "G:/git/LED_cube/FPGA/source/impl_1/SP256K.v"
addfile "G:/git/LED_cube/FPGA/source/impl_1/top_tb.sv"
vlib "G:/git/LED_cube/FPGA/completeSIM/work"
set worklib work
adel -all
vlog -dbg  "G:/git/LED_cube/FPGA/impl_1/LED_Cube_impl_1_vo.vo"
vlog -sv2k12 -dbg  -work work "G:/git/LED_cube/FPGA/source/impl_1/ram_mock.sv"
vlog -dbg  -work work "G:/git/LED_cube/FPGA/source/impl_1/SP256K.v"
vlog -sv2k12 -dbg  -work work "G:/git/LED_cube/FPGA/source/impl_1/top_tb.sv"
vsim  +access +r top_tb   -L pmi_work -L ovi_ice40up
add wave *
run 1000ns
