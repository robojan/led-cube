setenv SIM_WORKING_FOLDER .
set newDesign 0
if {![file exists "G:/git/LED_cube/FPGA/LedCubeSIM/LedCubeSIM.adf"]} { 
	design create LedCubeSIM "G:/git/LED_cube/FPGA"
  set newDesign 1
}
design open "G:/git/LED_cube/FPGA/LedCubeSIM"
cd "G:/git/LED_cube/FPGA"
designverincludedir -clear
designverlibrarysim -PL -clear
designverlibrarysim -L -clear
designverlibrarysim -PL pmi_work
designverlibrarysim ovi_ice40up
designverdefinemacro -clear
if {$newDesign == 0} { 
  removefile -Y -D *
}
set readmempath "G:/git/LED_cube/FPGA/comspi;G:/git/LED_cube/FPGA/syspll"
addfile "G:/git/LED_cube/FPGA/source/impl_1/EdgeDetect.v"
addfile "G:/git/LED_cube/FPGA/source/impl_1/PlaneDriver.v"
addfile "G:/git/LED_cube/FPGA/syspll/rtl/syspll.v"
addfile "G:/git/LED_cube/FPGA/source/impl_1/top.v"
addfile "G:/git/LED_cube/FPGA/comspi/rtl/comspi.v"
vlib "G:/git/LED_cube/FPGA/LedCubeSIM/work"
set worklib work
adel -all
vlog -dbg  -work work "G:/git/LED_cube/FPGA/source/impl_1/EdgeDetect.v"
vlog -dbg  -work work "G:/git/LED_cube/FPGA/source/impl_1/PlaneDriver.v"
vlog -dbg  -work work "G:/git/LED_cube/FPGA/syspll/rtl/syspll.v"
vlog -dbg  -work work "G:/git/LED_cube/FPGA/source/impl_1/top.v"
vlog -dbg  -work work "G:/git/LED_cube/FPGA/comspi/rtl/comspi.v"
module top
vsim  +access +r top   -L pmi_work -L ovi_ice40up
add wave *
run 1000ns
