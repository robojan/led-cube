setenv SIM_WORKING_FOLDER .
set newDesign 0
if {![file exists "G:/git/LED_cube/FPGA/MemoryArbiterSIM/MemoryArbiterSIM.adf"]} { 
	design create MemoryArbiterSIM "G:/git/LED_cube/FPGA"
  set newDesign 1
}
design open "G:/git/LED_cube/FPGA/MemoryArbiterSIM"
cd "G:/git/LED_cube/FPGA"
designverincludedir -clear
designverlibrarysim -PL -clear
designverlibrarysim -L -clear
designverlibrarysim -PL pmi_work
designverlibrarysim ovi_ice40up
designverdefinemacro -clear
if {$newDesign == 0} { 
  removefile -Y -D *
}
set readmempath "G:/git/LED_cube/FPGA/PlaneBufferRAM;G:/git/LED_cube/FPGA/syspll;G:/git/LED_cube/FPGA/TestFrameBufferROM"
addfile "G:/git/LED_cube/FPGA/source/impl_1/MainRam.v"
addfile "G:/git/LED_cube/FPGA/source/impl_1/MainRamArbiter.v"
addfile "G:/git/LED_cube/FPGA/source/impl_1/MemoryArbiter_tb.sv"
addfile "G:/git/LED_cube/FPGA/source/impl_1/SP256K.v"
vlib "G:/git/LED_cube/FPGA/MemoryArbiterSIM/work"
set worklib work
adel -all
vlog -dbg  -work work "G:/git/LED_cube/FPGA/source/impl_1/MainRam.v"
vlog -dbg  -work work "G:/git/LED_cube/FPGA/source/impl_1/MainRamArbiter.v"
vlog -sv2k12 -dbg  -work work "G:/git/LED_cube/FPGA/source/impl_1/MemoryArbiter_tb.sv"
vlog -dbg  "G:/git/LED_cube/FPGA/source/impl_1/SP256K.v"
vsim  +access +r MemoryArbiter_tb   -L pmi_work -L ovi_ice40up
add wave *
run 1000ns
